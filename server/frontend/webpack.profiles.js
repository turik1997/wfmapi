const path = require('path');

const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {

    production: {
        vars: {
            DEBUG_MODE: false,
            log: function(msg) { console.log(msg)}
        },
        config: {
            production: true,
            plugins: [
                new UglifyJsPlugin({
                    parallel: 3,
                    test: /\.js($|\?)/i,
                    uglifyOptions: {
                        ie8: true
                    }
                }),
                new OptimizeCssAssetsPlugin({
                    assetNameRegExp: /\.optimize\.css$/g,
                    cssProcessor: require('cssnano'),
                    cssProcessorOptions: { discardComments: { removeAll: true } },
                    canPrint: true
                }),
                new CssoWebpackPlugin()
            ]
        }
    },
    dev: {
        activeByDefault: true,
        vars: {
            DEBUG_MODE: false,
            consolere: consolere,
            log: function(msg) {
                if ( REMOTE_LOG )
                    console.re.log(msg);
                else
                    console.log(msg);
            },
            REMOTE_LOG: false
        },
        config: {
            plugins: []
        }
    },
    'remote-debug': {
        vars: {
            DEBUG_MODE: true,
            REMOTE_LOG: true,
            consolere: consolere,
            log: function(msg) {
                console.re.log(msg);
            }
        }
    }
};
