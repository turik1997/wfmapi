const $ = require('jquery');
const translate = require('./js/langs.js')();
import 'reset-css';
import './css/login.css';

var debugDefined = typeof DEBUG_MODE !== 'undefined';

$(document).ready(function(){

    var ip;

    $.get({
        url: "/wfm/api/v1/username",
        success: function(response) {
            ip = response.username;
            if ( debugDefined && DEBUG_MODE )
            	log(ip);
        }
    });

	$("form").submit(function(event){
	    var pass = $("#pass").val();
	    var username = ip, password = pass;
	    var token = username + ':' + password;
	    if ( debugDefined && DEBUG_MODE )
	    	log(token);
	    var authToken = btoa(token);
	    if ( debugDefined && DEBUG_MODE )
	    	log(authToken);
        $.ajax({
            url   : "/wfm/api/v1/auth",
            headers: {
                'Authorization': 'Basic ' + authToken//JSON.stringify({ pass:  pass.length == 0 ? "" : md5(pass)}), // data to be submitted
            },
            success: function(response){
                if ( response.defaultPath )
                {
                    document.cookie = "authorization=" + authToken + "; path=/";
                    $(location).attr('href', response.defaultPath);
                }
            }
        });
        return false;
	});
});