const $ = require('jquery');
const translate = require('./js/langs.js')();
import 'reset-css';
import './css/loading-animation.css';

document.cookie = 'authorization=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;';
var accessStatusTimer = setInterval(function(){
    var arr = window.location.href.split("/");
    //var origin = arr[0] + "//" + arr[2] + "/";
    $.ajax("/wfm/api/v1/access_status", {
        statusCode: {
            200: function (response) {
                clearInterval(accessStatusTimer);
                if ( arr.length == 3 )
                    $(location).attr('href', response.defaultPath);
                else
                {
                    log("stayin with the same url");
                    $(location).attr('href', window.location.href);
                }
            }
        }
    });
}, 1000);