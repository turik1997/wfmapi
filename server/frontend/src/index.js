const $ = require('jquery');
const jstree = require('jstree');
const Dropzone = require('dropzone');
const translate = require('./js/langs.js')();
const internalStorageImg = require('./images/internal.png');
const externalStorageImg = require('./images/external.png');
import StorageItem from './components/content/StorageItem';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import 'reset-css';
import 'dropzone/dist/min/dropzone.min.css';
import 'jstree/dist/themes/default/style.min.css';
import './css/style.css';
var columns = 0;
var rows = 0;
var ascending = true;
const types = {
	'NAME': 'NAME',
	'DATE': 'DATE',
	'SIZE': 'SIZE'
};
var orderType = types.NAME;
var content = null;
var files = [];
var filesToCopy = [];
var ascendingFiles = [];
var descendingFiles = [];
var copySrcPath = null;
const HORIZONTAL_MARGIN = 20;
const VERTICAL_MARGIN = 20;
const INNER_WIDTH = 140;
const INNER_HEIGHT = 140;
const ITEM_WIDTH = INNER_WIDTH + 2*HORIZONTAL_MARGIN;
const ITEM_HEIGHT = INNER_HEIGHT + 2*VERTICAL_MARGIN;
const STORAGE_AREA_LEFT_MARGIN = 0;
const STORAGE_AREA_TOP_MARGIN = 50;
var leftBarWidth = 0;
var headerHeight = 0;
var FLOOD = false;
var selectedFiles = {};
//const isMobile = checkMobile();
var isEntering = false;
var isMove = false;
var scrollWidth = 0;
var initialization = true;
const ESC = 27;
const CTRL = 17;
const DEL = 46;
const C_KEY = 67;
const V_KEY = 86;
const X_KEY = 88;
const ENTER = 13;
const ARROW_DOWN = 40;
const ARROW_LEFT = 37;
const ARROW_RIGHT = 39;
const ARROW_UP = 38;
const TASK_UPDATE_INTERVAL = 500;
var backgroundTasksUpdater = null;

function getTrueWidthOfDomElement(domElement) {
	// Use getComputedStyle to get width of elements because it returns float
	// https://github.com/que-etc/resize-observer-polyfill/issues/2
	if (domElement.currentStyle) {
		// Polyfill to support IE which has currentStyle instead of getComputedStyle
		// https://stackoverflow.com/a/5084041/6657837
		var elementWidth = domElement.currentStyle.width;
	} else if (window.getComputedStyle) {
		var elementWidth = window.getComputedStyle(domElement, null).getPropertyValue("width");
	} else {
		// fallback to offsetWidth
		var elementWidth = domElement.offsetWidth;
	}
	return parseFloat(elementWidth);
}


function makeAjaxRequest(ajaxRequest) {
    $.ajax({
        method: ajaxRequest.method,
        url: ajaxRequest.url,
        contentType: ajaxRequest.contentType,
        data: ajaxRequest.data,
        success: ajaxRequest.success,
        error: function(errResponse) {
            var json = errResponse.responseJSON;
            if (json.errorType === 'saf') {
                makeStorageAccessFrameworkDialog(json);
                return;
            }
            makeErrorDialog(json);
        }
    });
}

/*function checkMobile() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};*/

function forgetCut() {
    filesToCopy = [];
    copySrcPath = null;
    isMove = false;
    $('.cut').removeClass('cut');
    $('.btn-paste').addClass('disabled');
}

function showFileContextMenu(event) {
    var fileContextMenu = $('#file-context-menu');
    showContextMenu(fileContextMenu, event);
}

function showDefaultContextMenu(event) {
    var defaultContextMenu = $('#empty-context-menu');
    showContextMenu(defaultContextMenu, event);
}

function showContextMenu(jMenuElement, event) {
	if ( !jMenuElement )
		return;
    jMenuElement.css('left', event.pageX);
    jMenuElement.css('top', event.clientY);
    jMenuElement.css('display', 'block');
}

function enableCollectiveButtons() {
	$('.collective').removeClass('disabled');
}

function disableCollectiveButtons() {
	$('.collective').addClass('disabled');
}

function enableUnitButtons() {
	$('.single').removeClass('disabled');
}

function disableUnitButtons() {
	$('.single').addClass('disabled');
}

function AlertBuilder() {

    var width = 0;
    var dialog = $('<div/>', {
        'class': 'cd-popup',
        'role': 'alert'
    });
    dialog.bind('close', function(event) {
        $(this).removeClass('is-visible');
        setTimeout(function(){
            $(this).remove();
        }, 300);
    });

    var textHolder = $('<div/>', {
        'class': 'message'
    });

    var buttonHolder = $('<div/>', {
        'class': 'cd-buttons'
    });

    var closeButton = $('<div/>', {
        'class': 'cd-popup-close img-replace'
    });

    closeButton.click(function (event) {
        dialog.trigger('close');
    });

    var container = $('<div/>', {
        'class': 'cd-popup-container'
    }).append(textHolder).append(buttonHolder).append(closeButton).appendTo(dialog);

    var makeButton = function(text, id, attrClass, onclick) {
        var button = $('<div/>', {
            'class': 'cd-button' + ( attrClass && attrClass.length > 0 ? " " + attrClass : ""),
            'html': text
        });

        if ( id && id.length > 0 )
            button.attr('id', id);
        button.click(onclick);
        return button;
    };

    this.addButton = function(text, id, attrClass, onclick) {
        var button = makeButton(text, id, attrClass, onclick);
        width = Math.max(parseFloat(container.css('min-width')), buttonHolder.children().length * (150 + 10));
        buttonHolder.append(button);
        return this;
    };

    this.addCloseButtonClickedListener = function(callback) {
    	closeButton.click(callback);
    	return this;
    };

    this.setText = function(text, id, attrClass) {
        textHolder.removeClass('');
        textHolder.removeAttr('id');
        if ( attrClass && attrClass.length > 0 )
        {
            var classes = attrClass.split(' ');
            classes.forEach(function(item){
                if ( item.length > 0 )
                    textHolder.addClass(item);
            });
        }
        if ( id && id.length > 0 )
            textHolder.attr('id', id);
        textHolder.html(text);
        return this;
    };

    this.build = function() {
        container.css('width', width);
        return dialog;
    };
}

function getScrollbarWidth() {
    var outer = document.createElement("div");
    outer.style.visibility = "hidden";
    outer.style.width = "100px";
    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = $(outer).css('width');
    // force scrollbars
    outer.style.overflow = "scroll";

    // add innerdiv
    var inner = document.createElement("div");
    inner.style.width = "100%";
    outer.appendChild(inner);

    var widthWithScroll = $(inner).css('width');

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
}

function makeErrorDialog(json) {
    var message = json.error ? json.error : "Internal error";
    var message = json.reason ? message + ": " + json.reason : message;
    var alertBuilder = new AlertBuilder().setText(
        $('<div/>')
            .append(
                $('<h1/>')
                .append(message)
            )
    );
    var dialog = alertBuilder.build();
    $(document.body).append(dialog);
    dialog.addClass('is-visible');
}

function makeStorageAccessFrameworkDialog(errorData) {
    var alertBuilder = new AlertBuilder().setText(
        $('<div/>')
            .append(
                $('<h1/>')
                .append('Write access to external storage needed. Please open the application and press Continue')
            )
    ).addButton('Cancel', 'negative', "btn-no", function(event){
        $('.cd-popup').trigger('close');
    })
    .addButton('Continue', 'positive', "btn-yes confirm", function(event) {
        var continueAction = errorData.actions.find((action) => action.actionType === 'CONTINUE');
        makeAjaxRequest({
            method: continueAction.httpMethod,
            url: continueAction.url,
            data: continueAction.dataToSubmit,
            contentType: "application/json",
        });
        $('.cd-popup').trigger('close');
    })
    var dialog = alertBuilder.build();
    $(document.body).append(dialog);
    dialog.addClass('is-visible');
}

function makeInputDialog(onCreate) {
	var nameInputBox = $('<input/>', {
		'class': 'name-editor-box',
		'type': 'text'
	});
	nameInputBox.css('display', 'none');
	var error_msg = $('<p/>', {
		'class': 'error-msg'
	});
	error_msg.css('visibility', 'hidden');
	var text = $('<p/>', {
		'text': 'Enter the name of the new folder'
	});
	var message = $('<div/>').append(error_msg).append(text).append(nameInputBox);
	var setMessage = function(msg) {
		text.html(msg);
	};
	var setError = function(err) {
		if ( !err || typeof err !== typeof "" || err.length == 0 )
		{
			error_msg.html("");
			error_msg.css('visibility', 'hidden');
			return;
		}

		error_msg.html(err);
		error_msg.css('visibility', 'visible');
	};

	var setValue = function(value) {
		if ( typeof value !== typeof "" )
		{
			nameInputBox.val('');
			nameInputBox.css('display', 'none');
			return;
		}

		nameInputBox.val(value);
		nameInputBox.css('display', 'inline');
	};

	var msgObject = {
		set: setMessage,
		get: function() { return text.html(); }
	};
	var errorObject = {
		set: setError,
		get: function() { return error_msg.html(); }
	};

	var inputObject = {
		set: setValue,
		get: function() { return nameInputBox.val(); }
	};

	var inputDialogBuilder = new AlertBuilder().setText(message);

	onCreate(inputDialogBuilder.addButton, msgObject, errorObject, inputObject);

	return inputDialogBuilder.build();
}

function getTaskUpdater(id) {

    var idPrefix = "task";
    var taskId = idPrefix + id;

    function makeTaskHolder(id) {
        var target = $('<div/>', {
            'id': taskId,
            'class': 'task'
        });
        var innerDiv = $('<div/>');
        var details = $('<div/>', {
            'class': 'details'
        });

        var description = $('<div/>', {
        	'class': 'description'
        }).appendTo(details);
        var dynamicDescription = $('<div/>').appendTo(details);

        details.appendTo(innerDiv);

        var progressHolder = $('<div/>', {
            'class': 'progress'
        });

        var progressDiv = $('<div/>', {
            'class': 'indeterminate'
        }).appendTo(progressHolder);

        progressHolder.appendTo(innerDiv);
        var bottomInfo = $('<div/>').appendTo(innerDiv);
        innerDiv.appendTo(target);
        var button = $('<div/>', {
			'class': 'round-button cancel-button noselect',
			'html': '&#10005;'
		});
		target.append(button);
        target.hasDescription = function() {
        	return description.children().length > 0;
        };
        target.addDescription = function(element) {
        	description.append(element);
        };

        target.setDynamicDescription = function(element) {
        	dynamicDescription.html(element);
        };
        target.setBottomInfo = function(element) {
        	bottomInfo.html(element);
        };

        return target;
    }

    var taskHolder = makeTaskHolder(id);
    $('#notif-panel').prepend(taskHolder);

    return function(data) {
        var innerHolder = taskHolder.children('div:first');
        var details = innerHolder.children('.details');
        var button = taskHolder.children('.round-button');
        if ( !taskHolder.hasDescription() && data.description )
        {
        	var descriptionLines = data.description;
        	descriptionLines.forEach(function(item){
        		var name = item.name;
        		var value = item.value;
        		var line = $('<p/>', {
        		})
        		.append('<span class="name">' + name + '</span>')
        		.append(': ')
        		.append('<span class="value">' + value + '</span>');
        		taskHolder.addDescription(line);
        	});
        }

        var progress = innerHolder.children('.progress');
        var progressBar = progress.children('div:first');
        if ( data.status === 'PREPARING' )
        {
        	var info = $('<p/>')
        		.append('<span lang="en">Preparing...</span>')
        		.append('<span lang="ru">Подготовка...</span>')
        		.append('<span lang="az">Hazırlanır...</span>');

        	taskHolder.setBottomInfo(translate(info));
        	return;
        }

        if ( data.status === 'CANCELLING' )
        {
        	progressBar.removeClass('indeterminate').addClass('determinate');
        	progress.addClass('cancelled-progress');
        	button.html('&#10003;');
			button.removeClass('cancel-button').addClass('done-button').addClass('cancelled-button');
        }

        if ( progressBar.hasClass('indeterminate') )
        	progressBar.removeClass('indeterminate').addClass('determinate');
        progressBar.css('width', data.progress.totalDonePercentage + '%');

        if ( data.progress.currentFile )
        {
        	var currentFile = data.progress.currentFile;
        	var name = $('<span/>', {
        		'class': 'name'
        	});
        	var info = $('<p/>')
				.append('<span lang="en">Current file</span>')
				.append('<span lang="ru">Файл</span>')
				.append('<span lang="az">Hazırki fayl</span>');
			name.append(translate(info).html());
			var line = $('<p/>', {
			})
			.append(name)
			.append(': ')
			.append('<span class="value">' + currentFile + '</span>');
			taskHolder.setDynamicDescription(line);
        }

        if ( data.progress.ready && data.progress.total )
        {
        	var ready = data.progress.ready;
        	var total = data.progress.total;
        	var line = ('<p><span class="done">' + ready + '</span>/<span class="total">' + total + '</span><span class="unit"></span></p>')
        	taskHolder.setBottomInfo(line);
        }

        if ( data.progress.totalDonePercentage == 100 )
        {
        	button
        		.removeClass('cancel-button')
        		.addClass('done-button')
        		.html('&#10003;');

        }
    };
}

function redrawFiles(files, orderedIndices, content) {

	function getHtmlDivId(index) {
		const idPrefix = "file";
		return idPrefix + (index+1);
	}

	const fileItemsToRender = orderedIndices.map(function(item, index){

		var file = files[item];
		const props = {
			htmlDivId: getHtmlDivId(index),
			fileSize: file.dataSize,
			fileSizeUnit: file.dataSizeUnit,
			fileExtension: file.extension,
			fileName: file.name,
			fileType: file.type,
			isSelected: false,
			isSelectedLast: false,
		};

		return <StorageItem {...props} />;
	});

	
	ReactDOM.render(
		React.createElement(React.Fragment, null, fileItemsToRender),
		content.children('#file-area').get(0)
	);
}

function applyScroll(elementId, direction) {

}

function getTopXIndex(x) {
	var possibleItemIndex = Math.floor(x / ITEM_WIDTH);
	if ( x > (ITEM_WIDTH * possibleItemIndex + HORIZONTAL_MARGIN + INNER_WIDTH) )
		possibleItemIndex++;
	return possibleItemIndex;
}

function getBottomXIndex(x) {
	var possibleItemIndex = Math.floor(x / ITEM_WIDTH);
	if ( x < (ITEM_WIDTH * possibleItemIndex + HORIZONTAL_MARGIN) )
		possibleItemIndex--;
	return possibleItemIndex;
}

function getTopYIndex(y) {
	var possibleItemIndex = Math.floor(y / ITEM_HEIGHT);
	//log("possibleTop: " + possibleItemIndex);
	//log("End of possible: " + (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN + INNER_HEIGHT) );
	if ( y > (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN + INNER_HEIGHT) )
		possibleItemIndex++;
	return possibleItemIndex;
}

function getBottomYIndex(y) {
	var possibleItemIndex = Math.floor(y / ITEM_HEIGHT);
	//log("possibleBottom: " + possibleItemIndex);
	//log("begin of possible: " + (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN) + " but y: " + y);
	if ( y < (ITEM_HEIGHT * possibleItemIndex + VERTICAL_MARGIN) )
		possibleItemIndex--;
	return possibleItemIndex;
}

var ascendingOrder = true;
var storageContent = null;
var selectionStarted = false;
var itemsCount = 0;

function getMousePosition(event){
	//log("x: " + event.clientX + " y: " + event.clientY);
	return {x:(event.pageX+$('#storage-content').prop('scrollLeft')-getTrueWidthOfDomElement($('#left-bar').get(0))-$('#storage-content').prop('offsetLeft')),
		y: (event.pageY+$('#storage-content').prop('scrollTop')-$('#storage-content').prop('offsetTop') - headerHeight)};
}

function makeSegmentTreeQuery(rect) {
	return {
		'startRow': getTopYIndex(rect.p1.y),
		'startColumn': getTopXIndex(rect.p1.x),
		'endRow': getBottomYIndex(rect.p2.y),
		'endColumn': getBottomXIndex(rect.p2.x)
	};
}

function makeRectangle(point1, point2) {
	return {
		'p1': point1,
		'p2': point2
	};
}

function makePoint(x, y) {
	return {
		'x': x,
		'y': y
	};
}

/*function getItemsVisibleOnScreen(scrollableItemsContent) {

	if ( !scrollableItemsContent )
		return emptySet();

	var topY = scrollableItemsContent.prop('scrollTop') - STORAGE_AREA_TOP_MARGIN;
	var topX = scrollableItemsContent.prop('scrollLeft') - STORAGE_AREA_LEFT_MARGIN;
	var point1 = makePoint(topX, topY);

	var bottomY = topY + scrollableItemsContent.prop('offsetHeight') - 1;
	var bottomX = topX + scrollableItemsContent.prop('offsetWidth') - 1;
	var point2 = makePoint(bottomX, bottomY);

	var treeQuery = makeSegmentTreeQuery(makeRectangle(point1, point2));

	return getItems(0, 0, rows-1, columns-1, treeQuery, 0);
}*/

function isLeftButton(event) {
	return event.which && event.which == 1;
}

function isRightButton(event) {
	return event.which && event.which == 3;
}

function prepareStorageContent(storageContent) {

	function selectFiles(x1, y1, x2, y2, callback) {

		if ( !callback )
			callback = function (item) {};
		var array = [];

		var areaWidth = getTrueWidthOfDomElement(storageContent.get(0)) - STORAGE_AREA_LEFT_MARGIN;
		var areaHeight = storageContent.prop('scrollHeight') - STORAGE_AREA_TOP_MARGIN;
		if ( x1 > areaWidth )
		    x1 = areaWidth;
        if ( x2 > areaWidth )
            x2 = areaWidth;
        if ( y1 > areaHeight )
            y1 = areaHeight;
        if ( y2 > areaHeight )
            y2 = areaHeight;

		columns = Math.floor(areaWidth / 180);
		rows = Math.floor(areaHeight / 180);


		if ( itemsCount < columns )
			columns = itemsCount;

		var startRow = getTopYIndex(y1);
		var startColumn = getTopXIndex(x1);
		var endRow = getBottomYIndex(y2);
		var endColumn = getBottomXIndex(x2);

		if ( endColumn == columns )
		    endColumn = columns-1;
        if ( endRow == rows )
            endRow = rows-1;

		if ( DEBUG_MODE )
			log("Selecting files." + " startRow: " + startRow + " startColumn: " + startColumn + " endRow: " + endRow + " endColumn: " + endColumn);

		if ( startRow > endRow || startColumn > endColumn )
			return array;

		for ( var i = startRow; i <= endRow; i++ )
		{
			for ( var j = startColumn; j <= endColumn; j++ )
			{
				var id = i * columns + j + 1;
				if ( id > itemsCount )
					continue;
				var fileId = '#file' + id;
				callback(fileId);
				array.push(fileId);
			}
		}

		return array;
	}

	function configMouseMovements(storageContent) {

		function redrawMouseSelection(topX, topY, bottomX, bottomY) {
			mouseSelection.css('visibility', 'visible');
			mouseSelection.css('left', topX);
			mouseSelection.css('top', topY);
			mouseSelection.css('width', bottomX-topX);
			mouseSelection.css('height', bottomY-topY);
		}

		function stopMouseSelection() {
			mouseSelection.css('width', 0);
			mouseSelection.css('height', 0);
			mouseSelection.css('visibility', 'hidden');
		}

		function onItemMouseD(itemId, event, cursorPoint) {
			var clickEvent;
			if ( isLeftButton(event) )
			{
				clickEvent = $.Event('leftclick', cursorPoint);
			}
			else
			if ( isRightButton(event) )
			{
				clickEvent = $.Event('rightclick', cursorPoint);
			}
			else
			{
				clickEvent = event;
			}

			$(itemId).trigger(clickEvent);
		}

		function getMouseDownedItem(cursorPoint) {
			if ( !cursorPoint )
				return null;

			var possibleItemId = null;
			var array = selectFiles(cursorPoint.x - STORAGE_AREA_LEFT_MARGIN, cursorPoint.y - STORAGE_AREA_TOP_MARGIN,
									cursorPoint.x - STORAGE_AREA_LEFT_MARGIN, cursorPoint.y - STORAGE_AREA_TOP_MARGIN);
			if ( array.length > 0 )
				possibleItemId = array[0];

			return possibleItemId;
		}

		var isMouseDownedOnFile = false;
		var isMouseDownedOnEmptyArea = false;
		var isDraggingFiles = false;
		var isSelectingFiles = false;
		var mouseDownedFile = null;
		var mouseDownedPosition = null;
		var draggedFiles = [];
		const DRAG_EPSILON = 4;

		storageContent.mousedown(function(event){
            event.preventDefault();
            event.stopPropagation();

			var cursorPoint = getMousePosition(event);
			if ( DEBUG_MODE && FLOOD )
				log(cursorPoint);

			mouseDownedPosition = cursorPoint;

			//var possibleItemsToClickOn = getItemsVisibleOnScreen(storageContent);
			var itemId = getMouseDownedItem(cursorPoint);

			if ( itemId )
			{
				isMouseDownedOnFile = true;
				mouseDownedFile = itemId;
				enableCollectiveButtons();

				var selectedCount = $('#selected-count').html();
				/*if ( !isNaN(selectedCount) && selectedCount > 1 && $(mouseDownedFile).hasClass('selected') )
				{
					return;
				}*/

				/*if ( !event.ctrlKey )
				{
					deselectSelectedFiles();
				}*/

				//$('.last-selected').removeClass('last-selected');
				//$(mouseDownedFile).addClass('selected').addClass('last-selected');
			}
			else
			{
                $('#selected-count').html(0);
                disableCollectiveButtons();
			}

			if ( isLeftButton(event) )
			{
				if ( itemId )
					return;

				$('.last-selected').removeClass('last-selected');
				if ( !event.ctrlKey )
				{
					deselectSelectedFiles();
				}

				isMouseDownedOnEmptyArea = true;
				return;
			}

			if ( isRightButton(event) )
			{
				hideContextMenu();
				if ( !itemId )
				{
					deselectSelectedFiles();
					showDefaultContextMenu(event);
					disableCollectiveButtons();
					return;
				}

				if ( !$(itemId).hasClass('selected') && !event.ctrlKey )
				{
					deselectSelectedFiles();
				}
				return;
			}

			if ( DEBUG_MODE )
				log("No item is clicked. Must be a selection");

		});

		function isDrag(mouseDownedPosition, cursorPosition) {
			return Math.abs(mouseDownedPosition.x - cursorPosition.x) >= DRAG_EPSILON ||
				Math.abs(mouseDownedPosition.y - cursorPosition.y) >= DRAG_EPSILON;
		}

		storageContent.mousemove(function (event) {
			event.preventDefault();
			event.stopPropagation();

			var cursorPosition = getMousePosition(event);
			if ( DEBUG_MODE && FLOOD )
			{
                log(cursorPosition);
			}

			if ( isDraggingFiles )
			{
				//TODO animation of dragging
			}

			if ( isMouseDownedOnFile )
			{
				if ( isDrag(mouseDownedPosition, cursorPosition) )
				{
					isDraggingFiles = true;
					//TODO (animation or registering) of dragged files
				}

				return;
			}

			if ( isSelectingFiles )
			{
				var topX = Math.min(mouseDownedPosition.x, cursorPosition.x);
				var bottomX = Math.max(mouseDownedPosition.x, cursorPosition.x);
				var topY = Math.min(mouseDownedPosition.y, cursorPosition.y);
				var bottomY = Math.max(mouseDownedPosition.y, cursorPosition.y);

				redrawMouseSelection(topX, topY, bottomX, bottomY);

                deselectAll();

				if ( event.ctrlKey )
				{
					for ( var key in selectedFiles )
						$(key).addClass('selected');
				}

				selectFiles(topX-STORAGE_AREA_LEFT_MARGIN, topY-STORAGE_AREA_TOP_MARGIN,
					bottomX-STORAGE_AREA_LEFT_MARGIN, bottomY-STORAGE_AREA_TOP_MARGIN, function (item) {
						if ( selectedFiles[item] )
						{
							$(item).removeClass('selected');
						}
						else
						{
							$(item).addClass('selected');
						}
					});

				var selectedCount = $('.selected').length;
                var state = selectedCount > 0 ? enableCollectiveButtons : disableCollectiveButtons;
				state();
				if ( selectedCount == 1 )
					enableUnitButtons();
				else
					disableUnitButtons();
				$('#selected-count').html(selectedCount);
				return;
			}

			if ( isMouseDownedOnEmptyArea )
			{
				/*
				var topX = Math.min(mouseDownedPosition.x, cursorPosition.x);
				var bottomX = Math.max(mouseDownedPosition.x, cursorPosition.x);
				var topY = Math.min(mouseDownedPosition.y, cursorPosition.y);
				var bottomY = Math.max(mouseDownedPosition.y, cursorPosition.y);

				selectFiles(topX-STORAGE_AREA_LEFT_MARGIN, topY-STORAGE_AREA_TOP_MARGIN,
					bottomX-STORAGE_AREA_LEFT_MARGIN, bottomY-STORAGE_AREA_TOP_MARGIN);*/
				if ( isDrag(mouseDownedPosition, cursorPosition) )
					isSelectingFiles = true;
				return
			}
		});

		var scrollingtimer = null;
		var diff = 0;
		var height = 0;
		var scrollHeight = 0;
		var scrollInit = function(event, jElement, coeff) {
			scrollHeight = storageContent.prop('scrollHeight');
			if ( !isMouseDownedOnEmptyArea )
				return;

			if ( coeff == 1 && storageContent.prop('scrollTop') == 0 )
				return;
			if ( coeff == -1 && storageContent.prop('scrollTop') == (storageContent.prop('scrollHeight')-storageContent.prop('offsetHeight')) )
				return;

			height = jElement.prop('offsetHeight');
			if ( jElement.attr('id') === 'header' )
				diff = height - event.pageY;
			else
			{
				var footerStart = $(document.body).prop('offsetHeight') - jElement.prop('offsetHeight');
				diff = Math.max(0, event.pageY - footerStart);
			}

			if ( isSelectingFiles )
			{

				if ( !scrollingtimer )
					scrollingtimer = setInterval(function () {
						storageContent.get(0).scrollTop -= coeff == 1
							? (diff * 342) / height * coeff
							: coeff * Math.min((diff * 342) / height, scrollHeight);
						var cursorPosition = getMousePosition(event);
						var topX = Math.min(mouseDownedPosition.x, cursorPosition.x);
						var bottomX = Math.max(mouseDownedPosition.x, cursorPosition.x);
						var topY = Math.min(mouseDownedPosition.y, cursorPosition.y);
						var bottomY = Math.max(mouseDownedPosition.y, cursorPosition.y);

						redrawMouseSelection(topX, topY, bottomX, bottomY);
					}, 100);
				return;
			}

			var cursorPosition = getMousePosition(event);
			if ( isDrag(mouseDownedPosition, cursorPosition) )
			{
				isSelectingFiles = true;
				scrollingtimer = setInterval(function () {
					storageContent.get(0).scrollTop -= (diff * 114) / height * coeff;
				}, 100);
				return;
			}
		};

		$('#header').mousemove(function (event) {
			height = $(this).prop('offsetHeight');
			diff = height - event.pageY;
		});

		$('#header').mouseleave(function (event) {
			clearInterval(scrollingtimer);
			scrollingtimer = null;
		});

		$('#header').mouseenter(function (event) {
			scrollInit(event, $(this), 1);
		});

		$('#footer').mouseenter(function(event){
			scrollInit(event, $(this), -1);
		});

		$('#footer').mouseleave(function(event){
			clearInterval(scrollingtimer);
			scrollingtimer = null;
		});

		$('#footer').mousemove(function(event){
			height = $(this).prop('offsetHeight');
			var footerStart = $(document.body).prop('offsetHeight') - height;
			diff = Math.max(0, event.pageY - footerStart);
		});

		var lastUpTime = -300;
		var lastUpItem = null;
		storageContent.mouseup(function(event) {
			event.preventDefault();
			event.stopPropagation();

			var currentTime = new Date().getTime();
			var cursorPosition = getMousePosition(event);
			if ( DEBUG_MODE )
				log("mouse is up");

			if ( isSelectingFiles )
			{
				isMouseDownedOnEmptyArea = false;
				isSelectingFiles = false;
				selectedFiles = {};
				var count = 0;
				$('.selected').each(function () {
					selectedFiles['#' + $(this).attr('id')] = true;
					count++;
				});
				$('#selected-count').html(count);
				if ( count == 1 )
					enableUnitButtons();
				else
					disableUnitButtons();

				stopMouseSelection();
				return;
			}

			if ( isDraggingFiles )
			{
				isDraggingFiles = false;
				isMouseDownedOnFile = false;
				var count = 0;
				$('.selected').each(function () {
					selectedFiles['#' + $(this).attr('id')] = true;
					count++;
				});
				$('#selected-count').html(count);
				//stopDragging();
				return;
			}

			if ( isMouseDownedOnFile )
			{

				isMouseDownedOnFile = false;

				if ( event.ctrlKey )
				{
					if ( isRightButton(event) )
					{
						showFileContextMenu(event);
						return;
					}
					if ( selectedFiles[mouseDownedFile] )
					{
						$(mouseDownedFile).removeClass('last-selected').removeClass('selected');
						delete selectedFiles[mouseDownedFile];
					}
					else
					{
                        $(mouseDownedFile).addClass('selected').addClass('last-selected');
						selectedFiles[mouseDownedFile] = true;
					}
				}
				else
				{
					if ( isLeftButton(event) )
                    	deselectSelectedFiles();

					if ( !selectedFiles[mouseDownedFile] )
					{
                        $(mouseDownedFile).addClass('selected').addClass('last-selected');
                        selectedFiles[mouseDownedFile] = true;
					}
					else
					{
						$('.last-selected').removeClass('last-selected');
						$(mouseDownedFile).addClass('selected').addClass('last-selected');
						selectedFiles[mouseDownedFile] = true;
					}

					if ( isRightButton(event) )
						showFileContextMenu(event);
				}

				var count = $('.selected').length;
				$('#selected-count').html(count);
				if ( count == 1 )
					enableUnitButtons();
				else
					disableUnitButtons();
				if ( !isLeftButton(event) )
					return;

				if ( (currentTime - lastUpTime) <= 300 && $(mouseDownedFile).is(lastUpItem) )
                {
                    $(mouseDownedFile).trigger('enter', window.location.href);
                    return;
                }

                lastUpTime = currentTime;
				lastUpItem = $(mouseDownedFile);
				return;
			}

			if ( isMouseDownedOnEmptyArea )
			{
				isMouseDownedOnEmptyArea = false;
				$('#selected-count').html(0);
				disableUnitButtons();
				disableCollectiveButtons();
				return;
			}

		});
	}

	function deselectAll() {
        $('.last-selected').removeClass('last-selected');
        $('.selected').removeClass('selected');
    }

	function deselectSelectedFiles() {
		$('.last-selected').removeClass('last-selected');
        for ( var key in selectedFiles )
        {
            $(key).removeClass('selected');
            delete selectedFiles[key];
        }

        selectedFiles = {};
    }

	function configSegmentTree(storageContent) {
		itemsCount = storageContent.children().length-1; //1 child is the mouse-selection
		var areaWidth = storageContent.prop('scrollWidth') - STORAGE_AREA_LEFT_MARGIN;
		var areaHeight = storageContent.prop('scrollHeight') - STORAGE_AREA_TOP_MARGIN;

		//TODO substitute dynamically calculated sizes for 160
		columns = Math.floor(areaWidth / 160);
		rows = Math.floor(areaHeight / 160);
		if ( itemsCount < columns )
			columns = itemsCount;

		buildTree(0, 0, rows-1, columns-1, 0);
	}

	function configStorageContentScrolling(storageContent) {

		function isScrollDown(previousPos, currentPos) {
			return previousPos - currentPos < 0;
		}

		$(window).bind('mousewheel DOMMouseScroll', function(event)
		{
			if(event.ctrlKey == true)
			{
				event.preventDefault();
			}
		});

		var upperBound = $('#upper-bound');
		var previousPosition = 0;
		storageContent.scroll(function(event){
			if ( $('.context-menu').css('display') !== 'none' )
			{
				event.preventDefault();
				event.stopPropagation();
				$(this).get(0).scrollTop = previousPosition;
			}
			var currentPosition = $(this).prop('scrollTop');
			if ( !isScrollDown(previousPosition, currentPosition) )
			{
				if ( currentPosition > 0 )
					upperBound.addClass('bound-overlay')
				else
					upperBound.removeClass('bound-overlay');

				upperBound.removeClass('hidden-bound');
				upperBound.addClass('fixed-bound');
			}
			else
			{
				upperBound.removeClass('fixed-bound');
				upperBound.addClass('hidden-bound');
			}

			previousPosition = currentPosition;
		});
	}

	var mouseSelection = $('#mouse-selection');
	//configSegmentTree(storageContent);
	configStorageContentScrolling(storageContent);
	configMouseMovements(storageContent);
	var timer;

	var startedAt, currentPos;

}

function showSortMenu() {
	$('#sort-by-menu').css('display', 'block');
}

function hideSortMenu() {
	$('#sort-by-menu').css('display', 'none');
}

function hideContextMenu() {
    $('.menu').css('display', 'none');
}

function isContextMenuOpen() {
	return $('.menu').css('display') !== 'none';
}

function prepareInterface() {


	var getNamesArray = function(jFiles) {
        var fileNames = [];
        jFiles.each(function(index, element){
            var nameElement = $(element).children('p:first');
            var name = nameElement.children('.filename').html();
            name += nameElement.children('.ext').html();
            fileNames.push(name);
        });

        return fileNames;
	};

    var remember = function(event, btn) {
        var selected = $('.selected');
        if ( selected.length == 0 )
        	return;
        forgetCut();
        isMove = $(btn).hasClass('btn-cut');
        filesToCopy = getNamesArray(selected);
        copySrcPath = decodeURIComponent(getPath());
        $('.btn-paste').removeClass('disabled');

        if ( DEBUG_MODE )
        {
        	log("Files to copy:" + filesToCopy.toString());
			log("Src: " + copySrcPath);
        }
    };

    var makeDownloadAjaxObject = function (selected, isZip) {
        return {
            'srcPath': decodeURIComponent(getPath()),
            'files': getNamesArray(selected),
			'zip': isZip
        };
    };

    var makeDownloader = function (selected, isZip) {
        return function () {

            var ajaxData = makeDownloadAjaxObject(selected, isZip);
            if ( DEBUG_MODE )
            	log(ajaxData);

            makeAjaxRequest({
                method: "POST",
                url: "/wfm/api/v1/download",
                data: JSON.stringify(ajaxData),
                contentType: "application/json",
                success: function (data) {
                	if ( DEBUG_MODE )
                    	log(data);
                    window.location.href = data.url;
                    //window.open(data.url);
                },
                error: function (err) {
                    log(err);
                }
            });
        }
    };

    var taskStatusCallbacks = [];
    var subscribeTaskCount = function(initiatorTaskId, callback){
    	if ( initiatorTaskId && callback && !taskStatusCallbacks[initiatorTaskId] )
    	 	taskStatusCallbacks[initiatorTaskId] = callback;
    	if ( backgroundTasksUpdater )
    		return;
		$.get({
			url: '/wfm/api/v1/tasks',
			contentType: "application/json",
			success: function(data){
				if ( !data )
					return;

				data.forEach(function(item){
					if ( item.taskId == initiatorTaskId )
						return;
					startTask(item, item.progress.workingDir);
				});
				$('#task-numbers').html(data.length);
				if ( data.length > 0 )
				{
					$('#background-tasks-desc').css('visibility', 'visible');
					$('#load-bar').css('visibility', 'visible');
					if ( !backgroundTasksUpdater )
					{
						backgroundTasksUpdater = setInterval(function(){
							$.get({
								url: '/wfm/api/v1/tasks',
								contentType: 'application/json',
								success: function(data){
									var taskCount = 0;
									if ( !data )
										data = { length: 0 };
									taskCount = data.length;
									$('#task-numbers').html(data.length);
									if ( data.length == 0 )
									{
										$('#background-tasks-desc').css('visibility', 'hidden');
										$('#load-bar').css('visibility', 'hidden');
										clearInterval(backgroundTasksUpdater);
										backgroundTasksUpdater = null;
										taskStatusCallbacks = [];
									}
									else
									{
										data.forEach(function(item){
											var currentCallback = taskStatusCallbacks[item.taskId];
											if ( !currentCallback )
												return;
											currentCallback(item);
										});
									}
								}
							});
						}, 1000);
					}
				}
				else
				{
					$('#background-tasks-desc').css('visibility', 'hidden');
					$('#load-bar').css('visibility', 'hidden');
					if ( backgroundTasksUpdater )
					{
						clearInterval(backgroundTasksUpdater);
						backgroundTasksUpdater = null;
					}
					taskStatusCallbacks = [];
				}
			}
		});
	};

    var onUpdate = function(data, opts) {
    	if ( DEBUG_MODE )
			log(data);
		var update = opts.updateFunc;
		update(data);
		var hasConflict = data.progress.hasConflict;
		if ( hasConflict )
		{
			if ( isPopupVisible() )
				return;

			clearInterval(opts.taskUpdateInterval);
			opts.taskUpdateInterval = null;

			var message = data.progress.dialog.text;
			var conflictType = data.progress.conflictType;
			var builder = new AlertBuilder();
			var isSAFConflict = conflictType && conflictType == "SAF_CONFLICT";
			if ( isSAFConflict )
			{
				message = $('<div/>').load('/wfm/web/saftext.html');
			}
			builder = builder.setText(message);
			var buttons = data.progress.dialog.buttons;
			if ( DEBUG_MODE )
				log(buttons);

			buttons.forEach(function(button) {
				if ( DEBUG_MODE )
					log(button);
				builder = builder.addButton(button.text, null, null, function(){
					makeAjaxRequest({
						url: '/wfm/api/v1/tasks/' + data.taskId,
						method: "POST",
						contentType: "application/json",
						data: JSON.stringify({
							action: button.action
						}),
						success: function(data) {
							if ( isSAFConflict )
							{
								//$('#img_steps').css('display', 'block');
								//return;
							}
							opts.taskUpdateInterval = setInterval(function() {
								$.get(opts);
							}, TASK_UPDATE_INTERVAL);
						},
						error: function(err) {
							log(err);
						}
					});
					//if ( !isSAFConflict )
						dialog.trigger('close');
				});
			});
			builder = builder.addCloseButtonClickedListener(function(event){
				$('#task' + data.taskId).children('.cancel-button').trigger('click');
			});

			var dialog = builder.build();
			$(document.body).append(dialog);
			dialog.addClass('is-visible');
			return;
		}
		if ( DEBUG_MODE )
			log(data.progress);
	   if ( data.progress.totalDonePercentage == 100 )
	   {
		   clearInterval(opts.taskUpdateInterval);
		   opts.taskUpdateInterval = null;
		   goToDir(window.location.href);
	   }
   };

   var startTask = function(data, affectedDir) {
   		if ( DEBUG_MODE )
	   		log(data);
		$('#background-tasks-desc').css('visibility', 'visible');
		$('#load-bar').css('visibility', 'visible');
	   var taskId = data.taskId;
	   $('#task' + taskId).data('affectedDir', affectedDir);
	   var update = getTaskUpdater(data.taskId);
	   var opts = {
		updateFunc: update,
		taskUpdateInterval: null,
		   url: '/wfm/api/v1/tasks/' + taskId,
		   success: function(successData) {
			subscribeTaskCount(taskId, function(successData){
				onUpdate(successData, opts);
			});
		   },
		   error: function(err) {
			   clearInterval(opts.taskUpdateInterval);
			   opts.taskUpdateInterval = null;
			   var taskElement = $('#task' + taskId);
			   var progress = taskElement.children('div:first').children('.progress');
			   var progressBar = progress.children('div:first');
			   var button = taskElement.children('.round-button');
			   if ( button.hasClass('done-button') )
			   	return;
			   progressBar.removeClass('indeterminate').addClass('determinate');
				progress.addClass('cancelled-progress');
				button.html('&#10003;');
				button.removeClass('cancel-button').addClass('done-button').addClass('cancelled-button');
		   }
	   };

	   //opts.taskUpdateInterval = setInterval(function() {
	   $.get(opts);
	   //}, TASK_UPDATE_INTERVAL);

	};

	$(function(){
		subscribeTaskCount();
	});

	$('#sort-by-btn').click(function (event) {
		var sortByMenu = $('#sort-by-menu');
		if ( sortByMenu.css('display') !== 'none' )
			hideSortMenu();
		else
			showSortMenu();
	});

	var setError = function(errorObject, msg, reason) {
		if ( !errorObject )
			return;
		var error = "";
		if ( msg )
			error += msg;
		if ( reason )
			error += ( error.length > 0 ? "<br>" : "") + reason;
		errorObject.set(error);
	};

	$('.btn-new-folder').click(function(event){
		var dialog = makeInputDialog(function(addButton, msg, err, input){
			$.get({
				url: "/wfm/api/v1/vacant_dir_name",
				success: function(data) {
					input.set(data.name);
				},
				error: function(errResponse) {
					var response = errResponse.responseJSON;
					setError(err, response.error, response.reason);
				}
			});

			var txt = $('<p/>')
				.append('<span lang="en">Enter the name of the new folder</span>')
				.append('<span lang="ru">Введите имя новой папки</span>')
				.append('<span lang="az">Yeni qovluğun adını daxil edin</span>');

			msg.set(translate(txt));

			var create = $('<p/>')
				.append('<span lang="en">Create</span>')
				.append('<span lang="ru">Создать</span>')
				.append('<span lang="az">Yarat</span>');

			addButton(translate(create), null, "btn-create confirm", function(event){
				var srcPath = getPath();
				makeAjaxRequest({
				    method: "POST",
                    url: "/wfm/api/v1/mkdir",
                    contentType: "application/json",
                    data: JSON.stringify({
                        dirName: input.get()
                    }),
                    success: function(data) {
                                dialog.trigger('close');
                                if ( getPath() === srcPath )
                                {
                                    $('#storage-content').trigger('content-changed', [data]);
                                    $('.last-selected').removeClass('last-selected');
                                    $('.selected').removeClass('selected');
                                }
                    },
                    error: function(errResponse) {
                                setError(err, json.error, json.reason);
                    }
				})
			});
		});
        $(document.body).append(dialog);
        dialog.addClass('is-visible');
	});

	$('.btn-rename').click(function(event){
		var selected = $('.selected');
		if ( selected.length <= 0 || selected.length > 1 )
		{
			log("Very strange. Number of selected is " + selected.length);
			return;
		}
		var nameElement = selected.children('p:first');
		var name = nameElement.children('.filename').html();
		name += nameElement.children('.ext').html();

		var dialog = makeInputDialog(function(addButton, msg, err, input){

			input.set(name);

			var txt = $('<p/>')
				.append('<span lang="en">Enter the new name</span>')
				.append('<span lang="ru">Введите новое имя</span>')
				.append('<span lang="az">Yeni ad daxil edin</span>');

			msg.set(translate(txt));

			var create = $('<p/>')
				.append('<span lang="en">Rename</span>')
				.append('<span lang="ru">Переименовать</span>')
				.append('<span lang="az">Dəyiş</span>');

			addButton(translate(create), null, "btn-create confirm", function(event){
				var srcPath = getPath();
				makeAjaxRequest({
					method: "POST",
					url: "/wfm/api/v1/rename",
					contentType: "application/json",
					data: JSON.stringify({
						path: decodeURIComponent(srcPath),
						fileName: name,
						name: input.get()
					}),
					success: function(data) {
						dialog.trigger('close');
						if ( getPath() === srcPath )
						{
							$('#storage-content').trigger('content-changed', [data]);
							$('.last-selected').removeClass('last-selected');
							$('.selected').removeClass('selected');
						}
					},
					error: function(errResponse) {
						var json = errResponse.responseJSON;
						setError(err, json.error, json.reason);
					}
				});
			});
		});
        $(document.body).append(dialog);
        dialog.addClass('is-visible');
	});

	$('.btn-download').click(function (event) {
		var selected = $('.selected');
		if ( selected.length == 0 )
			return;
		makeDownloader(selected, false)();
    });

	$('.btn-zip-download').click(function (event) {
		var selected = $('.selected');
		if ( selected.length == 0 )
			return;
		makeDownloader(selected, true)();
    });

	$('.btn-copy').click(remember);
	$('.btn-cut').click(function (event) {
		$('.cut').removeClass('cut');
		remember(event, this);
		$('.selected').each(function (index, item) {
			var icon = $(item).children('div:first');
			icon.addClass('cut');
        });
    });

	$('.btn-open').click(function(event){
		var selected = $('.selected');
		if ( selected.length != 1 )
		{
			log("Very strange. Selected count should be 1. Report to the admin");
			return;
		}
		selected.trigger('enter', [window.location.href]);
	});

    $('.btn-paste').click(function(event){
        if ( !copySrcPath )
            return;

		if ( DEBUG_MODE )
		{
			log("Copying");
			log(filesToCopy);
			log("src: " + copySrcPath);
		}

        var destPath = decodeURIComponent(getPath());
        makeAjaxRequest({
            method: "POST",
            url: isMove ? "/wfm/api/v1/move" : "/wfm/api/v1/copy",
            data: JSON.stringify({
                files: filesToCopy,
                srcPath: copySrcPath,
                destPath: destPath
            }),
            contentType: "application/json",
            success: function(data) {
            	startTask(data, destPath);
            }
        });
        if ( isMove )
            copySrcPath = null;
        isMove = false;
        forgetCut();
    });

    $('.btn-delete').click(function(event){

        var selected = $('.selected');
        if ( selected.length == 0 )
        {
            if ( DEBUG_MODE )
                log("Very peculiar. Not selected, but delete clicked");
            return;
        }

        var error_msg = $('<p/>', {
            'class': 'error-msg'
        });
        error_msg.css('visibility', 'hidden');

        var promptMsg = selected.length == 1
            ? selected.children('p:first').children('.filename').html() + selected.children('p:first').children('.ext').html()
            : selected.length + " items";
        var text = $('<p/>', {
            'text': 'Do you really want to delete ' + promptMsg + '?'
        });

        //var message = $('<div/>')append(text);

        var confirmDialogBuilder = new AlertBuilder()
            .setText(text)
            .addButton('Yes', 'positive', "btn-yes confirm", function(event){
                 if ( !dialog.hasClass('is-visible') )
                 {
                     if ( DEBUG_MODE )
                         log("Popup is not visible, but positive button pressed");
                     return;
                 }

                 var fileNames = [];
                 selected.each(function(index, element){
                     var nameElement = $(element).children('p:first');
                     var name = nameElement.children('.filename').html();
                     name += nameElement.children('.ext').html();
                     fileNames.push(name);
                 });

                 var srcPath = decodeURIComponent(getPath());

                 makeAjaxRequest({
                     method: "POST",
                     url: "/wfm/api/v1/delete",
                     data: JSON.stringify({
                         files: fileNames,
                         srcPath: srcPath
                     }),
                     contentType: "application/json",
                     success: function(data) {
                     	startTask(data, srcPath);
                     }
                 });
                 dialog.trigger('close');
             }).addButton("No", "negative", "btn-no", function(){
                dialog.trigger('close');
             });

        var dialog = confirmDialogBuilder.build();
        $(document.body).append(dialog);
        dialog.addClass('is-visible');
    });

}

function getJsonContent(dirName, successFunc, errorFunc) {

    $.get({
		url: dirName,
		headers: {
			'Prefer': 'json'
		},
		cache: false,
		success: function (dirContent) {
			successFunc(dirContent);
		},
		error: function (err) {
			errorFunc(err);
		}
	});
}

$(document).ready(function(){
    $('#storage-content').delegate('.file-holder', 'enter', function(event, url) {

    	if ( DEBUG_MODE )
        	log("Wants to update listeners at url " + url + " But isEntering: " + isEntering);
        if ( isEntering )
            return;
        isEntering = true;

        var fileName = $(this).children('p:first').children('.filename').html();
        fileName = encodeURIComponent(fileName);

        if ( DEBUG_MODE )
        	log("filename: " + fileName);

        var slashLastIndex = url.lastIndexOf('/');
        if ( slashLastIndex < url.length-1 )
            url += '/';
        if ( DEBUG_MODE )
        	log("corrected url: " + url);
        var newUrl = url+fileName;


        var firstDiv = $(this).children('div:first');
        if ( firstDiv.hasClass('folder') && supports_history_api() )
        {
        	if ( DEBUG_MODE )
            	log("Pushing new state with content: " + newUrl);
            goToDir(newUrl);
            return;
        }
        var ext = firstDiv.data('ext');
        if ( ext.length > 0 )
            newUrl += '.' + ext;

        isEntering = false;
        window.location.href = newUrl;
    });

	var mouseSelectionArea = $('.mouse-selection');
	storageContent = $('#storage-content');
	//var orderButtonIcon = $('#order-btn-icon');
	$(this).on('contextmenu', function(event){
		event.preventDefault();
	});

	if ( DEBUG_MODE )
		log("Very first time content loading");
	$('#loading').css('display', 'block');
	getJsonContent(window.location.href, function(data){

		if ( DEBUG_MODE )
			log(data);
		content = data;
		 if ( supports_history_api() )
		 {
			var folders = window.location.href.split('/');
			if ( DEBUG_MODE )
				log("Folders: " + folders.toString());
			var urls = folders[0] + "//" + folders[2];
			history.replaceState(null, null, urls);
			if ( 3 < folders.length && folders[3].length == 2 && folders[3].charAt(1) === ':')
			{
				folders[3] += '/';
				if ( DEBUG_MODE )
				{
					log("Handled windows partition letter");
					log(folders[3]);
				}
			}
			for ( var i = 3; i < folders.length; i++ )
			{
				if ( folders[i].length == 0 )
					continue;

				urls += '/' + folders[i];
				history.pushState(null, null, urls);
				if ( DEBUG_MODE )
					log("Pushed new state: " + urls);
			}
		}
		storageContent.trigger('content-changed', [data, window.location.href]);
		$('#loading').css('display', 'none');
	}, function(err) {
		isEntering = false;
		$('#loading').css('display', 'none');
	});

	prepareStorageContent(storageContent);
	prepareInterface();
});


$(window).load(function () {

	leftBarWidth = getTrueWidthOfDomElement($('#left-bar').get(0));
    headerHeight = $('#header').prop('offsetHeight');
    //INNER_HEIGHT
	//scrollWidth = getScrollbarWidth();
	$('#load-bar').click(function(event){
	    var notifPanel = $('#notif-panel');
	    if ( notifPanel.hasClass('closed') )
	        notifPanel.removeClass('closed').addClass('opened');
        else
            notifPanel.removeClass('opened').addClass('closed');
	});

	$('#storage-content').mousedown(function (event) {
		hideSortMenu();
		closeNotifPanel();

		if ( isLeftButton(event) )
		{
			hideContextMenu();
		}
    });

	$(document.body).mousemove(function (event) {
		if ( DEBUG_MODE && FLOOD )
			log({ x: event.pageX, y: event.pageY });
    })
});

$(window).on('resize', function (event) {
	leftBarWidth = getTrueWidthOfDomElement($('#left-bar').get(0));

});

$(document).on('keyup', function (event) {

    /*if ( event.keyCode >= 65 && event.keyCode <= 92 &&
		!event.ctrlKey && !event.shiftKey && !event.altKey )
    {

    }*/

	if (event.keyCode == ESC) {
        if ( isPopupVisible() )
        {
        	$('.cd-popup').trigger('close');
        	return;
        }

        if ( isNotifPanelActive() )
		{
			closeNotifPanel();
			return;
		}

		if ( isContextMenuOpen() )
		{
			hideContextMenu();
			return;
		}

		var cut = $('.cut');
		if ( cut.length > 0 )
		{
			forgetCut();
		}

		return;
	}

	if ( event.keyCode == DEL )
	{
		if ( isNotifPanelActive() || isPopupVisible() )
			return;

		$('#delete-tool-item').trigger('click');

		return;
	}

	if (event.keyCode == ENTER) {

        if (isPopupVisible()) {
            var buttons = $('.cd-popup.is-visible').find('.cd-button.confirm');
            if (buttons.length > 0) {
                buttons.first().trigger('click');
            }
            return;
        }

		var selected = $('.selected');
		if ( selected.length != 1 )
			return;

		selected.trigger('enter', [ window.location.href ]);
		return;
	}

    if (event.ctrlKey) {
        if (event.keyCode == C_KEY) {

            if (isPopupVisible()) {
                return;
            }

            var selected = $('.selected');
            if (selected.length > 0) {
                $('.btn-copy').trigger('click');
            }
            return;
        }

        if (event.keyCode == X_KEY) {

            if (isPopupVisible()) {
                return;
            }

            var selected = $('.selected');
            if (selected.length > 0) {
                $('.btn-cut').trigger('click');
            }
            return;
        }

        if (event.keyCode == V_KEY) {

            if (isPopupVisible()) {
                return;
            }

            if (filesToCopy.length > 0) {
                $('.btn-paste').trigger('click');
                return;
            }
        }
    }
});

function supports_history_api() {
  return window.history && history.pushState;
}

$(window).on('popstate', function(event){

	if ( DEBUG_MODE )
    	log("pop");
    goToDir(window.location.href);
    hideContextMenu();
});


$(function(){
    $('#storage-content').on('content-changed', function(jEvent, content){
        itemsCount = content.files.length;
        $('#items-count').html(itemsCount);
        var orderedIndices = null;
        if ( orderType == types.NAME )
        	orderedIndices = ascending ? content.ascName : content.descName;
		else
		if ( orderType == types.DATE )
			orderedIndices = ascending ? content.ascDate : content.descDate;
		else
			orderedIndices = ascending ? content.ascSize : content.descSize;

        redrawFiles(content.files, orderedIndices, $(this));
        $(this).get(0).scrollTop = 0;
    });

    $('.menu > li').click(function(event){
    	hideContextMenu();
    });
})

$(function(){
	$('#notif-panel').delegate('.cancel-button', 'click', function(event){
		var task = $(this).parent();
		var progressBar = task.children('div:first').children('.progress');
		var taskId = task.attr('id').substr('task'.length);
		var button = $(this);
		button.html('&#10003;');
		button.removeClass('cancel-button').addClass('done-button').addClass('cancelled-button');
		progressBar.addClass('cancelled-progress');
		makeAjaxRequest({
			method: "DELETE",
			url: "/wfm/api/v1/tasks/" + taskId,
			success: function(data) {
				var affectedDir = task.data('affectedDir');
				if ( affectedDir && affectedDir == decodeURIComponent(getPath()) )
					goToDir(window.location.href);
			}
		});
	});
	$('#notif-panel').delegate('.done-button', 'click', function(event){
		var task = $(this).parent();
		task.remove();
	});
});

$(function () {
    $('#jstree_demo_div')
    .on('select_node.jstree', function(e, data){
        var idPath = data.instance.get_path(data.node, '/', true);
        var fsPath = data.instance.get_path(data.node, '/');
        var rootIdEndPos = idPath.indexOf('/');
        var rootId = rootIdEndPos == -1 ? idPath : idPath.substr(0, rootIdEndPos);
        var rootNode = $(this).jstree(true).get_node(rootId);
        if ( !endsWith(rootNode.data.path, "/") && !endsWith(rootNode.data.path, "\\") )
        	rootNode.data.path += '/';

        fsPath = rootNode.data.path + fsPath.substr(rootNode.text.length+1);

		if ( DEBUG_MODE )
		{
			log(idPath);
			log(fsPath);
			log(data);
		}
        goToDir(fsPath);
    })
    .jstree({
        'core' : {
            'data' : {
                'url' : '/wfm/api/v1/get_tree',
                'contentType': 'application/json',
                'data': function(node) {
                    var isFirst = initialization;

                    return JSON.stringify({ 'isFirst': isFirst, 'nodeId': node.id });
                },
                'method': 'POST',
                'success': function (a) {
                	if ( DEBUG_MODE )
                		log(a);
					if ( DEBUG_MODE )
					{
						a.forEach(function(item){
							log(item);
						});
					}
                }
            }
        }
    });

    initialization = false;
});

$(function(){
	var _this = null;
    var files = [];
    Dropzone.options.myAwesomeDropzone = {
		paramName: "file", // The name that will be used to transfer the file
		parallelUploads: 1,
		maxFilesize: 16000,
		timeout: 500000,
		success: function(file, done) {
		},
		init: function() {
			this.on("sending", function(file) {
				files.push(file);
			});
			// Using a closure.
			_this = this;
			// Setup the observer for the button.
			//document.querySelector("button#clear-dropzone").addEventObserver("click", function() {
			// Using "_this" here, because "this" doesn't point to the dropzone anymore
			//_this.removeAllFiles();
			// If you want to cancel uploads as well, you
			// could also call _this.removeAllFiles(true);
		}
	};
});

$(function(){
	var orderButtonIcon = $('#order-btn-icon');
	var started = false;
	var timer;
	$('#order-btn').click(function (event) {
		/*var sortAnimate = function(id1, id2){
		  var replacedWith = $(id2);
		  var indexItem = $(id1);
		  var cloneDiv = replacedWith.clone();
		  cloneDiv.css('position', 'absolute');
		  cloneDiv.css('left', replacedWith.prop('offsetLeft')-20);
		  cloneDiv.css('top', replacedWith.prop('offsetTop')-20);
		  cloneDiv.appendTo('#storage-content');
		  replacedWith.css('visibility', 'hidden');
		  var timer = setTimeout(function () {
			  cloneDiv.animate({
				  'left': (indexItem.prop('offsetLeft')-HORIZONTAL_MARGIN) + 'px',
				  'top': (indexItem.prop('offsetTop')-VERTICAL_MARGIN) + 'px'
			  }, 1200, function(){
				  log("Animation finished");
				  replacedWith.css('visibility', 'visible');
				  cloneDiv.remove();
				  clearTimeout(timer);
			  });
		  }, Math.random() * 200);
		};

		var onetone = function (id1, id2) {
		  sortAnimate(id1, id2);
		  sortAnimate(id2, id1);
		};*/

		/*var orderfunc = function(item, index){
			  var fileSelector = '#file' + (index+1);
			  var file = content.files[item];
			  $(fileSelector + ' .name').html(file.name);
			  $(fileSelector + ' .ext').html(file.extension);

			  var fileHolder = $(fileSelector + ' div:first');
			  fileHolder.removeClass(fileHolder.data('ext'));
			  if ( file.type == 1 )
				fileHolder.removeClass('folder');
			  else
				fileHolder.addClass('folder');
			  fileHolder.addClass(file.extension);
			  fileHolder.data('size', file.dataSize);
			  fileHolder.data('size-unit', file.dataSizeUnit);
			  //if ( !animatedFiles.has(item.id) )
				  //return;
			  //onetone(selector, item.id);
		  };*/
		  if ( ascending )
		  {
		  	orderButtonIcon.removeClass('arrow-up').addClass('arrow-down');
		  }
		  else
		  	orderButtonIcon.removeClass('arrow-down').addClass('arrow-up');
		  ascending = !ascending;
		$('#storage-content').trigger('content-changed', [content]);
	});

	$('#sort-by-menu > li').click(function(event){
		var typeText = $(this).text();
		orderType = types[typeText.toUpperCase()];
		$('#storage-content').trigger('content-changed', [content]);
		var sortBtn = $('#sort-by-btn');
		$(this).text(sortBtn.text());
		sortBtn.text(typeText);
	});

});

function getDomain() {
    var parts = window.location.href.split('/');
    return parts[0] + '//' + parts[2];
}

function getPath() {
    return window.location.href.substr(getDomain().length);
}

function goToDir(dirUrl) {
	$('#loading').css('display', 'block');
    getJsonContent(dirUrl, function(data){
		$('#storage-content').trigger('content-changed', [data, dirUrl]);
		$('.last-selected').removeClass('last-selected');
		$('.selected').removeClass('selected');
		if ( window.location.href !== dirUrl )
			history.pushState(null, null, dirUrl);

		if ( DEBUG_MODE )
			log(data);
		content = data;
		isEntering = false;
		$('#loading').css('display', 'none');
	}, function(err) {
		isEntering = false;
		$('#loading').css('display', 'none');
	});
}

function isNotifPanelActive() {
	return $("#notif-panel").hasClass('opened');
}

function closeNotifPanel() {
	$('#notif-panel').removeClass('opened').addClass('closed');
}

function openNatifPanel() {
	$('#notif-panel').removeClass('closed').addClass('opened');
}

function isPopupVisible() {
	return $('.cd-popup').hasClass('is-visible');
}

function endsWith(str1, ending) {
	if ( typeof str1 == typeof "" && typeof ending == typeof "" && str1.length == 0 && ending.length == 0 )
		return true;
	for ( var i = ending.length-1; i >= 0; i-- )
	{
		if ( ending.charAt(i) !== str1.charAt(str1.length - (ending.length-i)) )
			return false;
	}

	return true
}
