const $ = require('jquery');
module.exports = function() {
    var langs = ['en', 'ru', 'az'];
    if ( DEBUG_MODE )
    	log(document.cookie);
    var cookieHeaders = document.cookie.split(';');
    if ( DEBUG_MODE )
    	log(cookieHeaders);
    var lang = null;
    for ( var i = 0; i < cookieHeaders.length; ++i )
    {
        var item = cookieHeaders[i];
        if ( DEBUG_MODE )
        	log(item);
        var assignmentPos = item.indexOf('=');
        if ( assignmentPos == -1 )
            continue;
        var key = item.substr(0, assignmentPos);
        if ( key !== 'lang' )
            continue;
        lang = item.substr(assignmentPos+1);
        break;
    }

    if ( lang == null || !includes(langs, lang) )
        lang = "en";
    if ( DEBUG_MODE )
    	log(lang);
    var selector = "p[lang='" + lang + "']";
    $(function() { $(selector).css('display', 'inherit'); });

    var translate = function(element) {
    	return element.children("span[lang='" + lang + "']");
    }

    function includes(arr, element) {
    	if ( !arr )
    		return false;
    	for ( var i = 0; i < arr.length; i++ )
    	{
    		if ( arr[i] == element )
    			return true;
    	}

    	return false;
    }

    return translate;
}

