import React, { Component } from 'react';

export default class StorageItem extends Component {

  getClassName(props) {
    const classes = ['noselect', 'file'];
    if (props.fileType == 2) {
      classes.push('folder')
    }
    if (props.fileExtension.length > 0) {
      classes.push(props.fileExtension);
    }
    if (props.isSelected) {
      classes.push('selected');
    }
    if (props.isSelectedLast) {
      classes.push('last-selected');
    }

    return classes.join(' ');
  }

  render() {
    return(
      <div id={this.props.htmlDivId} className='file-holder noselect'>
        <div
          className={this.getClassName(this.props)}
          data-size={this.props.fileSize}
          data-size-unit={this.props.fileSizeUnit}
          data-ext={this.props.fileExtension}
          data-name={this.props.fileName} />
        <p>
          <span className="filename">{this.props.fileName}</span>
          <span className="ext">{this.props.fileExtension.length > 0 ? '.' + this.props.fileExtension : ''}</span>
        </p>
      </div>
    )
  }
}
