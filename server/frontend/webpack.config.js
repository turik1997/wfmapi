const PROD = 'production', DEV = 'development', MVN = "maven"
const isProd = process.env.NODE_ENV === PROD;
const isMvn = process.env.NODE_ENV === MVN;
const isDev = process.env.NODE_ENV === DEV;
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const SRC = path.resolve(__dirname, './src');
const DIST = path.resolve(__dirname, './dist');
const CSS_DIR = path.resolve(SRC, './css');
const IMG_DIR = path.resolve(SRC, './images');
const webpack = require('webpack');
const RemoveStrictPlugin = require('remove-strict-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const consolereconfig = require('./consolere.config.js');
const consolere = isProd ? null : require('console-remote-client').connect('console.re','80', consolereconfig.channel);
const VERSION = "1.0.1";
const GLOBALS  = {
  VERSION,
  DEBUG_MODE: false,
  REMOTE_LOG: false
};
adjustGlobalVariables();

function adjustGlobalVariables() {

  if ( !isProd )
  {
    GLOBALS.log = function(msg) {
        if ( REMOTE_LOG )
            console.re.log(msg);
        else
            console.log(msg);
    };
    GLOBALS.consolere = consolere;
  }
  else
  {
    GLOBALS.log = function(msg) {
      console.log(msg);
    };
  };
}

module.exports = {
    optimization: {
        minimize: false,
    },
    entry: {
        index: path.join(SRC, './index.js'),
        loading: path.join(SRC, './loading.js'),
        login: path.join(SRC, './login.js'),
        error: path.join(SRC, './error-page.js')
    },
    devServer: {
      host: '0.0.0.0',
      hot: true,
      open: false,
      compress: true,
      inline: true,
      proxy: {
        '/wfm/web': {
            target: 'http://localhost:8080',
            pathRewrite: {'^/wfm/web' : ''}
        },
        '/wfm/api': 'http://localhost:1234',
        '/home/turik1997/*': 'http://localhost:1234'
      }
    },
    resolve: {
        modules: ['node_modules', CSS_DIR, IMG_DIR]
    },
    output: {
        publicPath: isProd || isMvn ? '/wfm/web/' : '/',
        path: DIST,
        filename: 'js/[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                use: [
                  MiniCssExtractPlugin.loader,
                  "css-loader"
                ]
            },
            {
                test: /\.(png|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'images/',
                            name: '[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new RemoveStrictPlugin(),
        new webpack.NamedModulesPlugin(),
		    new webpack.HotModuleReplacementPlugin(),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/index.html'),
            filename: 'index.html',
            chunks: ['index', 'common'],
            minify: false
        }),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/loading.html'),
            filename: 'loading.html',
            chunks: ['loading', 'common'],
            minify: false
        }),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/login.html'),
            filename: 'login.html',
            chunks: ['login', 'common'],
            minify: false
        }),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/forbidden.html'),
            filename: 'forbidden.html',
            chunks: ['error', 'common'],
            minify: false
        }),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/saftext.html'),
            filename: 'saftext.html',
            chunks: [ 'error', 'common' ],
            minify: false
        }),
        new HTMLWebpackPlugin({
            template: path.resolve(SRC, './views/404.html'),
            filename: '404.html',
            chunks: [ 'error', 'common' ],
            minify: false
        }),
        new MiniCssExtractPlugin({
          filename: "css/[name].bundle.css"
        }),
        new webpack.ProvidePlugin({
    			_: 'underscore'
    		}),
        new webpack.DefinePlugin(GLOBALS)
    ]
};
