var langs = ['en', 'ru', 'az'];
var debugDefined = typeof DEBUG_MODE !== 'undefined';

if ( debugDefined && DEBUG_MODE )
	${js.log}(document.cookie);
var cookieHeaders = document.cookie.split(';');
if ( debugDefined && DEBUG_MODE )
	${js.log}(cookieHeaders);
var lang = null;
for ( var i = 0; i < cookieHeaders.length; ++i )
{
    var item = cookieHeaders[i];
    if ( debugDefined && DEBUG_MODE )
    	${js.log}(item);
    var assignmentPos = item.indexOf('=');
    if ( assignmentPos == -1 )
        continue;
    var key = item.substr(0, assignmentPos);
    if ( key !== 'lang' )
        continue;
    lang = item.substr(assignmentPos+1);
    break;
}

if ( lang == null || !includes(langs, lang) )
    lang = "en";
if ( debugDefined && DEBUG_MODE )
	${js.log}(lang);
var selector = "p[lang='" + lang + "']";
$(function() { $(selector).css('display', 'inherit'); });

function translate(element) {
	return element.children("span[lang='" + lang + "']");
}

function includes(arr, element) {
	if ( !arr )
		return false;
	for ( var i = 0; i < arr.length; i++ )
	{
		if ( arr[i] == element )
			return true;
	}

	return false;
}
