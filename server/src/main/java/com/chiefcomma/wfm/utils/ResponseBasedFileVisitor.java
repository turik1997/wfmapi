package com.chiefcomma.wfm.utils;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ResponseBasedFileVisitor extends SimpleFileVisitor {

	protected FileVisitResult runConflictableCode(Callable<Conflict.Result> code) {
		Conflict.Result result = Conflict.Result.RETRY;
		while ( result == Conflict.Result.RETRY )
		{
			try {
				result = code.call();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		switch ( result )
		{
			case CANCEL:
				return FileVisitResult.TERMINATE;
			case SKIP: case SKIP_ALL:
			{
				return FileVisitResult.SKIP_SUBTREE;
			}
		}


		return FileVisitResult.CONTINUE;
	}


	protected boolean shouldReturn(FileVisitResult result) {
		return result == FileVisitResult.TERMINATE || result == FileVisitResult.SKIP_SUBTREE;
	}
}
