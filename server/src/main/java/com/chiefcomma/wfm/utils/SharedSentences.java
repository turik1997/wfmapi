package com.chiefcomma.wfm.utils;

import java.util.Formatter;
import java.util.Locale;
import java.util.ResourceBundle;

public class SharedSentences {

	public final static String PATH = "com.chiefcomma.wfm.SharedSentences";
	private static LocaleReader localeReader = null;

	public static class Keys {
		public final static String BAD_REQUEST = "bad_request";
		public final static String EMPTY_POST_DATA = "null_post";
		public final static String CREATE = "create";
		public final static String OVERWRITE = "overwrite";
		public final static String OVERWRITE_ALL = "overwrite_all";
		public final static String SKIP = "skip";
		public final static String SKIP_ALL = "skip_all";
		public final static String CANCEL = "cancel";
		public final static String WRITE_INTO = "write_into";
		public final static String WRITE_INTO_ALL = "write_into_all";
		public final static String FILE_EXISTS = "file_exists";
		public final static String RETRY = "retry";
		public final static String FILE_IN_USE = "file_in_use";
		public final static String FILE_NOT_FOUND = "file_doesnt_exist";
		public final static String FROM = "from";
		public final static String TO = "to";
		public final static String FILE_TO_ZIP_FAILED = "file_to_zip_failed";
		public final static String SHOW_SAF = "show_saf";
		public final static String MISSING_FIELD = "missing_field";
	}

	public static String read(String key, String ...params) {
		Locale locale = localeReader != null ? localeReader.getLocale() : new Locale("");
		ResourceBundle bundle = ResourceBundle.getBundle(PATH, locale);
		StringBuilder logBuilder = new StringBuilder();
		Formatter formatter = new Formatter(logBuilder);
		formatter.format(bundle.getString(key), params);

		return logBuilder.toString();
	}

	public static void setLocaleReader(LocaleReader reader) {
		localeReader = reader;
	}
}
