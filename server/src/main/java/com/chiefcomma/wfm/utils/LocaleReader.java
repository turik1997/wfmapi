package com.chiefcomma.wfm.utils;

import java.util.Locale;

public interface LocaleReader {

	Locale getLocale();
}
