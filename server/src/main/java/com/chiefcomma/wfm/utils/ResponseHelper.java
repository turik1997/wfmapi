package com.chiefcomma.wfm.utils;

import com.chiefcomma.wfm.API;
import com.chiefcomma.wfm.WFMWebService;
import com.chiefcomma.wfm.models.SafRequest;
import com.chiefcomma.wfm.response.server.errors.TaskStatusError;
import com.chiefcomma.wfm.server.WFMServer;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.webservice.api.response.ErrorAction;
import com.chiefcomma.wfm.webservice.api.response.ErrorResponse;
import com.google.gson.Gson;
import org.nanohttpd.protocols.http.request.Method;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.util.Arrays;

import static com.chiefcomma.wfm.server.WFMServer.API_BUNDLE_PATH;

public class ResponseHelper {

    private final static Gson gson = new Gson();

    public static Response errorWrongApi(AssetManager assetManager, String uri) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.WRONG_API, uri);
        ErrorResponse error = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorNoHeaderReferer(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.NO_HEADER, "referer");
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorNoHeaderHost(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.NO_HEADER, "host");
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorHeaderMismatch(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.HOST_REF_MISMATCH);
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response internalError(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, "something_went_wrong");
        String reason = assetManager.readBundle(API_BUNDLE_PATH, "could_not_read_input");
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.INTERNAL_ERROR, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorBadDestination(AssetManager assetManager, String dest) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.CANT_HANDLE);
        String reason = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, "destination_doesnt_exist", dest);
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorCantCopyToItself(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.CANT_HANDLE);
        String reason = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, "cant_copy_to_itself");
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.CONFLICT, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorCantHandleFileUpload(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.CANT_HANDLE);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.NO_TMP_FILE_PATH);
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.INTERNAL_ERROR, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorEmptyPostData(AssetManager assetManager) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.EMPTY_POST_DATA);
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorNegativeTaskId(AssetManager assetManager, String taskId) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.TASK_READ_ERR);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.NEGATIVE_ID, taskId);
        TaskStatusError error = new TaskStatusError(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.NOT_FOUND, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorBadTaskId(AssetManager assetManager, String taskId) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.TASK_READ_ERR);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.BAD_ID, taskId);
        TaskStatusError error = new TaskStatusError(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorFileNotFoundForDownload(AssetManager assetManager, String fileName, String fileDir) {
        String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, WFMServer.ApiBundleKey.DOWNLOAD_ERR);
        String reason = assetManager.readBundle(
                WFMWebService.API_BUNDLE_PATH,
                WFMWebService.ApiBundleKey.FILE_DOESNT_EXIST,
                fileName,
                fileDir
        );
        TaskStatusError error = new TaskStatusError(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorFileNotFound(AssetManager assetManager, String fileName, String parentPath) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMWebService.ApiBundleKey.FILE_DOESNT_EXIST, fileName, parentPath);
        ErrorResponse error = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.NOT_FOUND, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorAccessDenied(AssetManager assetManager, String filePath) {
        String errorMsg = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, "cant_access_file");
        String reason = assetManager.readBundle(
                WFMWebService.API_BUNDLE_PATH,
                "access_denied",
                filePath
        );
        TaskStatusError error = new TaskStatusError(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.FORBIDDEN, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorStorageAccessFrameworkNeeded(AssetManager assetManager, String dirPath) {
        String errorMsg = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, "saf_needed");
        String reason = null;
        ErrorResponse error = new ErrorResponse(errorMsg, reason);
        error.setErrorType("saf");
        ErrorAction cancelAction = new ErrorAction();
        cancelAction.setActionText("Cancel");
        cancelAction.setActionDescription("Click to ignore the message");
        cancelAction.setActionType(Conflict.Action.CANCEL);
        ErrorAction continueAction = new ErrorAction();
        continueAction.setActionText("Continue");
        continueAction.setActionText("Click when the application is open and ready to continue");
        continueAction.setActionType(Conflict.Action.CONTINUE);
        continueAction.setUrl(API.API_URL + API.CALLBACK + "/saf");
        continueAction.setHttpMethod(Method.POST);
        SafRequest safRequest = new SafRequest();
        safRequest.setDirectoryPath(dirPath);
        continueAction.setDataToSubmit(gson.toJson(safRequest));
        error.setActions(Arrays.asList(
                cancelAction,
                continueAction
        ));
        return Response.newFixedLengthResponse(Status.CONFLICT, MimeTypes.JSON, gson.toJson(error));
    }

    public static Response errorMissingField(AssetManager assetManager, String fieldName) {
        String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
        String reason = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.MISSING_FIELD, fieldName);
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(errorResponse));
    }

    public static Response errorBadName(AssetManager assetManager, String fileName) {
        String error = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, WFMWebService.ApiBundleKey.DIR_NOT_CREATED);
        String reason = assetManager.readBundle(WFMWebService.API_BUNDLE_PATH, WFMWebService.ApiBundleKey.BAD_NAME, fileName);
        ErrorResponse response = new ErrorResponse(error, reason);
        return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(response));
    }

    public static Response errorDirExists(AssetManager assetManager, String dirName) {
        String error = assetManager.readBundle(API_BUNDLE_PATH, WFMWebService.ApiBundleKey.DIR_NOT_CREATED, dirName);
        String reason = assetManager.readBundle(API_BUNDLE_PATH, WFMWebService.ApiBundleKey.DIR_EXISTS, dirName);
        ErrorResponse response = new ErrorResponse(error, reason);
        return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(response));
    }
}
