package com.chiefcomma.wfm.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class PathSplitter {

	public ArrayList<String> split(File file) {
		String path = file.getAbsolutePath();
		if ( path.startsWith("/") && path.charAt(1) != ':' && path.charAt(2) != '\\' )
			return splitLinuxPath(path);
		return splitWindowsPath(path);
	}

	private ArrayList<String> splitWindowsPath(String path) {
		String[] parts = path.split("\\\\");
		return new ArrayList<String>(Arrays.asList(parts));
	}

	private ArrayList<String> splitLinuxPath(String path) {
		String[] parts = path.split("/");
		return new ArrayList<String>(Arrays.asList(parts));
	}
}
