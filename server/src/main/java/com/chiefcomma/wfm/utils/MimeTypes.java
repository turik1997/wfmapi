package com.chiefcomma.wfm.utils;

import org.nanohttpd.protocols.http.NanoHTTPD;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLConnection;

public class MimeTypes {

	public final static String JSON = "application/json";
	public final static String HTML = "text/html";
	public final static String CSS = "text/css";
	public final static String JS = "application/javascript";
	public final static String PLAIN_TEXT = "text/plain";
	public final static String OCTET_STREAM = "application/octet-stream";
	public final static String ZIP = "application/zip";

	public String getMime(File file) {
		FileInputStream fis = null;
		String mime = null;
		try {
			fis = new FileInputStream(file);
			mime = URLConnection.guessContentTypeFromStream(fis);
		} catch (Exception e) {
			return OCTET_STREAM;
		} finally {
			NanoHTTPD.safeClose(fis);
		}

		return mime == null ? (mime = URLConnection.guessContentTypeFromName(file.getName()) == null ? OCTET_STREAM : mime) : mime;
	}
}
