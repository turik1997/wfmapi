package com.chiefcomma.wfm.utils;

import java.io.File;
import java.io.IOException;

/**
 * Created by turok on 9/14/17.
 */

public interface FileVisitor {

    FileVisitResult preVisitDirectory(File dir)
            throws IOException;

    FileVisitResult visitFile(File file)
            throws IOException;

    FileVisitResult visitFileFailed(File file, IOException exc)
            throws IOException;

    FileVisitResult postVisitDirectory(File dir, IOException exc)
            throws IOException;

    void cancel() throws IOException;
}

