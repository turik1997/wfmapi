package com.chiefcomma.wfm.utils;

import java.util.Formatter;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaledReader {

	private static LocaleReader localeReader = null;
	private static Locale defaultLocale = new Locale("");
	public static String readLocaleString(String path, String key, String ...params) {
		ResourceBundle bundle = ResourceBundle.getBundle(path, localeReader != null ? localeReader.getLocale() : defaultLocale);
		StringBuilder logBuilder = new StringBuilder();
		Formatter formatter = new Formatter(logBuilder);
		formatter.format(bundle.getString(key), params);

		return logBuilder.toString();
	}


	public static void setLocaleReader(LocaleReader reader) {
		localeReader = reader;
	}
}
