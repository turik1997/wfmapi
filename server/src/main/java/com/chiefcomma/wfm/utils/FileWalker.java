package com.chiefcomma.wfm.utils;

import com.chiefcomma.wfm.tasks.interfaces.Cancelable;

import java.io.File;
import java.io.IOException;

/**
 * Created by turok on 9/14/17.
 */

public class FileWalker implements Cancelable {

    private boolean isCancelled = false;

    @Override
    public void cancel() {
        isCancelled = true;
    }

    public FileVisitResult walkTree(File start, FileVisitor visitor) throws IOException {

        if ( isCancelled )
        {
            return FileVisitResult.TERMINATE;
        }

        FileVisitResult result = FileVisitResult.CONTINUE;
        if ( !start.isDirectory() )
        {
            try {
                return visitor.visitFile(start);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File[] files = start.listFiles();
        result = visitor.preVisitDirectory(start);
        if ( result != FileVisitResult.CONTINUE )
            return result;

        for ( int i = 0; i < files.length && !isCancelled; i++ )
        {
            File file = files[i];
            if ( !file.isDirectory() )
            {
                result = visitor.visitFile(file);
                switch ( result )
                {
                    case TERMINATE:
                        return FileVisitResult.TERMINATE;
                }

                continue;
            }

            result = walkTree(file, visitor);
            switch ( result )
            {
                case TERMINATE:
                    return FileVisitResult.TERMINATE;
            }
        }

        if ( isCancelled )
            return FileVisitResult.TERMINATE;

        return visitor.postVisitDirectory(start, null);
    }
}
