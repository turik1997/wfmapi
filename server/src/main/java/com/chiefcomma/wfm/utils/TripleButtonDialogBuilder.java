package com.chiefcomma.wfm.utils;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;

public class TripleButtonDialogBuilder extends ConflictDialog.Builder {

	private static ConflictDialog.Builder builder = null;

	private TripleButtonDialogBuilder(){}

	public static ConflictDialog.Builder getInstance() {
		if ( builder == null )
		{
			builder = new ConflictDialog.Builder();
			builder = builder
					.addButton(Conflict.Action.RETRY, SharedSentences.read(SharedSentences.Keys.RETRY))
					.addButton(Conflict.Action.SKIP, SharedSentences.read(SharedSentences.Keys.SKIP))
					.addButton(Conflict.Action.CANCEL, SharedSentences.read(SharedSentences.Keys.CANCEL));
		}

		return builder;
	}
}
