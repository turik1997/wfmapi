package com.chiefcomma.wfm.utils;

import java.io.File;
import java.io.IOException;

/**
 * Created by turok on 9/14/17.
 */

public class SimpleFileVisitor implements FileVisitor {
    /**
     * Initializes a new instance of this class.
     */

    private boolean isCancelled = false;

    protected SimpleFileVisitor() {
    }

    /**
     * Invoked for a directory before entries in the directory are visited.
     *
     * <p> Unless overridden, this method returns {@link FileVisitResult#CONTINUE
     * CONTINUE}.
     */
    @Override
    public FileVisitResult preVisitDirectory(File dir) throws IOException {

        requireNotNull(dir);
        return FileVisitResult.CONTINUE;
    }

    /**
     * Invoked for a file in a directory.
     *
     * <p> Unless overridden, this method returns {@link FileVisitResult#CONTINUE
     * CONTINUE}.
     */
    @Override
    public FileVisitResult visitFile(File file) throws IOException {
        requireNotNull(file);
        return FileVisitResult.CONTINUE;
    }

    /**
     * Invoked for a file that could not be visited.
     *
     * <p> Unless overridden, this method re-throws the I/O exception that prevented
     * the file from being visited.
     */
    @Override
    public FileVisitResult visitFileFailed(File file, IOException exc) throws IOException {
        requireNotNull(file);
        throw exc;
    }

    /**
     * Invoked for a directory after entries in the directory, and all of their
     * descendants, have been visited.
     *
     * <p> Unless overridden, this method returns {@link FileVisitResult#CONTINUE
     * CONTINUE} if the directory iteration completes without an I/O exception;
     * otherwise this method re-throws the I/O exception that caused the iteration
     * of the directory to terminate prematurely.
     */
    @Override
    public FileVisitResult postVisitDirectory(File dir, IOException exc) throws IOException {

        requireNotNull(dir);
        if ( exc != null ) throw exc;
        return FileVisitResult.CONTINUE;
    }

    @Override
    public void cancel() throws IOException {
        isCancelled = true;
    }

    protected boolean isCancelled() {
        return this.isCancelled;
    }

    private void requireNotNull(Object o) {
        if ( o == null ) throw new NullPointerException();
    }
}
