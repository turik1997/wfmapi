package com.chiefcomma.wfm.utils;

import com.chiefcomma.wfm.API;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.io.InputStream;
import java.util.Locale;
import java.util.ResourceBundle;

public class AssetManager {

    private Locale locale;

    public AssetManager(Locale locale) {
        this.locale = locale;
    }

    public InputStream getAssetFile(String path) {
        InputStream loadingPageStream = this.getClass().getClassLoader().getResourceAsStream(path);
        return loadingPageStream;
    }

    public Response getResource(String uri) {
        String mimeType = uri.contains("css")
                ? MimeTypes.CSS
                : uri.contains("js")
                    ? MimeTypes.JS
                    : uri.contains("html")
                        ? MimeTypes.HTML
                        : "image/png";
        String fileName = uri.substring((API.PREFIX + "/").length());
        return Response.newChunkedResponse(Status.OK, mimeType, getAssetFile(fileName));
    }

    public String readBundle(String path, String key, Locale locale, String ...params) {
        ResourceBundle bundle = ResourceBundle.getBundle(path, locale);
        return String.format(bundle.getString(key), params);
    }

    public String readBundle(String path, String key, String ...params) {
        return readBundle(path, key, locale, params);
    }
}
