package com.chiefcomma.wfm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public interface SafFileManager {
    boolean mkdir(File file);
    boolean renameTo(File originalFile, String newName);
    boolean canWrite(File file);
    boolean delete(File file);
    InputStream getInputStream(File file) throws FileNotFoundException;
    OutputStream getOutputStream(File file) throws FileNotFoundException;
}
