package com.chiefcomma.wfm;

import com.chiefcomma.wfm.implementations.tasks.ApiTaskExecutor;
import com.chiefcomma.wfm.interfaces.environment.EnvironmentInformer;
import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;
import com.chiefcomma.wfm.interfaces.environment.Notifier;
import com.chiefcomma.wfm.interfaces.tasks.TaskExecutor;
import com.chiefcomma.wfm.interfaces.users.validators.UserAccessValidator;
import com.chiefcomma.wfm.interfaces.users.validators.UserAuthValidator;
import com.chiefcomma.wfm.models.SafRequest;
import com.chiefcomma.wfm.request.*;
import com.chiefcomma.wfm.response.TaskStatus;
import com.chiefcomma.wfm.response.api.DownloadResponse;
import com.chiefcomma.wfm.server.WFMServer;
import com.chiefcomma.wfm.server.servlet.security_layer.SecureServer;
import com.chiefcomma.wfm.tasks.building.TaskFactory;
import com.chiefcomma.wfm.tasks.building.Task;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.utils.*;
import com.chiefcomma.wfm.webservice.IdToPathMapper;
import com.chiefcomma.wfm.webservice.Mapper;
import com.chiefcomma.wfm.webservice.NodeData;
import com.chiefcomma.wfm.webservice.PathToIdMapper;
import com.chiefcomma.wfm.webservice.api.response.DirContent;
import com.chiefcomma.wfm.webservice.api.response.ErrorResponse;
import com.chiefcomma.wfm.webservice.api.response.ui_tree_node.UiNode;
import com.chiefcomma.wfm.webservice.api.response.uifile.UiFile;
import com.chiefcomma.wfm.webservice.api.response.uifile.UiFileNameComparator;
import com.google.gson.Gson;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.io.*;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by turok on 9/8/17.
 */

public class WFMWebService extends WFMServer {

    private TaskExecutor taskExecutor;
    private Mapper<String, Integer> pathIdMapper;
    private Mapper<Integer, String> idToPathMapper;
    private EnvironmentInformer environmentInformer;
    private AssetManager assetManager;
    private SafFileManager safFileManager;
    private FileStatusReader fileStatusReader;
    private RequestValidator requestValidator;
    private Gson gson = new Gson();
    //private final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(WFMWebService.class.getName());
    private final static String LOGS_BUNDLE_PATH = "com.chiefcomma.wfm." + "ApiLogs";
    public final static String API_BUNDLE_PATH = "com.chiefcomma.wfm." + "ApiResponses";

    private TaskFactory taskFactory;
    private final SecureServer secureServer;

    public WFMWebService(
            ConfigProvider configProvider,
            UserAccessValidator accessValidator,
            UserAuthValidator authValidator,
            EnvironmentInformer environmentInformer,
            RequestValidator requestValidator,
            Notifier notifier,
            SecureServer secureServer
    ) {
        super(
                configProvider,
                secureServer,
                environmentInformer.getLocale(),
                requestValidator,
                environmentInformer);
        this.secureServer = secureServer;
        this.assetManager = requestValidator.getAssetManager();
        this.requestValidator = requestValidator;
        this.taskExecutor = new ApiTaskExecutor();
        this.environmentInformer = environmentInformer;
        pathIdMapper = new PathToIdMapper(null);
        idToPathMapper = new IdToPathMapper();
        SharedSentences.setLocaleReader(environmentInformer);
        this.taskFactory = new TaskFactory();
    }


    /** Should be called before starting the server
     * @param fileStatusReader Returns SAF status if the provided file can only be access through SAF
     * @param safFileManager Wrapper for working with SAF through URIs
     */
    public void enableStorageAccessFrameworkSupport(FileStatusReader fileStatusReader, SafFileManager safFileManager) {
        AbstractFile.init(fileStatusReader, safFileManager);
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        if (this.isAlive()) {
            throw new IllegalStateException("Task executor should be replaced only when the server is shut down");
        }
        this.taskExecutor = taskExecutor;
    }

    @Override
    protected Response zip(String zipName, ArrayList<String> fileNames, File srcDir, String clientIp) {
        File zipFile = new File(srcDir, zipName);
        if ( zipFile.exists() )
        {
            //todo error file already exists
            return null;
        }

        FileOutputStream destination = null;
        try {
            destination = new FileOutputStream(zipFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Task zipTask = taskFactory.buildZipTask(fileNames, srcDir, destination);
        int taskId = taskExecutor.execute(zipTask);

        //todo correct response
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, "{ \"taskId\": \"" + taskId + "\" } ");
    }

    @Override
    protected Response download(DownloadRequest request, String ip) {
        ArrayList<String> fileNames = request.getFiles();
        if (fileNames == null) {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.FILES_NULL);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        String srcPath = request.getSrcPath();
        if ( srcPath == null )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.SRC_PATH_NULL);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        File srcDir = new File(srcPath);
        if ( !fileExists(srcDir) )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.SRC_DOESNT_EXIST, srcPath);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(error));
        }

        if ( fileNames.size() == 0 )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.FILES_EMPTY);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        if (fileNames.size() == 1 && !request.isZip()) {
            File file = new File(srcDir, fileNames.get(0));
            if (!file.isDirectory()) {
                String url = null;
                try {
                    url = downloadManager.makeDownloadUrl(ip, file);
                } catch (FileNotFoundException e) {
                    return ResponseHelper.errorFileNotFoundForDownload(assetManager, file.getName(), file.getParentFile().getAbsolutePath());
                }
                DownloadResponse downloadResponse = new DownloadResponse(url);
                return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(downloadResponse));
            }
        }

        for ( String fileName : fileNames )
        {
            File file = new File(srcDir, fileName);
            if ( !fileExists(file) )
            {
                String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
                String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.FILE_DOESNT_EXIST, fileName, srcPath);
                ErrorResponse error = new ErrorResponse(errorMsg, reason);
                return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(error));
            }
        }

        PipedOutputStream sharedOutputStream = new PipedOutputStream();
        final Task zipTask = taskFactory.buildZipTask(fileNames, srcDir, sharedOutputStream);
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                zipTask.run();
            }
        }).start();*/

        PipedInputStream inputStream = null;
        try {
            inputStream = new PipedInputStream(sharedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        taskExecutor.execute(zipTask);
        Response zipResponse = Response.newChunkedResponse(Status.OK, MimeTypes.ZIP, inputStream);
        String downloadUrl = downloadManager.makeDownloadUrl(ip, zipResponse, "wfm-" + Calendar.getInstance().getTimeInMillis() + ".zip");
        DownloadResponse downloadResponse = new DownloadResponse(downloadUrl);

        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(downloadResponse));
    }

    @Override
    protected Response getTasks(String ip) {
        TaskStatus[] statuses = taskExecutor.runningTasks();
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(statuses));
    }

    @Override
    protected Response getDirContent(File dir) {
        if (!fileExists(dir)) {
            String responseJson = gson.toJson(new DirContent(
                    new UiFile[]{},
                    new Integer[]{},
                    new Integer[]{},
                    new Integer[]{},
                    new Integer[]{},
                    new Integer[]{},
                    new Integer[]{}
            ));
            Response r = Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, responseJson);
            r.addHeader("Vary", "Accept");
            return r;
        }
        File[] files = dir.listFiles();
        if (files == null ) {
            LOG.log(Level.WARNING, "Cannot get content of directory" + dir.getAbsolutePath());
            //TODO return cannot access the folder (root or other reason)
            return  null;
        }

        UiFile[] responseFiles = new UiFile[files.length];
        Integer ascendingNames[] = new Integer[files.length];
        Integer descendingNames[] = new Integer[files.length];
        Integer ascendingSize[] = new Integer[files.length];
        Integer descSize[] = new Integer[files.length];
        Integer ascDate[] = new Integer[files.length];
        Integer descDate[] = new Integer[files.length];
        UiFile.UiFileFactory fileFactory = new UiFile.UiFileFactory();
        for ( int i = 0; i < files.length; i++ )
        {
            File f = files[i];
            UiFile uiFile = f.isDirectory() ? fileFactory.buildDir(f) : fileFactory.buildFile(f);
            uiFile.roundToHighestUnit();
            responseFiles[i] = uiFile;
            ascendingNames[i] = descendingNames[i] = ascendingSize[i] = descSize[i] = ascDate[i] = descDate[i] = i;
        }

        Arrays.sort(ascendingNames, new UiFileNameComparator(responseFiles));
        Arrays.sort(descendingNames, new UiFileNameComparator(responseFiles, false, UiFile.NAME));
        Arrays.sort(ascendingSize, new UiFileNameComparator(responseFiles, true, UiFile.SIZE));
        Arrays.sort(descSize, new UiFileNameComparator(responseFiles, false, UiFile.SIZE));
        Arrays.sort(ascDate, new UiFileNameComparator(responseFiles, true, UiFile.DATE));
        Arrays.sort(descDate, new UiFileNameComparator(responseFiles, false, UiFile.DATE));

        String responseJson = gson.toJson(new DirContent(responseFiles, ascendingNames, descendingNames, ascendingSize, descSize, ascDate, descDate));
        Response r = Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, responseJson);
        r.addHeader("Vary", "Accept");
        return r;
    }

    @Override
    protected Response getTree(TreeRequest request) {
        ArrayList<UiNode> nodes = new ArrayList<UiNode>();
        if (request.isFirst()) {
            String internalStoragePath = secureServer.getDefaultPath();
            Integer id = pathIdMapper.map(internalStoragePath);
            idToPathMapper.set(id, internalStoragePath);
            List<String> listOfStorages = environmentInformer.getListOfStorages();
            if (!listOfStorages.isEmpty()) {
                nodes.add(new UiNode(id.toString(), "Device Storage", "#", "/wfm/web/images/internal.png", new NodeData(internalStoragePath)));
            }
            for (int i = 0; i < listOfStorages.size(); i++) {
                String storagePath = listOfStorages.get(i);
                if (internalStoragePath.equals(storagePath)) {
                    continue;
                }
                id = pathIdMapper.map(storagePath);
                idToPathMapper.set(id, storagePath);
                nodes.add(new UiNode(id.toString(), "SD Memory Card" + (nodes.size() > 1 ? " #" + (i+1) : ""), "#", "/wfm/web/images/external.png", new NodeData(storagePath)));
            }
            return Response.newFixedLengthResponse(Status.OK, "application/json", gson.toJson(nodes));
        }

        String nodeId = request.getNodeId();
        if ( nodeId == null )
        {
            return getErrorResponse(Status.BAD_REQUEST, "Cannot handle tree request", "Path is null");
        }

        Integer id = null;
        try
        {
            id = Integer.parseInt(nodeId);
        } catch (NumberFormatException ex) {
            return getErrorResponse(Status.BAD_REQUEST, "Cannot handle tree request", "Id should be an integer");
        }

        /*
        if ( start == null )
        {
            return getErrorResponse(Status.BAD_REQUEST, "Cannot handle tree request", "Start path is null");
        }*/

        /*if ( !end.startsWith(start) )
        {
            return getErrorResponse(Status.PRECONDITION_FAILED, "Cannot handle tree request", "End path is not a child of start path");
        }*/

        String path = idToPathMapper.map(id);
        if ( path == null )
        {
            return getErrorResponse(Status.BAD_REQUEST, "Wrong id", "Such node doesn't exist");
        }

        File dir = new File(path);
        if ( !fileExists(dir) )
        {
            return getErrorResponse(Status.PRECONDITION_FAILED, dir.getAbsolutePath() + " doesn't exist", null);
        }

        if ( !dir.isDirectory() )
        {
            return getErrorResponse(Status.PRECONDITION_FAILED, "Cannot handle tree request", dir.getAbsolutePath() + " is a file, not a directory");
        }

        File[] files = dir.listFiles();
        if ( files == null )
            return getErrorResponse(Status.INTERNAL_ERROR, "Cannot read the contents of the " + dir.getAbsolutePath() + " directory", "Root access needed");

        for ( File file : files )
        {
            if ( !file.isDirectory() )
                continue;

            id = pathIdMapper.map(file.getAbsolutePath());
            idToPathMapper.set(id, file.getAbsolutePath());
            nodes.add(new UiNode(id.toString(), file.getName(), pathIdMapper.map(file.getParent()).toString()));
        }
        //Log.d("TREE", System.getenv("SECONDARY_STORAGE"));

        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(nodes));
        /*

        File startFile = new File(start);
        File endFile = new File(end);

        if ( !startFile.exists() || !endFile.exists() )
        {
            return getErrorResponse(Status.PRECONDITION_FAILED, "Cannot handle the tree reqeust", "Start file or end file doesn't exist");
        }

        if ( !startFile.isDirectory() || !endFile.isDirectory() )
        {
            return getErrorResponse(Status.PRECONDITION_FAILED, "Cannot handle the tree request", "Start or end is not a folder");
        }

        NodesGenerator generator = new NodesGenerator();*/

        /*File[] files = currentDir.listFiles();
        if ( files == null )
            break;
        for ( File file : files )
        {
            if ( !file.isDirectory() )
                continue;
            String path = file.getAbsolutePath();
            String id = pathToId.get(path);
            if ( id == null )
            {
                String value = Integer.toString(pathToId.size());
                pathToId.put(path, value);
                id = value;
            }

            String parent = file.getParent();

            nodes.add(new UiNode(id, file.getName(), pathToId.get(parent)));
        }*/

        //return null;
    }


    @Override
    protected Response fileUpload(FileUploadRequest fileUploadRequest) {
        File tmpUploadedFile = new File(fileUploadRequest.getRawFilePath());
        String fileName = fileUploadRequest.getNewFileName();
        if (fileName != null) {
            File renamedTmpUploadedFile = new File(tmpUploadedFile.getParent(), fileName);
            if (renamedTmpUploadedFile.exists()) {
                if (!renamedTmpUploadedFile.delete()) {
                    return ResponseHelper.internalError(assetManager);
                }
            }
            if (!tmpUploadedFile.renameTo(renamedTmpUploadedFile)) {
                return ResponseHelper.internalError(assetManager);
            }
            fileUploadRequest.setRawFilePath(renamedTmpUploadedFile.getAbsolutePath());
            tmpUploadedFile = renamedTmpUploadedFile;
        }

        Task task = taskFactory.buildMoveTask(
                Arrays.asList(tmpUploadedFile.getName()),
                new File(fileUploadRequest.getDestPath()),
                tmpUploadedFile.getParentFile()
        );
        int id = taskExecutor.execute(task);
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, "{ \"taskId\": \"" + id + "\" }");
    }

    @Override
    protected Response getTaskInfo(String clientIp, int id) {
        TaskStatus status = taskExecutor.getTaskStatus(id);
        if ( status == null )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.TASK_DOESNT_EXIST, Integer.toString(id));
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.NOT_FOUND, MimeTypes.JSON, gson.toJson(error));
        }

        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(status));
    }

    @Override
    protected Response updateTask(TaskUpdateRequest request, String clientIp, int id) {
        TaskStatus status = taskExecutor.getTaskStatus(id);
        if ( status == null )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.TASK_DOESNT_EXIST, Integer.toString(id));
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.NOT_FOUND, MimeTypes.JSON, gson.toJson(error));
        }

        if ( !status.hasConflict() )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.NOT_IN_CONFLICT);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        if ( request == null )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.EMPTY_POST_DATA);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        String actionName = request.getAction();
        if ( actionName == null )
        {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.ACTION_NULL);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        Conflict.Action action = null;
        try
        {
            action = Conflict.Action.valueOf(actionName);
        } catch (IllegalArgumentException ex) {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.ACTION_DOESNT_EXIST);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        taskExecutor.resolveConflict(id, action, request.getData());
        return Response.newFixedLengthResponse(Status.OK, "", "");
    }

    @Override
    protected Response doCopy(CopyRequest request, String clientIp) {
        try {
            requestValidator.validateCopyRequest(request);
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }

        Task copyTask = taskFactory.buildCopyTask(
                request.getFiles(),
                new File(request.getDestPath()),
                new File(request.getSrcPath())
        );

        int id = taskExecutor.execute(copyTask);
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, "{ \"taskId\": \"" + id + "\" }");
    }

    @Override
    protected Response doMove(CopyRequest request, String clientIp) {
        try {
            requestValidator.validateCopyRequest(request);
            requestValidator.validateDeleteRequest(new DeleteRequest(request.getFiles(), request.getSrcPath()));
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }
        String srcPath = request.getSrcPath();
        ArrayList<String> originalFiles = request.getFiles();
        for ( String file : originalFiles )
        {
            File f = new File(srcPath, file);
            //Log.d("File to copy", f.getAbsolutePath());

            if ( !f.exists() )
            {
                //todo return error
                return null;
            }
        }
        Task moveTask = taskFactory.buildMoveTask(originalFiles, new File(request.getDestPath()), new File(srcPath));

        int id = taskExecutor.execute(moveTask);

        return Response.newFixedLengthResponse(Status.OK, "application/json", "{ \"taskId\": \"" + id + "\" }");
    }

    @Override
    protected Response doDelete(DeleteRequest request, String clientIp) {
        try {
            requestValidator.validateDeleteRequest(request);
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }

        Task deleteTask = taskFactory.buildDeleteTask(request.getFileNames(), new File(request.getSrcPath()));
        int execId = taskExecutor.execute(deleteTask);
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, "{ \"taskId\": \"" + execId + "\" }");
    }

    @Override
    protected Response makeDir(MakeDirRequest request, File srcDir) {
        try {
            requestValidator.validateMakeDirRequest(request, srcDir);
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }

        String fileName = request.getDirName();
        File destDir = new File(srcDir, fileName);
        if (!AbstractFile.getInstance().mkdir(destDir)) {
            return getErrorResponse(Status.INTERNAL_ERROR, "Cannot create dir with name " + destDir.getName(), null);
        }

        return getDirContent(srcDir);
    }

    @Override
    protected Response getVacantDirName(String srcDirPath) {

        File srcDir = new File(srcDirPath);
        if ( !srcDir.exists() )
        {
            return getErrorResponse(Status.EXPECTATION_FAILED, "Current directory " + srcDir.getAbsolutePath() + " doesn't exist. Refresh the page", null);
        }

        String name = "New folder";
        String pattern = name + " (%d)";
        StringBuilder builder = new StringBuilder();
        Formatter formatter = new Formatter(builder);
        File file = new File(srcDir, name);
        for ( int i = 1; file.exists(); i++ )
        {
            builder.setLength(0);
            formatter.format(pattern, i);
            file = new File(srcDir, builder.toString());
        }

        return Response.newFixedLengthResponse(Status.OK, "application/json", "{ \"name\": \"" + file.getName() + "\"}");
    }

    @Override
    protected Response doRename(RenameRequest request) {
        String path = request.getPath();
        if (path == null) {
            String errorMsg = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.BAD_REQUEST);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.PATH_NULL);
            ErrorResponse error = new ErrorResponse(errorMsg, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(error));
        }

        String name = request.getName();
        if (name == null) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.NULL_NAME);
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(response));
        }

        String fileName = request.getFileName();
        if (fileName == null) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.FILES_NULL);
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(response));
        }

        if (name.contains("/")) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.BAD_NAME, name);
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.BAD_REQUEST, MimeTypes.JSON, gson.toJson(response));
        }

        File file = new File(path, fileName);
        if (!fileExists(file)) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.FILE_NOT_FOUND, file.getAbsolutePath());
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(response));
        }

        File parent = file.getParentFile();
        File resultFile = new File(parent, name);
        if (resultFile.exists()) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = assetManager.readBundle(SharedSentences.PATH, SharedSentences.Keys.FILE_EXISTS, name, parent.getAbsolutePath());
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.EXPECTATION_FAILED, MimeTypes.JSON, gson.toJson(response));
        }

        boolean isRenamed = AbstractFile.getInstance().renameTo(file, resultFile.getName());
        if (!isRenamed) {
            String error = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.CANT_RENAME);
            String reason = null;
            ErrorResponse response = new ErrorResponse(error, reason);
            return Response.newFixedLengthResponse(Status.INTERNAL_ERROR, MimeTypes.JSON, gson.toJson(response));
        }

        return getDirContent(file.getParentFile());
    }

    @Override
    protected Response doCallback(CallbackRequest callbackRequest) {
        try {
            requestValidator.validateCallbackRequest(callbackRequest);
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }
        if (API.STORAGE_ACCESS_FRAMEWORK.equalsIgnoreCase(callbackRequest.getType())) {
            SafRequest safRequest = gson.fromJson(callbackRequest.getPostData(), SafRequest.class);
            try {
                requestValidator.validateSafCallbackRequest(safRequest);
            } catch (ValidationException e) {
                return e.getErrorResponse();
            }
        }

        return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, "{ \"status\": \"ok\" }");
    }

    @Override
    protected Response cancelTask(String clientIp, int id) {
        TaskStatus status = taskExecutor.getTaskStatus(id);
        if ( status == null )
        {

            return Response.newFixedLengthResponse(Status.NOT_FOUND, "application/json", "{ \"error\": \"task " + id + " doesn't exist\"}");
        }
        taskExecutor.stop(id);
        return Response.newFixedLengthResponse(Status.OK, MimeTypes.PLAIN_TEXT, "");
    }

    private boolean fileExists(File file) {
        List<String> listOfStorages = environmentInformer.getListOfStorages();
        for (String storageVolumePath : listOfStorages) {
            if (file.getAbsolutePath().startsWith(storageVolumePath) && file.exists()) {
                return true;
            }
        }
        return false;
    }

    public static class ApiBundleKey {
        public final static String FILES_NULL = "files_null";
        public final static String ACTION_NULL = "action_null";
        public final static String FILES_EMPTY = "files_empty";
        public final static String SRC_PATH_NULL = "src_path_null";
        public final static String SRC_DOESNT_EXIST = "src_doesnt_exist";
        public final static String FILE_DOESNT_EXIST = "file_doesnt_exist";
        public final static String ACTION_DOESNT_EXIST = "action_doesnt_exist";
        public final static String NOT_IN_CONFLICT = "not_in_conflict";
        public final static String DIR_EXISTS = "dir_exists";
        public final static String DIR_NOT_CREATED = "dir_not_created";
        public final static String CANT_RENAME = "cant_rename";
        public final static String NULL_NAME = "null_name";
        public final static String BAD_NAME = "bad_name";
        public final static String PATH_NULL = "path_null";
        public final static String TASK_DOESNT_EXIST = "task_doesnt_exist";
    }
}
