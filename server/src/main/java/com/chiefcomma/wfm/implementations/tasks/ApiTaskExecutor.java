package com.chiefcomma.wfm.implementations.tasks;

import com.chiefcomma.wfm.interfaces.tasks.TaskExecutor;
import com.chiefcomma.wfm.response.TaskStatus;
import com.chiefcomma.wfm.tasks.building.Task;
import com.chiefcomma.wfm.tasks.TaskProgress;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ApiTaskExecutor implements TaskExecutor {

	private int tasksCount = 0;
	private Map<Integer, Task> tasksContainer = new HashMap<Integer, Task>();

	@Override
	public synchronized int execute(Task task) {
		tasksContainer.put(++tasksCount, task);
		new Thread(task).start();
		return tasksCount;
	}

	@Override
	public synchronized int stop(int taskId) {
		Task task = tasksContainer.get(taskId);
		if (task != null) {
			task.cancel();
			tasksContainer.remove(taskId);
		}

		return 1;
	}

	@Override
	public synchronized TaskStatus getTaskStatus(int taskId) {
		Task task = tasksContainer.get(taskId);
		if (task == null) {
			return null;
		}
		TaskProgress progress = task.getProgress();
		if (progress.isDone() || task.isCancelled()) {
			tasksContainer.remove(taskId);
		}
		return new TaskStatus(taskId, task.getStatus(), progress, task.getDescription());
	}

	@Override
	public synchronized TaskStatus[] runningTasks() {
		Set<Integer> taskIds = tasksContainer.keySet();
		ArrayList<TaskStatus> statuses = new ArrayList<TaskStatus>();
		for (Integer id : taskIds) {
			statuses.add(getTaskStatus(id));
		}
		return statuses.toArray(new TaskStatus[statuses.size()]);
	}

	@Override
	public synchronized void resolveConflict(int id, Conflict.Action action, String data) {
		Task task = tasksContainer.get(id);
		if (task != null) {
			task.resolve(action, data);
		}
	}
}
