package com.chiefcomma.wfm.response.api;

public class DownloadResponse {

	private String url;

	public DownloadResponse(String url) {
		this.url = url;
	}
}
