package com.chiefcomma.wfm.response;

import com.chiefcomma.wfm.tasks.Description;
import com.chiefcomma.wfm.tasks.DescriptionLine;
import com.chiefcomma.wfm.tasks.TaskProgress;

import java.util.ArrayList;

public class TaskStatus {

	int taskId;
	private TaskProgress.Status status;
	private TaskProgress progress;
	private ArrayList<DescriptionLine> description;

	public TaskStatus(int id, TaskProgress.Status status, TaskProgress progress, Description description) {
		this.taskId = id;
		this.status = status;
		this.progress = progress;
		this.description = description == null ? null : description.getDescription();
	}

	public boolean hasConflict() {
		return progress.hasConflict();
	}

	@Override
	public String toString() {
		return "id: " + taskId + " progress: " + progress.toString();
	}
}
