package com.chiefcomma.wfm.response.security_layer;

public class UsernameResponse {

	private String username;

	public UsernameResponse(String username) {
		this.username = username;
	}
}
