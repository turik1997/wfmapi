package com.chiefcomma.wfm.response;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;

import java.util.ArrayList;

public class ConflictDialog {

	private ArrayList<Button> buttons = new ArrayList<Button>();
	private String text;
	public static class Builder {

		private boolean built = false;
		private ConflictDialog dialog = new ConflictDialog();

		public Builder addButton(Conflict.Action action, String text) {
			if ( built ) throw new IllegalStateException("Builder cannot be used more than once");
			dialog.addButton(new Button(action.toString(), text));
			return this;
		}

		public Builder setText(String text) {
			dialog.setText(text);
			return this;
		}

		public ConflictDialog build() {
			return dialog;
		}
	}

	private static class Button {
		private String action;
		private String text;

		public Button(String action, String text) {
			this.action = action;
			this.text = text;
		}
	}

	void addButton(Button button) {
		buttons.add(button);
	}

	void setText(String text) {
		this.text = text;
	}
}
