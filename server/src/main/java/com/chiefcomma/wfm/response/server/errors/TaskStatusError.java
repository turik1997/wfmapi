package com.chiefcomma.wfm.response.server.errors;

public class TaskStatusError {

	private String error;
	private String reason;

	public TaskStatusError(String error, String reason) {
		this.error = error;
		this.reason = reason;
	}
}
