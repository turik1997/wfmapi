package com.chiefcomma.wfm.response.server.errors;

public class BadRequest {

	private String error;
	private String reason;

	public BadRequest(String error, String reason) {
		this.error = error;
		this.reason = reason;
	}
}
