package com.chiefcomma.wfm.response;

import com.chiefcomma.wfm.webservice.api.response.ErrorAction;

import java.util.ArrayList;
import java.util.List;

public class ErrorResponse {

	public enum ErrorType {
		INLINED,
		DIALOG
	}

	private ErrorType errorType;
	private String error;
	private String reason;
	private List<ErrorAction> actions = new ArrayList<>();

	public ErrorResponse(String error, String reason) {
		this.error = error;
		this.reason = reason;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<ErrorAction> getActions() {
		return actions;
	}

	public void setActions(List<ErrorAction> actions) {
		this.actions = actions;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}
}
