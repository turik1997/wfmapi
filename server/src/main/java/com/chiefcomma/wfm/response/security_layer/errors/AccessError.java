package com.chiefcomma.wfm.response.security_layer.errors;

public class AccessError {

	private String error;
	private String description;

	public AccessError(String errorMsg) {
		this.error = error;
		this.description = null;
	}

	public AccessError(String errorMsg, String description) {
		this.error = errorMsg;
		this.description = description;
	}
}
