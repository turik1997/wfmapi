package com.chiefcomma.wfm.request;

import java.util.ArrayList;

/**
 * Created by Hp on 9/27/2017.
 */

public class DeleteRequest {

    private ArrayList<String> files;
    private String srcPath;

    public DeleteRequest(ArrayList<String> fileNames, String srcPath) {
        this.files = fileNames;
        this.srcPath = srcPath;
    }

    public ArrayList<String> getFileNames() {
        return files;
    }

    public String getSrcPath() {
        return srcPath;
    }
}
