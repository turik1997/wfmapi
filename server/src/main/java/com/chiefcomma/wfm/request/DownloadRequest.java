package com.chiefcomma.wfm.request;

import java.util.ArrayList;

public class DownloadRequest {

	private ArrayList<String> files;
	private String srcPath;
	private boolean zip = false;

	public DownloadRequest(ArrayList<String> files, String srcPath, boolean zip) {
		this.files = files;
		this.srcPath = srcPath;
		this.zip = zip;
	}

	public ArrayList<String> getFiles() {
		return files;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public boolean isZip() {
		return zip;
	}
}
