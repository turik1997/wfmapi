package com.chiefcomma.wfm.request;

public class TaskUpdateRequest {

	private String action;
	private String data;

	public TaskUpdateRequest(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public String getData() {
		return data;
	}
}
