package com.chiefcomma.wfm.request;

public class RenameRequest {

	private String path;
	private String name;
	private String fileName;

	public RenameRequest(String path, String name, String fileName) {
		this.path = path;
		this.name = name;
		this.fileName = fileName;
	}

	public String getPath() {
		return path;
	}

	public String getName() {
		return name;
	}

	public String getFileName() {
		return fileName;
	}
}
