package com.chiefcomma.wfm.request;

import org.nanohttpd.protocols.http.response.Response;

public class ValidationException extends Exception {

    private final Response errorResponse;

    public ValidationException(Response errorResponse) {
        this.errorResponse = errorResponse;
    }

    public Response getErrorResponse() {
        return errorResponse;
    }
}
