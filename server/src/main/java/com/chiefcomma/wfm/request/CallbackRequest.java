package com.chiefcomma.wfm.request;

public class CallbackRequest {
    private String type;
    private String postData;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPostData() {
        return postData;
    }

    public void setPostData(String postData) {
        this.postData = postData;
    }
}
