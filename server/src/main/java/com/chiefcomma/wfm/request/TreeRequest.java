package com.chiefcomma.wfm.request;

/**
 * Created by Hp on 10/2/2017.
 */

public class TreeRequest {

    private boolean isFirst;
    private String nodeId;

    public TreeRequest(boolean isFirst, String nodeId) {
        this.isFirst = isFirst;
        this.nodeId = nodeId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public boolean isFirst() {
        return isFirst;
    }
}
