package com.chiefcomma.wfm.request;

import com.chiefcomma.wfm.API;
import com.chiefcomma.wfm.AbstractFile;
import com.chiefcomma.wfm.interfaces.environment.EnvironmentInformer;
import com.chiefcomma.wfm.models.SafRequest;
import com.chiefcomma.wfm.utils.AssetManager;
import com.chiefcomma.wfm.utils.ResponseHelper;
import org.nanohttpd.protocols.http.response.Response;

import java.io.File;
import java.util.List;
import java.util.Map;

public class RequestValidator {
    private final AssetManager assetManager;
    private final EnvironmentInformer environmentInformer;

    public RequestValidator(AssetManager assetManager, EnvironmentInformer environmentInformer) {
        this.assetManager = assetManager;
        this.environmentInformer = environmentInformer;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public void validateTaskId(String taskId) throws ValidationException {
        try {
            int id = Integer.parseInt(taskId);
            if (id < 0) {
                throw new ValidationException(
                        ResponseHelper.errorNegativeTaskId(assetManager, Integer.toString(id))
                );
            }
        } catch (NumberFormatException e) {
            throw new ValidationException(
                    ResponseHelper.errorBadTaskId(assetManager, taskId)
            );
        }
    }

    public void validateFileUploadRequest(FileUploadRequest fileUploadRequest) throws ValidationException {
        if (fileUploadRequest.getRawFilePath() == null) {
            throw new ValidationException(
                    ResponseHelper.errorCantHandleFileUpload(assetManager)
            );
        }
        if (!new File(fileUploadRequest.getRawFilePath()).exists()) {
            throw new ValidationException(
                    ResponseHelper.internalError(assetManager)
            );
        }
        if (!new File(fileUploadRequest.getDestPath()).exists()) {
            throw new ValidationException(
                    ResponseHelper.errorBadDestination(assetManager, fileUploadRequest.getDestPath())
            );
        }
    }

    public void validateHeaders(Map<String, String> headers) throws ValidationException {
        String host = headers.get("host");
        String referer = headers.get("referer");
        if (referer == null || referer.isEmpty()) {
            throw new ValidationException(
                    ResponseHelper.errorNoHeaderReferer(assetManager)
            );
        } else if (host == null || host.isEmpty()) {
            throw new ValidationException(
                    ResponseHelper.errorNoHeaderHost(assetManager)
            );
        } else if (!referer.startsWith("http://" + host + "/")) {
            throw new ValidationException(
                    ResponseHelper.errorHeaderMismatch(assetManager)
            );
        }
    }

    public void validateCopyRequest(CopyRequest copyRequest) throws ValidationException {
        Response response = null;
        if (copyRequest.getSrcPath() == null || copyRequest.getSrcPath().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "srcPath");
        } else if (copyRequest.getFiles() == null || copyRequest.getFiles().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "files");
        } else if (copyRequest.getDestPath() == null || copyRequest.getDestPath().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "destPath");
        } else {
            File srcDir = new File(copyRequest.getSrcPath());
            File destDir = new File(copyRequest.getDestPath());
            if (!srcDir.canRead()) {
                response = ResponseHelper.errorAccessDenied(assetManager, srcDir.getAbsolutePath());
            } else if (!fileExists(destDir)) {
                response = ResponseHelper.errorBadDestination(assetManager, destDir.getAbsolutePath());
            } else if (!AbstractFile.getInstance().canWrite(destDir)) {
                if (AbstractFile.getInstance().isSAF(destDir)) {
                    response = ResponseHelper.errorStorageAccessFrameworkNeeded(assetManager, destDir.getAbsolutePath());
                } else {
                    response = ResponseHelper.errorAccessDenied(assetManager, destDir.getAbsolutePath());
                }
            } else {
                for (String fileName : copyRequest.getFiles()) {
                    File fileToCopy = new File(srcDir, fileName);
                    if (!fileExists(fileToCopy)) {
                        response = ResponseHelper.errorFileNotFound(assetManager, fileName, srcDir.getAbsolutePath());
                        break;
                    } else if (!fileToCopy.canRead()) {
                        response = ResponseHelper.errorAccessDenied(assetManager, fileToCopy.getAbsolutePath());
                        break;
                    } else if (destDir.getAbsolutePath().contains(fileToCopy.getAbsolutePath())) {
                        response = ResponseHelper.errorCantCopyToItself(assetManager);
                    }
                }
            }
        }
        if (response != null) {
            throw new ValidationException(response);
        }
    }

    public void validateDeleteRequest(DeleteRequest deleteRequest) throws ValidationException {
        Response response = null;
        if (deleteRequest.getSrcPath() == null || deleteRequest.getSrcPath().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "srcPath");
        } else if (deleteRequest.getFileNames() == null || deleteRequest.getFileNames().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "fileNames");
        } else {
            File srcDir = new File(deleteRequest.getSrcPath());
            for (String fileName : deleteRequest.getFileNames()) {
                File fileToDelete = new File(srcDir, fileName);
                if (!fileExists(fileToDelete)) {
                    response = ResponseHelper.errorFileNotFound(assetManager, fileName, srcDir.getAbsolutePath());
                    break;
                }
                if (!AbstractFile.getInstance().canWrite(fileToDelete)) {
                    if (AbstractFile.getInstance().isSAF(fileToDelete)) {
                        response = ResponseHelper.errorStorageAccessFrameworkNeeded(assetManager, srcDir.getAbsolutePath());
                    } else {
                        response = ResponseHelper.errorAccessDenied(assetManager, fileToDelete.getAbsolutePath());
                    }
                    break;
                }
            }
        }
        if (response != null) {
            throw new ValidationException(response);
        }
    }

    public void validateMakeDirRequest(MakeDirRequest makeDirRequest, File srcDir) throws ValidationException {
        Response response = null;
        if (makeDirRequest.getDirName() == null || makeDirRequest.getDirName().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "dirName");
        } else if (!fileExists(srcDir)) {
            response = ResponseHelper.errorFileNotFound(assetManager, srcDir.getName(), srcDir.getParent());
        } else if (makeDirRequest.getDirName().contains("/") ) {
            response = ResponseHelper.errorBadName(assetManager, makeDirRequest.getDirName());
        } else if (new File(srcDir, makeDirRequest.getDirName()).exists()) {
            response = ResponseHelper.errorDirExists(assetManager, makeDirRequest.getDirName());
        } else if (!AbstractFile.getInstance().canWrite(srcDir)) {
            if (AbstractFile.getInstance().isSAF(srcDir)) {
                response = ResponseHelper.errorStorageAccessFrameworkNeeded(assetManager, srcDir.getAbsolutePath());
            } else {
                response = ResponseHelper.errorAccessDenied(assetManager, srcDir.getAbsolutePath());
            }
        }
        if (response != null) {
            throw new ValidationException(response);
        }
    }

    public void validateCallbackRequest(CallbackRequest callbackRequest) throws ValidationException {
        Response response = null;
        if (callbackRequest.getType() == null || callbackRequest.getType().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "type");
        } else if (callbackRequest.getPostData() == null || callbackRequest.getPostData().isEmpty()) {
            response = ResponseHelper.errorEmptyPostData(assetManager);
        } else if (!API.STORAGE_ACCESS_FRAMEWORK.equalsIgnoreCase(callbackRequest.getType())) {
            response = ResponseHelper.errorWrongApi(assetManager, null);
        }
        if (response != null) {
            throw new ValidationException(response);
        }
    }

    public void validateSafCallbackRequest(SafRequest safRequest) throws ValidationException {
        Response response = null;
        if (safRequest.getDirectoryPath() == null || safRequest.getDirectoryPath().isEmpty()) {
            response = ResponseHelper.errorMissingField(assetManager, "directoryPath");
        } else {
            File safDir = new File(safRequest.getDirectoryPath());
            if (!safDir.exists()) {
                response = ResponseHelper.errorFileNotFound(assetManager, safDir.getName(), safDir.getParentFile().getAbsolutePath());
            } else if (!AbstractFile.getInstance().isSAF(safDir)) {
                response = ResponseHelper.errorBadDestination(assetManager, safDir.getAbsolutePath());
            }
        }
        if (response != null) {
            throw new ValidationException(response);
        }
    }

    private boolean fileExists(File file) {
        List<String> listOfStorages = environmentInformer.getListOfStorages();
        for (String storageVolumePath : listOfStorages) {
            if (file.getAbsolutePath().startsWith(storageVolumePath) && file.exists()) {
                return true;
            }
        }
        return false;
    }
}
