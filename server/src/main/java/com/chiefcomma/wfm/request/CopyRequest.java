package com.chiefcomma.wfm.request;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Hp on 9/26/2017.
 */

public class CopyRequest {

    private ArrayList<String> files;
    private String srcPath;
    private String destPath;

    public CopyRequest(ArrayList<String> files, String srcPath, String destPath) {
        this.files = files;
        this.srcPath = srcPath;
        this.destPath = destPath;
    }

    public ArrayList<String> getFiles() {
        return files;
    }

    public String getSrcPath() {
        return srcPath;
    }

    public String getDestPath() {
        return destPath;
    }
}
