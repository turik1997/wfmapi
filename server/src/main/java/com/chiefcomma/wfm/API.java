package com.chiefcomma.wfm;

import java.util.Locale;
import java.util.UUID;

/**
 * Created by turok on 9/8/17.
 */

public class API {
    private final static String VERSION = "v1";
    public final static String PREFIX = "/wfm";
    private final static String API = PREFIX + "/api";
    public final static String API_URL = API + "/" + VERSION;
    public final static String RESOURCES = PREFIX + "/web";
    public final static String AUTH = "/auth";
    public final static String COPY = "/copy";
    public final static String MOVE = "/move";
    public final static String CALLBACK = "/callback";
    public final static String DELETE = "/delete";
    public final static String RENAME = "/rename";
    public final static String MKDIR = "/mkdir";
    public final static String TASKS = "/tasks";
    public final static String DOWNLOAD = "/download";
    public final static String FILE_UPLOAD = "/file_upload";
    public final static String ACCESS_STATUS = "/access_status";
    public final static String USERNAME = "/username";
    public final static String GET_TREE = "/get_tree";
    public final static String VACANT_DIR_NAME = "/vacant_dir_name";
    public final static String STORAGE_ACCESS_FRAMEWORK = "saf";

    public final static String API_AUTH = API_URL + AUTH;
    public final static String API_COPY = API_URL + COPY;
    public final static String API_MOVE = API_URL + MOVE;
    public final static String API_DELETE = API_URL + DELETE;
    public final static String API_RENAME = API_URL + RENAME;
    public final static String API_MKDIR = API_URL + MKDIR;
    public final static String API_TASKS = API_URL + TASKS;
    public final static String API_FILE_UPLOAD = API_URL + FILE_UPLOAD;
    public final static String API_ACCESS_STATUS = API_URL + ACCESS_STATUS;
    public final static String API_USERNAME = API_URL + USERNAME;
    public final static String API_GET_TREE = API_URL + GET_TREE;
    public final static String API_VACANT_DIR_NAME = API_URL + VACANT_DIR_NAME;
    public final static String API_DOWNLOAD = API_URL + DOWNLOAD;

    private final static String[] apis = new String[] {
            AUTH,
            DOWNLOAD,
            COPY,
            MOVE,
            DELETE,
            RENAME,
            MKDIR,
            TASKS,
            FILE_UPLOAD,
            ACCESS_STATUS,
            USERNAME,
            GET_TREE,
            VACANT_DIR_NAME,
            CALLBACK,
    };

    private final static String[] paramInlinedApis = new String[] {
            DOWNLOAD,
            TASKS,
            CALLBACK,
    };

    public final static boolean isApiCall(String url) {
        if (url.startsWith(API_URL)) {
            url = url.substring(API_URL.length());
            for (String api : apis) {
                if (api.equals(url)) {
                    return true;
                }
            }
            if (isParamInlinedApi(url)) {
                return true;
            }
        }

        return false;
    }

    private final static boolean isParamInlinedApi(String url) {
        for (String api : paramInlinedApis) {
            if (isParamInlinedWithUrl(url, api)) {
                return true;
            }
        }
        return false;
    }

    private final static boolean isParamInlinedWithUrl(String url, String apiEndpoint) {
        return url.startsWith(apiEndpoint + "/") &&
                url.indexOf("/", apiEndpoint.length()+1) == -1;
    }
}
