package com.chiefcomma.wfm.interfaces.users.validators;

/**
 * Created by Hp on 9/19/2017.
 */

public interface UserAuthValidator extends Validator {

    boolean isAuthTokenValid(String clientIp, String authToken);
}
