package com.chiefcomma.wfm.interfaces.users;

/**
 * Created by turok on 9/8/17.
 */

public interface UserPermissionManager {

    boolean canRead(String ip);
    boolean canWrite(String ip);
}
