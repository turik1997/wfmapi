package com.chiefcomma.wfm.interfaces.resources;

import java.io.InputStream;

public interface ResourceManager {

	enum ResourceType {
		INDEX_PAGE,
		FORBIDDEN_PAGE,
		PASSWORD_PAGE
	}

	InputStream open(String path);
}
