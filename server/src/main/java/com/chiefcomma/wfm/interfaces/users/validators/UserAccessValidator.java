package com.chiefcomma.wfm.interfaces.users.validators;

/**
 * Created by turok on 9/8/17.
 */

public interface UserAccessValidator extends Validator {

    boolean isAccessAllowed(String clientIp);
    boolean isAccessProhibited(String clientIp);
}
