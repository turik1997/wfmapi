package com.chiefcomma.wfm.interfaces.environment;

import com.chiefcomma.wfm.utils.LocaleReader;

import java.io.File;
import java.util.List;
import java.util.Locale;

/**
 * Created by turok on 9/9/17.
 */

public interface EnvironmentInformer extends LocaleReader {

    File getDefaultStorage();
     Locale getLocale();
    List<String> getListOfStorages();
}
