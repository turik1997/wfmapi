package com.chiefcomma.wfm.interfaces.listener;

public interface LogListener {

	public final static int DEBUG = 500;
	public final static int INFO = 700;
	public final static int SEVERE = 900;
	public final static int ERROR = 1100;


	void onError(String message);
	void onInfo(String message);
}
