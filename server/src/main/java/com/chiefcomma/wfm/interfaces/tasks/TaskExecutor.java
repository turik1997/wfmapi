package com.chiefcomma.wfm.interfaces.tasks;

import com.chiefcomma.wfm.response.TaskStatus;
import com.chiefcomma.wfm.tasks.building.Task;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;

public interface TaskExecutor {

	int execute(Task task);
	int stop(int taskId);
	TaskStatus getTaskStatus(int taskId);
	TaskStatus[] runningTasks();
	void resolveConflict(int id, Conflict.Action action, String data);
}