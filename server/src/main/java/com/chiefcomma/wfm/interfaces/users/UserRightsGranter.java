package com.chiefcomma.wfm.interfaces.users;

/**
 * Created by Hp on 9/19/2017.
 */

public interface UserRightsGranter {

    void grantReadPermission(String ip);
    void grantWritePermission(String ip);
    void grantServerAccess(String ip);
    void denyServerAccess(String ip);
    void denyReadPermission(String ip);
    void denyWritePermission(String ip);
}
