package com.chiefcomma.wfm.interfaces.environment;

/**
 * Created by turok on 9/8/17.
 */

public interface ConfigProvider {

    long getUploadLimit();
    int getPort();
    boolean isSecuredByPass();
    boolean isManualAccessEnabled();
}
