package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface Overwritable {

    void overwrite(String conflictingItem);
    void overWriteAll(String conflictingItem);
}
