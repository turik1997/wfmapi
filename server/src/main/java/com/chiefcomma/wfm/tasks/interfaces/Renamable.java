package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface Renamable {

    void rename(String conflictingItem, String newName);
    void renameAll(String conflictingItem, String pattern);
}
