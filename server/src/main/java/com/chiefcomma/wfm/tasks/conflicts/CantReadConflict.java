package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;

import java.io.File;

/**
 * Created by turok on 9/14/17.
 */

public class CantReadConflict extends Conflict {

    private File conflictingFile;
    private ConflictDialog dialog;

    public CantReadConflict(File conflictingFile)
    {
        super("CantRead");
        this.conflictingFile = conflictingFile;
    }

    @Override
    public Result resolve(Action action, String data) {
        return Result.valueOf(action.toString());
    }

    @Override
    public ConflictDialog getDialog() {
        //todo corrent return value and think about existence of this class
        return null;
    }
}
