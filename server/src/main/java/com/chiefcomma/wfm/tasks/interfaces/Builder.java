package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.building.Task;

import java.io.File;
import java.util.List;

/**
 * Created by turok on 9/11/17.
 */

public interface Builder {

    Task buildCopyTask(List<String> fileNames, File destDir, File src);
    Task buildMoveTask(List<String> srcFiles, File destDir, File srcDir);
    Task buildDeleteTask(List<String> fileNames, File srcDir);
    Task buildRenameTask(File file, String name, String prefixPattern, String suffixPattern);
}
