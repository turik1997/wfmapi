package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/14/17.
 */

public interface Callback<X, Y, Z> {

    void callback(X x, Y y, Z z);
}
