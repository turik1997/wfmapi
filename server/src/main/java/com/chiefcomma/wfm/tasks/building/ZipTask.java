package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.*;
import com.chiefcomma.wfm.utils.FileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.util.List;

public class ZipTask extends FileWalkingTask {

	private FileCounter counter;
	private final List<File> files;

	public ZipTask(List<File> files, FileWalker fileWalker, FileVisitor fileVisitor) {
		super(files, fileWalker, fileVisitor);
		this.files = files;
	}

	@Override
	public void onPrepare() {
		counter = new FileCounter(files);
		FileCounterResult result = counter.countFiles(true);
		totalAmountOfFiles = result.getTotalFiles();
	}

	@Override
	protected void onWalkCancelled() {
		if ( counter != null) {
			counter.cancel();
		}
	}
}
