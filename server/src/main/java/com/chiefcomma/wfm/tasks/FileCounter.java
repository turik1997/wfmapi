package com.chiefcomma.wfm.tasks;

import com.chiefcomma.wfm.tasks.interfaces.Cancelable;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.FileWalker;
import com.chiefcomma.wfm.utils.SimpleFileVisitor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by turok on 9/10/17.
 */

public class FileCounter extends SimpleFileVisitor implements Cancelable {

    private long totalBytes = 0;
    private int totalFiles = 0;
    private List<File> files = new ArrayList<File>();
    private FileWalker walker = new FileWalker();
    private boolean countDirs = false;

    public FileCounter(ArrayList<String> fileNames, File src) {
        for ( String name : fileNames )
            files.add(new File(src, name));
    }

    public FileCounter(List<File> files) {
        this.files = files;
    }

    public FileCounterResult countFiles(boolean countDirs) {
        this.countDirs = countDirs;
        for ( File file : files )
            try {
                walker.walkTree(file, this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        return new FileCounterResult(totalBytes, totalFiles);
    }

    @Override
    public FileVisitResult preVisitDirectory(File dir) throws IOException {
        if ( countDirs )
            ++totalFiles;
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(File file) throws IOException {
        ++totalFiles;
        totalBytes += file.length();

        return FileVisitResult.CONTINUE;
    }

    @Override
    public void cancel() {
        walker.cancel();
    }
}
