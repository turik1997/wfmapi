package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.LocaledReader;
import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.io.File;

public class CantMakeDirsConflict extends Conflict {

	public final static String TYPE = "MKDIR_FAIL";
	private final static String CANT_MAKE_DIR = "cant_make_dir";
	private File conflictingDir;

	public CantMakeDirsConflict(File conflictingDir) {
		super(TYPE);
		this.conflictingDir = conflictingDir;
		dialogBuilder = TripleButtonDialogBuilder.getInstance();
		String text = LocaledReader.readLocaleString(TEXT_PATH, CANT_MAKE_DIR, conflictingDir.getAbsolutePath());
		dialogBuilder = dialogBuilder.setText(text);
	}

	@Override
	public ConflictDialog getDialog() {
		return dialogBuilder.build();
	}

	@Override
	public Result resolve(Action action, String data) {
		switch ( action )
		{
			case CANCEL: case SKIP: case SKIP_ALL: case RETRY:
				return Result.valueOf(action.toString());
		}

		return Result.RETRY;
	}
}
