package com.chiefcomma.wfm.tasks.conflicts;

import java.io.File;

/**
 * Created by Hp on 9/27/2017.
 */

public class LockException extends Exception {

    private File conflictedFile;

    public LockException(File conflictedFile) {
        this.conflictedFile = conflictedFile;
    }

    public File getConflictedFile() {
        return conflictedFile;
    }
}
