package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.TaskProgress;

/**
 * Created by turok on 9/10/17.
 */

public interface Progressive {
    TaskProgress getProgress();
}
