package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.CantMakeDirsConflict;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.DestinationExistsConflict;
import com.chiefcomma.wfm.tasks.conflicts.SAFConflict;
import com.chiefcomma.wfm.utils.FileVisitResult;

import java.io.File;
import java.util.concurrent.Callable;

public class DestinationExistsCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		void mergeExistingDirs();
		void overwriteExistingFiles();
		boolean canOverwriteExistingFiles();
		boolean canMergeExistingDirs();
		void skipExistingDirs();
		void skipExistingFiles();
		boolean canSkipExistingFiles();
		boolean canSkipExistingDirs();
		boolean canRenameAll();
		void renameAll();
		void cancel();
	}

	private Configurator configurator;
	private MkdirsCallable.Configurator mkdirConfigurator;
	private DestinationExistsConflict conflict;
	private ConflictHandler conflictHandler;
	private File initialFile;
	private File destDir;
	private File fileCopy;
	private ConflictableRunner conflictableRunner = new ConflictableRunner();

	public DestinationExistsCallable(Configurator configurator, MkdirsCallable.Configurator mkdirConfigurator, ConflictHandler conflictHandler, File initialFile, File destDir, File fileCopy) {
		this.configurator = configurator;
		this.mkdirConfigurator = mkdirConfigurator;
		this.conflictHandler = conflictHandler;
		this.initialFile = initialFile;
		this.destDir = destDir;
		this.fileCopy = fileCopy;
		this.conflict = new DestinationExistsConflict(initialFile, destDir, fileCopy);
	}

	@Override
	public Conflict.Result call() throws Exception {
		Conflict.Result result = conflictHandler.handle(conflict);
		switch (result) {
			case CANCEL:
				configurator.cancel();
				break;
			case SKIP_ALL:
				if (initialFile.isDirectory()) {
					configurator.skipExistingDirs();
				} else {
					configurator.skipExistingFiles();
				}
				break;
			case RENAME:
				fileCopy = conflict.getRenamedFile();
				if ( fileCopy.exists() && initialFile.isDirectory() && !fileCopy.mkdirs() )
				{
					conflict = new DestinationExistsConflict(initialFile, destDir, fileCopy);
					return Conflict.Result.RETRY;
				}

				if ( !fileCopy.exists() && initialFile.isDirectory() && !fileCopy.mkdirs() )
				{
					MkdirsCallable mkdirsCallable = new MkdirsCallable(fileCopy, conflictHandler);
					FileVisitResult visitResult = conflictableRunner.runConflictableCode(mkdirsCallable);
					if ( visitResult == FileVisitResult.TERMINATE )
						return Conflict.Result.CANCEL;
					if ( visitResult == FileVisitResult.SKIP_SUBTREE )
						return Conflict.Result.SKIP;
				}

				return Conflict.Result.RENAME;
			case RENAME_ALL: {
				configurator.renameAll();
				break;
			}
			case OVERWRITE_ALL:
				configurator.overwriteExistingFiles();
				break;
			case WRITE_INTO_ALL:
				configurator.mergeExistingDirs();
				break;
		}
		return result;
	}

	public File getRenamedFile() {
		return fileCopy;
	}
}
