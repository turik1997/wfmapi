package com.chiefcomma.wfm.tasks;

public class DescriptionLine {

	String name;
	String value;

	public DescriptionLine(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
