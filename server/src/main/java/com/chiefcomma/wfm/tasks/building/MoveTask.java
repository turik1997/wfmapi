package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.*;

import java.io.File;
import java.util.List;

/**
 * Created by Hp on 9/30/2017.
 */

public class MoveTask extends Task {

    private final Task copyTask;
    private final Task deleteTask;
    private boolean isDeleting = false;

    public MoveTask(List<File> files, Task copyTask, Task deleteTask) {
        this.copyTask = copyTask;
        this.deleteTask = deleteTask;
    }

    @Override
    public TaskProgress getProgress() {
        if (!isDeleting) {
            TaskProgress progress = copyTask.getProgress();
            progress.setTotalDonePercentage(progress.getTotalDonePercentage() / 2.0);
            return progress;
        }
        TaskProgress progress = deleteTask.getProgress();
        progress.setTotalDonePercentage(progress.getTotalDonePercentage() / 2.0 + 50.0);
        return progress;
    }

    @Override
    public void onPrepare() throws OperationFailedException {
    }

    @Override
    public void onStart() {
        copyTask.run();
        // todo check if copy not failed, run delete task, otherwise go to failed state
        isDeleting = true;
        deleteTask.run();
        // todo check if delete failed, go to failed state
    }

    @Override
    public void onCancel() {
        copyTask.cancel();
        deleteTask.cancel();
    }
}
