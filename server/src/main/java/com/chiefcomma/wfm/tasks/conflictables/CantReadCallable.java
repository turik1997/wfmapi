package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.conflicts.CantReadConflictConfig;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;

import java.util.concurrent.Callable;

public class CantReadCallable implements Callable<Conflict.Result> {

	private CantReadConflictConfig j;

	@Override
	public Conflict.Result call() throws Exception {
		return null;
	}
}
