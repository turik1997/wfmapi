package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.*;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.interfaces.*;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by turok on 9/10/17.
 */

public abstract class Task implements Runnable, Progressive, Cancelable, ProgressHandler, ConflictHandler {

    private final static long RESOLUTION_INTERVAL = 200;
    volatile private Conflict conflict = null;
    private long resolutionInterval;

    volatile private boolean isResolved = true;
    volatile private boolean isCancelled = false;
    volatile private Conflict.Result actionAfterResolving;
    volatile private TaskProgress.Status status = TaskProgress.Status.NOT_STARTED;

    private ArrayList<CompleteListener> completeListeners = new ArrayList<CompleteListener>();
    private ArrayList<StartListener> fileEntranceListeners = new ArrayList<StartListener>();
    private ArrayList<ProgressUpdateListener> updateListeners = new ArrayList<ProgressUpdateListener>();
    private ArrayList<SkipListener> skipListeners = new ArrayList<SkipListener>();
    protected int totalAmountOfFiles;
    protected int finishedAmountOfFiles;
    protected long finishedBytesForCurrentFile;
    protected File currentFile;
    private Object lock = new Object();

    private Description description;

    public Task() {
        this(RESOLUTION_INTERVAL);
    }

    public Task(long resolutionInterval) {

        if ( resolutionInterval == 0 )
            throw new IllegalArgumentException("Resolution interval should be positive");
        
        this.resolutionInterval = resolutionInterval;
    }

    final protected Conflict.Result stickToConflict(Conflict conflict) {
        this.conflict = conflict;
        isResolved = false;

        while ( !isResolved && !isCancelled )
        {
            try {
                Thread.sleep(resolutionInterval);
            } catch (InterruptedException ignored) {
                continue;
            }
        }

        if ( isCancelled )
            return Conflict.Result.CANCEL;

        return actionAfterResolving;
    }

    final public void resolve(Conflict.Action action) {
        resolve(action, null);
    }

    public void resolve(Conflict.Action action, String data) {
        if (conflict != null) {
            actionAfterResolving = conflict.resolve(action, data);
        }
        isResolved = true;
        conflict = null;
    }

    @Override
    public void cancel() {
        status = TaskProgress.Status.CANCELLING;
        isCancelled = true;
        try {
            onCancel();
        } catch (OperationFailedException e) {
            e.printStackTrace();
            // todo go into failed mode
        }
    }

    public Description getDescription() {
        return description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    final public void addCompleteListener(CompleteListener listener) {
        completeListeners.add(listener);
    }

    final public void addStartListener(StartListener listener) {
        fileEntranceListeners.add(listener);
    }

    final public void addUpdateListener(ProgressUpdateListener listener) {
        updateListeners.add(listener);
    }

    final public void addSkipListener(SkipListener listener) {
        skipListeners.add(listener);
    }

    public ArrayList<SkipListener> getSkipListeners() {
        return skipListeners;
    }

    protected ArrayList<CompleteListener> getCompleteListeners() {
        return completeListeners;
    }

    protected ArrayList<StartListener> getFileEntranceListeners() {
        return fileEntranceListeners;
    }

    protected ArrayList<ProgressUpdateListener> getUpdateListeners() {
        return updateListeners;
    }

    final public boolean isCancelled() {
        return isCancelled;
    }

    final protected boolean isInConflict() {
        return conflict != null;
    }

    final protected Conflict.ConflictInfo getConflictInfo(){
        return new Conflict.ConflictInfo(conflict);
    }

    public TaskProgress.Status getStatus() {
        return status;
    }

    public abstract void onPrepare() throws OperationFailedException;
    public abstract void onStart() throws OperationFailedException;
    public abstract void onCancel() throws OperationFailedException;

    @Override
    public void run() {
        try {
            status = TaskProgress.Status.PREPARING;
            onPrepare();
            status = TaskProgress.Status.IN_PROGRESS;
            onStart();
        } catch (OperationFailedException operationFailedException) {
            // todo go into failed mode
        }

    }

    @Override
    public void startedNewFile(File file) {
        currentFile = file;
        finishedBytesForCurrentFile = 0;
    }

    @Override
    public void currentFileUpdate(long totalBytes) {
        finishedBytesForCurrentFile += totalBytes;
    }

    @Override
    public void done(FileCounterResult filesDone) {
        finishedAmountOfFiles += filesDone.getTotalFiles();
        finishedBytesForCurrentFile = 0;
        currentFile = null;
    }

    @Override
    public TaskProgress getProgress() {
        double totalPercentage = totalAmountOfFiles > 0
                ? finishedAmountOfFiles * 100.0 / totalAmountOfFiles + (
                        currentFile != null && currentFile.length() > 0
                        ? finishedBytesForCurrentFile / (1.0 * currentFile.length() * totalAmountOfFiles) * 100.0
                        : 0.0
                    )
                : 0.0;
        String path = null;
        synchronized (lock) {
            if (currentFile != null) {
                path = currentFile.getAbsolutePath();
            }
        }
        if (isInConflict()) {
            Conflict.ConflictInfo info = getConflictInfo();

            return new TaskProgress(
                    totalPercentage,
                    totalAmountOfFiles,
                    finishedAmountOfFiles,
                    path,
                    null,
                    info.getConflictType(),
                    info.getDialog()
            );
        }
        return new TaskProgress(
                totalPercentage,
                totalAmountOfFiles,
                finishedAmountOfFiles,
                path,
                null
        );
    }

    @Override
    public void skipped(File file, FileCounterResult result) {

    }

    @Override
    public Conflict.Result handle(Conflict conflict) {
        return stickToConflict(conflict);
    }
}
