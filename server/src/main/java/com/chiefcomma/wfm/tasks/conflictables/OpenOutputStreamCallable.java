package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.NotFoundConflict;
import com.chiefcomma.wfm.tasks.conflicts.SAFConflict;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.concurrent.Callable;

import static com.chiefcomma.wfm.tasks.conflicts.Conflict.Result.CANCEL;
import static com.chiefcomma.wfm.tasks.conflicts.Conflict.Result.CONTINUE;
import static com.chiefcomma.wfm.tasks.conflicts.Conflict.Result.SKIP_ALL;

public class OpenOutputStreamCallable implements Callable<Conflict.Result> {

	private File destFile;
	private ConflictHandler conflictHandler;
	private OpenInputStreamCallable.Configurator configurator;
	private SAFConflict.Configurator safConfigurator;
	private FileOutputStream outputStream = null;

	public OpenOutputStreamCallable(File destFile, ConflictHandler conflictHandler, OpenInputStreamCallable.Configurator configurator) {
		this.destFile = destFile;
		this.conflictHandler = conflictHandler;
		this.configurator = configurator;
	}

	public OpenOutputStreamCallable(File destFile, ConflictHandler conflictHandler, OpenInputStreamCallable.Configurator configurator, SAFConflict.Configurator s) {
		this.destFile = destFile;
		this.conflictHandler = conflictHandler;
		this.configurator = configurator;
		this.safConfigurator = s;
	}

	@Override
	public Conflict.Result call() {

		try {
			outputStream = new FileOutputStream(destFile);
		} catch (FileNotFoundException e) {
			if ( configurator.canSkipAllNotFound() )
				return SKIP_ALL;

			Conflict.Result result = safConfigurator.isSAF(destFile)
					? conflictHandler.handle(new SAFConflict(destFile, safConfigurator))
					: conflictHandler.handle(new NotFoundConflict(destFile));
			if ( result == SKIP_ALL )
				configurator.skipAllNotFound();
			else
			if ( result == CANCEL )
				configurator.cancel();
			return result;
		}

		return CONTINUE;
	}

	public FileOutputStream getOutputStream() {
		return outputStream;
	}
}
