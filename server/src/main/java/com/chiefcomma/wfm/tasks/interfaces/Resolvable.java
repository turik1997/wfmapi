package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;

/**
 * Created by turok on 9/14/17.
 */

public interface Resolvable {
    Conflict.Result resolve(Conflict.Action action, String data);
}
