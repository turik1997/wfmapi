package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.AbstractFile;
import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import java.io.File;
import java.util.concurrent.Callable;

public class MkdirsCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		void skipDirMakingProblem();
		boolean canSkipDirMakingProblem();
		void cancel();
	}

	private File dir;
	private ConflictHandler conflictHandler;

	public MkdirsCallable(File dir, ConflictHandler conflictHandler) {
		this.dir = dir;
		this.conflictHandler = conflictHandler;
	}

	@Override
	public Conflict.Result call() throws Exception {
		if (!AbstractFile.getInstance().mkdir(dir)) {
			return Conflict.Result.RETRY;
		}

		return Conflict.Result.CONTINUE;
	}
}
