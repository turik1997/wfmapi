package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface Skippable {

    void skip(String conflictItem);
    void skipAll(String conflictItem);
}
