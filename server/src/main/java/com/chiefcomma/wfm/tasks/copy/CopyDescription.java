package com.chiefcomma.wfm.tasks.copy;

import com.chiefcomma.wfm.tasks.Description;
import com.chiefcomma.wfm.tasks.DescriptionLine;
import com.chiefcomma.wfm.utils.LocaledReader;
import com.chiefcomma.wfm.utils.SharedSentences;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

public class CopyDescription extends Description {

	private String string;

	public CopyDescription(File dirFrom, File dirTo) {
		String fromName = SharedSentences.read(SharedSentences.Keys.FROM);
		description.add(new DescriptionLine(fromName, dirFrom.getAbsolutePath()));
		String toName = SharedSentences.read(SharedSentences.Keys.TO);
		description.add(new DescriptionLine(toName, dirTo.getAbsolutePath()));
		Gson gson = new Gson();
		this.string = gson.toJson(this);
	}

	@Override
	public String toString() {
		return string;
	}
}
