package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.PutEntryConflict;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CloseEntryCallable implements Callable<Conflict.Result> {

	private ZipOutputStream outputStream;
	private ZipEntry zipEntry;
	private PutEntryCallable.Configurator configurator;
	private ConflictHandler conflictHandler;

	public CloseEntryCallable(ZipOutputStream outputStream, ZipEntry zipEntry, PutEntryCallable.Configurator configurator, ConflictHandler conflictHandler) {
		this.outputStream = outputStream;
		this.zipEntry = zipEntry;
		this.configurator = configurator;
		this.conflictHandler = conflictHandler;
	}

	@Override
	public Conflict.Result call() throws Exception {
		try {
			outputStream.closeEntry();
		} catch (IOException e) {
			if ( configurator.canSkipEntryProblems() )
			{
				return Conflict.Result.SKIP;
			}

			PutEntryConflict conflict = new PutEntryConflict(zipEntry);
			Conflict.Result result = conflictHandler.handle(conflict);
			if ( result == Conflict.Result.SKIP_ALL )
				configurator.skipEntryProblems();

			return result;
		}

		return Conflict.Result.CONTINUE;
	}
}
