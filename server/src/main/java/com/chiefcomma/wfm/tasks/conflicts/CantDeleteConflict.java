package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.io.File;

public class CantDeleteConflict extends Conflict {

	public final static String TYPE = "CANT_DELETE";
	private File conflictingFile;

	public CantDeleteConflict(File file) {
		super(TYPE);
		this.conflictingFile = file;
		dialogBuilder = TripleButtonDialogBuilder.getInstance().setText("Cant delete " + conflictingFile.getAbsolutePath());
	}

	@Override
	public Result resolve(Action action, String data) {
		switch ( action )
		{
			case RETRY: case CANCEL: case SKIP: case SKIP_ALL:
				return Result.valueOf(action.toString());
		}

		return Result.RETRY;
	}
}
