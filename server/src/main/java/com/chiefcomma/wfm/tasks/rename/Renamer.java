package com.chiefcomma.wfm.tasks.rename;

import com.chiefcomma.wfm.request.RenameRequest;
import org.nanohttpd.protocols.http.response.Response;

public interface Renamer {

	String doRename(RenameRequest request);
}
