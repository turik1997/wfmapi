package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface Retryable {

    void retry(String conflictingItem);
    void retryAll(String conflictingItem);
}
