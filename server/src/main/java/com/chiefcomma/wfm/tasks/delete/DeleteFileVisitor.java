package com.chiefcomma.wfm.tasks.delete;

import com.chiefcomma.wfm.AbstractFile;
import com.chiefcomma.wfm.tasks.FileCounter;
import com.chiefcomma.wfm.tasks.FileCounterResult;
import com.chiefcomma.wfm.tasks.interfaces.ProgressHandler;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.SimpleFileVisitor;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Hp on 9/27/2017.
 */

public class DeleteFileVisitor extends SimpleFileVisitor {

    private ProgressHandler progressHandler;

    public void setProgressHandler(ProgressHandler progressHandler) {
        this.progressHandler = progressHandler;
    }

    @Override
    public FileVisitResult visitFile(File file) throws IOException {
        return delete(file);
    }

    @Override
    public FileVisitResult postVisitDirectory(File dir, IOException exc) throws IOException {
        if (exc != null) {
            throw exc;
        }
        return delete(dir);
    }

    private FileVisitResult delete(File file) throws IOException {
        if (!AbstractFile.getInstance().delete(file)) {
            throw new IOException("Can't delete file " + file.getAbsolutePath());
        }
        return handleReturn(file, FileVisitResult.CONTINUE);
    }

    private FileVisitResult handleReturn(File file, FileVisitResult result) {
        FileCounterResult done = null;
        if ( file.isDirectory() ) {
            done = new FileCounter(Arrays.asList(file)).countFiles(true);
        } else {
            done = new FileCounterResult(file.length(), 1);
        }
        progressHandler.done(done);
        return result;
    }

}
