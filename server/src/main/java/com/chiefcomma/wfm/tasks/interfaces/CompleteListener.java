package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.FileCounterResult;

/**
 * Created by Hp on 9/30/2017.
 */

public interface CompleteListener {

    void done(FileCounterResult result);
}
