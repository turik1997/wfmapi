package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.PutEntryConflict;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class PutEntryCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		boolean canSkipEntryProblems();
		void skipEntryProblems();
	}

	private ZipEntry zipEntry;
	private ZipOutputStream outputStream;
	private Configurator configurator;
	private ConflictHandler conflictHandler;

	public PutEntryCallable(ZipEntry zipEntry, ZipOutputStream outputStream, Configurator configurator, ConflictHandler conflictHandler) {
		this.zipEntry = zipEntry;
		this.outputStream = outputStream;
		this.configurator = configurator;
		this.conflictHandler = conflictHandler;
	}

	@Override
	public Conflict.Result call() throws Exception {
		try {
			outputStream.putNextEntry(zipEntry);
		} catch (IOException e) {
			if ( configurator.canSkipEntryProblems() )
			{
				return Conflict.Result.SKIP;
			}

			PutEntryConflict conflict = new PutEntryConflict(zipEntry);
			Conflict.Result result = conflictHandler.handle(conflict);
			if ( result == Conflict.Result.SKIP_ALL )
				configurator.skipEntryProblems();

			return result;
		}

		return Conflict.Result.CONTINUE;
	}
}
