package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.utils.SharedSentences;
import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.util.zip.ZipEntry;

public class PutEntryConflict extends Conflict {

	public final static String TYPE = "PUT_ENTRY";
	private ZipEntry zipEntry;

	public PutEntryConflict(ZipEntry zipEntry) {
		super(TYPE);
		String text = SharedSentences.read(SharedSentences.Keys.FILE_TO_ZIP_FAILED, zipEntry.getName());
		dialogBuilder = TripleButtonDialogBuilder.getInstance().setText(text);
		this.zipEntry = zipEntry;
	}

	@Override
	public Result resolve(Action action, String data) {
		switch ( action )
		{
			case SKIP_ALL: case SKIP: case CANCEL: case RETRY:
				return Result.valueOf(action.toString());
		}

		return Result.RETRY;
	}
}
