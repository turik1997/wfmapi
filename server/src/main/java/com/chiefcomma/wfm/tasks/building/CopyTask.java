package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.*;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.copy.CopyDescription;
import com.chiefcomma.wfm.tasks.copy.CopyFileVisitor;
import com.chiefcomma.wfm.tasks.interfaces.*;
import com.chiefcomma.wfm.utils.FileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by turok on 9/10/17.
 */

public class CopyTask extends FileWalkingTask {

    private List<File> files;
    FileCounter fileCounter;
    public final static String ON_FILE_COPIED = "ON_FILE_COPIED";
    public final static String ON_DIR_ENTERED = "ON_DIR_ENTERED";
    public final static String ON_DIR_COPIED = "ON_DIR_COPIED";
    public final static String ON_FILE_ENTERED = "ON_FILE_ENTERED";

    CopyTask(List<File> files, FileWalker fileWalker, FileVisitor fileVisitor) {
        super(files, fileWalker, fileVisitor);
        this.files = files;
    }

    @Override
    protected void onWalkCancelled() {
        if ( fileCounter != null ) {
            fileCounter.cancel();
        }
    }

    @Override
    public void onPrepare() {
        fileCounter = new FileCounter(files);
        FileCounterResult result = fileCounter.countFiles(true);
        this.totalAmountOfFiles = result.getTotalFiles();
    }

    @Override
    public void skipped(File file, FileCounterResult result) {
        for (SkipListener listener : getSkipListeners() )
            listener.skipped(file, result);
    }


}
