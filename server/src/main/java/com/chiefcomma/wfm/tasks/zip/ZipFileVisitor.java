package com.chiefcomma.wfm.tasks.zip;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.FileCounter;
import com.chiefcomma.wfm.tasks.FileCounterResult;
import com.chiefcomma.wfm.tasks.conflictables.*;
import com.chiefcomma.wfm.tasks.interfaces.*;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.ResponseBasedFileVisitor;
import org.nanohttpd.protocols.http.NanoHTTPD;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFileVisitor extends ResponseBasedFileVisitor implements PutEntryCallable.Configurator, OpenInputStreamCallable.Configurator, StreamRWCallable.Configurator {

	private ZipOutputStream zos;
	private File src;
	private boolean isCancelled = false;
	private boolean skipRWProblems = false;
	private boolean skipNotFound = false;
	private ConflictHandler conflictHandler;
	private ArrayList<StartListener> startListeners = new ArrayList<>();
	private ArrayList<CompleteListener> completedListeners = new ArrayList<>();
	private ArrayList<ProgressUpdateListener> updateListeners = new ArrayList<>();

	public ZipFileVisitor(ZipOutputStream outputStream, File srcDir) {
		this.zos = outputStream;
		this.src = srcDir;
	}

	@Override
	public FileVisitResult preVisitDirectory(File dir) throws IOException {
		if ( isCancelled() )
			return FileVisitResult.TERMINATE;
		started(dir);
		ZipEntry ze = new ZipEntry(dir.getAbsolutePath().substring(src.getAbsolutePath().length()+1) + "/");
		zos.putNextEntry(ze);
		zos.closeEntry();

		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(File file) throws IOException {
		if ( isCancelled() )
			return FileVisitResult.TERMINATE;
		started(file);
		FileVisitResult result = null;

		ZipEntry ze = new ZipEntry(file.toString().substring(src.getAbsolutePath().length()+1));
		zos.putNextEntry(ze);

		OpenInputStreamCallable callable = new OpenInputStreamCallable(file, conflictHandler, this);
		result = runConflictableCode(callable);
		if ( shouldReturn(result) )
			return handleReturn(file, result);
		InputStream inputStream = callable.getInputStream();

		result = runConflictableCode(new StreamRWCallable(inputStream, zos, file, updateListeners, conflictHandler, this));
		if ( shouldReturn(result) )
		{
			NanoHTTPD.safeClose(inputStream);
			return handleReturn(file, result);
		}

		try {
			zos.closeEntry();
		} finally {
			NanoHTTPD.safeClose(inputStream);
		}

		completed(new FileCounterResult(file.length(), 1));
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(File dir, IOException exc) throws IOException {
		completed(new FileCounterResult(0, 1));
		if ( isCancelled() )
			return FileVisitResult.TERMINATE;

		return FileVisitResult.CONTINUE;
	}

	@Override
	public void cancel() {
		isCancelled = true;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	private FileVisitResult handleReturn(File file, FileVisitResult result) {
		FileCounterResult done = null;
		if ( file.isDirectory() )
			done = new FileCounter(Arrays.asList(file)).countFiles(true);
		else
			done = new FileCounterResult(file.length(), 1);
		completed(done);
		return result;
	}

	@Override
	public boolean canSkipEntryProblems() {
		return false;
	}

	@Override
	public void skipEntryProblems() {

	}

	@Override
	public boolean canSkipAllNotFound() {
		return skipNotFound;
	}

	@Override
	public void skipAllNotFound() {
		skipNotFound = true;
	}

	private void started(File file) {
		for ( StartListener listener : startListeners )
			listener.startedNewFile(file);
	}

	private void completed(FileCounterResult result) {
		for (CompleteListener listener : completedListeners) {
			listener.done(result);
		}
	}

	@Override
	public boolean canSkipAllRWProblems() {
		return skipRWProblems;
	}

	@Override
	public void skipAllRWProblems() {
		skipRWProblems = true;
	}

	public void setConflictHandler(ConflictHandler conflictHandler) {
		this.conflictHandler = conflictHandler;
	}

	public void addCompletedListeners(CompleteListener completedListener) {
		this.completedListeners.add(completedListener);
	}

	public void addUpdateListeners(ProgressUpdateListener updateListener) {
		this.updateListeners.add(updateListener);
	}

	public void addStartListeners(StartListener startListener) {
		this.startListeners.add(startListener);
	}
}