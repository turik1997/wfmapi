package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface TaskWrapper extends Progressive, Retryable,
        Cancelable, Overwritable, Renamable, Skippable {
}
