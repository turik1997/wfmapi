package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.FileCounterResult;

import java.io.File;

/**
 * Created by turok on 9/14/17.
 */

public interface ProgressHandler extends StartListener, ProgressUpdateListener,
		CompleteListener, SkipListener {
}
