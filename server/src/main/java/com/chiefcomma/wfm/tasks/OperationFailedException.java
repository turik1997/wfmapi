package com.chiefcomma.wfm.tasks;

public class OperationFailedException extends Exception {
    public OperationFailedException(String message) {
        super(message);
    }
}
