package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.AbstractFile;
import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.NotFoundConflict;

import java.io.*;
import java.util.concurrent.Callable;

import static com.chiefcomma.wfm.tasks.conflicts.Conflict.Result.*;

public class OpenInputStreamCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		boolean canSkipAllNotFound();
		void skipAllNotFound();
		void cancel();
	}

	private File srcFile;
	private ConflictHandler conflictHandler;
	private Configurator configurator;
	private InputStream srcInputStream = null;

	public OpenInputStreamCallable(File srcFile, ConflictHandler conflictHandler, Configurator configurator) {
		this.srcFile = srcFile;
		this.conflictHandler = conflictHandler;
		this.configurator = configurator;
	}

	@Override
	public Conflict.Result call() throws Exception {
		try {
			srcInputStream = AbstractFile.getInstance().getInputStream(srcFile);
		} catch (FileNotFoundException e) {
			if ( configurator.canSkipAllNotFound() )
				return SKIP_ALL;

			Conflict.Result result = conflictHandler.handle(new NotFoundConflict(srcFile));
			if ( result == SKIP_ALL )
				configurator.skipAllNotFound();
			else
			if ( result == CANCEL )
				configurator.cancel();
			return result;
		}

		return CONTINUE;
	}

	public InputStream getInputStream() {
		return srcInputStream;
	}
}
