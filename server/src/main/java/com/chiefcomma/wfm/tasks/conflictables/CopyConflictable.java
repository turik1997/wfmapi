package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.SAFConflict;
import com.chiefcomma.wfm.tasks.interfaces.ProgressUpdateListener;
import com.chiefcomma.wfm.utils.FileVisitResult;
import org.nanohttpd.protocols.http.NanoHTTPD;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class CopyConflictable implements Callable<Conflict.Result> {

	private File srcFile;
	private File destFile;
	private ConflictHandler conflictHandler;
	private ConflictableRunner conflictableRunner = new ConflictableRunner();
	private OpenInputStreamCallable.Configurator streamConfigurator;
	private StreamRWCallable.Configurator rwConfigurator;
	private ArrayList<ProgressUpdateListener> listeners;
	private Conflict.Result lastResult = null;

	public CopyConflictable(File srcFile, File destFile, ConflictHandler conflictHandler,
							OpenInputStreamCallable.Configurator streamConfigurator, StreamRWCallable.Configurator rwConfigurator,
							ArrayList<ProgressUpdateListener> listeners) {
		this.srcFile = srcFile;
		this.destFile = destFile;
		this.conflictHandler = conflictHandler;
		this.streamConfigurator = streamConfigurator;
		this.rwConfigurator = rwConfigurator;
		this.listeners = listeners;
	}

	@Override
	public Conflict.Result call() {
		BufferedInputStream srcInputStream = prepareInputStream();
		if ( lastResult != null )
		{
			NanoHTTPD.safeClose(srcInputStream);
			return lastResult;
		}

		FileOutputStream destOutputStream = prepareOutputStream();
		if ( lastResult != null )
		{
			NanoHTTPD.safeClose(srcInputStream);
			NanoHTTPD.safeClose(destOutputStream);
			return lastResult;
		}

		StreamRWCallable rwCallable = new StreamRWCallable(srcInputStream, destOutputStream, srcFile, listeners, conflictHandler, rwConfigurator);
		FileVisitResult conflictableCodeResult = conflictableRunner.runConflictableCode(rwCallable);

		switch ( conflictableCodeResult )
		{
			case SKIP_SUBTREE:
				lastResult = Conflict.Result.SKIP;
				break;
			case TERMINATE:
				lastResult = Conflict.Result.CANCEL;
				break;
		}
		NanoHTTPD.safeClose(srcInputStream);
		NanoHTTPD.safeClose(destOutputStream);
		if ( lastResult != null )
			return lastResult;
		return Conflict.Result.CONTINUE;
	}

	private boolean shouldReturn(FileVisitResult result) {
		return result == FileVisitResult.TERMINATE || result == FileVisitResult.SKIP_SUBTREE;
	}

	private BufferedInputStream prepareInputStream() {
		lastResult = null;
		OpenInputStreamCallable inputStreamCallable = new OpenInputStreamCallable(srcFile, conflictHandler, streamConfigurator);
		FileVisitResult conflictableCodeResult = conflictableRunner.runConflictableCode(inputStreamCallable);

		switch ( conflictableCodeResult )
		{
			case SKIP_SUBTREE:
				lastResult = Conflict.Result.SKIP;
				break;
			case TERMINATE:
				lastResult = Conflict.Result.CANCEL;
				break;
		}

		InputStream inputStream = inputStreamCallable.getInputStream();
		if ( inputStream == null )
			return null;

		return new BufferedInputStream(inputStream, 8192<<1);
	}

	private FileOutputStream prepareOutputStream() {
		lastResult = null;
		OpenOutputStreamCallable outputStreamCallable = new OpenOutputStreamCallable(destFile, conflictHandler, streamConfigurator);
		FileVisitResult conflictableCodeResult = conflictableRunner.runConflictableCode(outputStreamCallable);

		switch ( conflictableCodeResult )
		{
			case SKIP_SUBTREE:
				lastResult = Conflict.Result.SKIP;
				break;
			case TERMINATE:
				lastResult = Conflict.Result.CANCEL;
				break;
		}

		FileOutputStream outputStream = outputStreamCallable.getOutputStream();
		return outputStream;
	}
}
