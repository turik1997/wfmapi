package com.chiefcomma.wfm.tasks.interfaces;

import java.io.File;

/**
 * Created by Hp on 9/30/2017.
 */

public interface ProgressUpdateListener {

    void currentFileUpdate(long totalBytes);
}
