package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.LocaledReader;
import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.io.File;

public class CantRenameConflict extends Conflict {

	public final static String TYPE = "CANT_RENAME";
	private final static String CANT_RENAME = "cant_rename";
	private File conflictingFile;
	private String name;

	public CantRenameConflict(File conflictingFile, String name) {
		super(TYPE);
		this.conflictingFile = conflictingFile;
		this.name = name;
		dialogBuilder = TripleButtonDialogBuilder.getInstance();
		String text = LocaledReader.readLocaleString(TEXT_PATH, CANT_RENAME, conflictingFile.getAbsolutePath());
		dialogBuilder = dialogBuilder.setText(text);
	}

	@Override
	public Result resolve(Action action, String data) {
		switch ( action )
		{
			case RENAME:
			{
				if ( data == null || data.contains("/") )
					return Result.RETRY;
				this.name = data;
				return Result.RENAME;
			}
			case CANCEL:
				return Result.CANCEL;
			case SKIP:
				return Result.SKIP;
		}

		return Result.RETRY;
	}
}
