package com.chiefcomma.wfm.tasks;

import com.chiefcomma.wfm.response.ConflictDialog;

import java.io.File;

/**
 * Created by turok on 9/14/17.
 */

public class TaskProgress {

    public enum Status {
        NOT_STARTED,
        PREPARING,
        IN_PROGRESS,
        CANCELLING,
        CANCELLED,
        COMPLETED
    }

    private double totalDonePercentage;
    private long total;
    private long ready;
    private String currentFile;
    private String workingDir;
    private boolean hasConflict;
    private String conflictType;
    private ConflictDialog dialog;



    public TaskProgress(double totalDonePercentage, long total, long ready, String currentFile, File workingDir) {
        this(totalDonePercentage, total, ready, currentFile, workingDir, false, null, null);
    }

    private TaskProgress(double totalDonePercentage, long total, long ready, String currentFile, File workingDir,
                         boolean hasConflict, String conflictType, ConflictDialog dialog) {
        if ( hasConflict && (conflictType == null || dialog == null) )
        throw new IllegalArgumentException(currentFile + " " + "Conflict cannot be null " + conflictType + " " + dialog);

        this.totalDonePercentage = totalDonePercentage;
        this.total = total;
        this.ready = ready;
        this.currentFile = currentFile;
        this.workingDir = workingDir != null ? workingDir.getAbsolutePath() : null;
        this.hasConflict = hasConflict;
        this.conflictType = conflictType;
        this.dialog = dialog;
    }

    public TaskProgress(double totalDonePercentage, long total, long ready, String currentFile, File workingDir,
                        String conflictType, ConflictDialog dialog) {
        this(totalDonePercentage, total, ready, currentFile, workingDir, conflictType != null && dialog != null, conflictType, dialog);
    }

    public boolean isDone() {
        return total == ready && (long) totalDonePercentage == 100;
    }

    public String getConflictType() {
        return conflictType;
    }

    public ConflictDialog getDialog() {
        return dialog;
    }

    public boolean hasConflict() {
        return hasConflict;
    }
    @Override
    public String toString() {
        return "percentage: " + totalDonePercentage + " ready: " + ready + " total: " + total;
    }

    public void setTotalDonePercentage(double totalDonePercentage) {
        this.totalDonePercentage = totalDonePercentage;
    }

    public double getTotalDonePercentage() {
        return totalDonePercentage;
    }
}
