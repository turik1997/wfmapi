package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.SharedSentences;
import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.io.File;

/**
 * Created by turok on 9/14/17.
 */

public class NotFoundConflict extends Conflict {

    private File conflictingFile;

    public NotFoundConflict(File conflictingFile) {
        super("NotFound");
        this.conflictingFile = conflictingFile;
        String text = SharedSentences.read(SharedSentences.Keys.FILE_NOT_FOUND, conflictingFile.getAbsolutePath());
        dialogBuilder = TripleButtonDialogBuilder.getInstance().setText(text);
    }

    @Override
    public Conflict.Result resolve(Action action, String data) {
        return Conflict.Result.valueOf(action.toString());
    }
}
