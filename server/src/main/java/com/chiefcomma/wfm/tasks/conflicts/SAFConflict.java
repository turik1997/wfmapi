package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.SharedSentences;

import java.io.File;

public class SAFConflict extends Conflict {

	public final static String TYPE = "SAF_CONFLICT";
	private File file;
	private Configurator configurator;

	public interface Configurator {
		void requestSAFAccess(File file);
		boolean isSAF(File file);
	}

	public SAFConflict(File conflictingFile, Configurator configurator) {
		super(TYPE);
		this.file = conflictingFile;
		this.configurator = configurator;
		String btnText = SharedSentences.read(SharedSentences.Keys.SHOW_SAF);
		dialogBuilder = new ConflictDialog.Builder().addButton(Action.CANCEL, btnText);
	}

	@Override
	public Result resolve(Action action, String data) {
		if ( action == Action.CANCEL )
		{
			//configurator.requestSAFAccess(file);
			return Result.CANCEL;
		}

		return Result.RETRY;
	}
}
