package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.SharedSentences;

import java.io.File;
import java.util.regex.Pattern;

/**
 * Created by turok on 9/14/17.
 */

public class DestinationExistsConflict extends Conflict {

    private static String formatSpecifier
            = "%(\\d+\\$)?([-#+ 0,(\\<]*)?(\\d+)?(\\.\\d+)?([tT])?([a-zA-Z%])";
    private static Pattern pattern = Pattern.compile(formatSpecifier);
    private String namePattern = " (%d)";
    private File src;
    private File dest;
    private File conflictingDestFile;
    private File renamedFile = null;
    private ConflictDialog dialog;

    public DestinationExistsConflict(File originalFile, File dest, File conflictingDestFile) {
        super("DestinationExists");
        this.src = originalFile;
        this.dest = dest;
        this.conflictingDestFile = conflictingDestFile;
        ConflictDialog.Builder builder = new ConflictDialog.Builder();
        //builder = builder.addButton(Action.RENAME, "Rename");
        if (!src.getAbsolutePath().equals(conflictingDestFile.getAbsolutePath())) {
            if (conflictingDestFile.isDirectory()) {
                builder = builder
                        .addButton(Action.WRITE_INTO, SharedSentences.read(SharedSentences.Keys.WRITE_INTO))
                        .addButton(Action.WRITE_INTO_ALL, SharedSentences.read(SharedSentences.Keys.WRITE_INTO_ALL));
            } else {
                builder = builder
                        .addButton(Action.OVERWRITE, SharedSentences.read(SharedSentences.Keys.OVERWRITE))
                        .addButton(Action.OVERWRITE_ALL, SharedSentences.read(SharedSentences.Keys.OVERWRITE_ALL));
            }
        }

        dialog = builder
                //.addButton(Action.RENAME, SharedSentences.read(SharedSentences.Keys.RENAME))
                .addButton(Action.SKIP, SharedSentences.read(SharedSentences.Keys.SKIP))
                .addButton(Action.SKIP_ALL, SharedSentences.read(SharedSentences.Keys.SKIP_ALL))
                .addButton(Action.CANCEL, SharedSentences.read(SharedSentences.Keys.CANCEL))
                .setText(SharedSentences.read(SharedSentences.Keys.FILE_EXISTS, conflictingDestFile.getName(), conflictingDestFile.getParent()))
                .build();
    }

    @Override
    public Conflict.Result resolve(Action action, String newName) {
        switch (action) {
            case RENAME:
                if (newName == null) {
                    return Conflict.Result.RETRY;
                }

                File parentDir = conflictingDestFile.getParentFile();
                renamedFile = new File(parentDir, newName);
                return Result.RENAME;
            case RENAME_ALL:
                renamedFile = rename(conflictingDestFile);
                return Conflict.Result.RENAME_ALL;
        }

        return Conflict.Result.valueOf(action.toString());
    }

    public File getRenamedFile() {
        return renamedFile;
    }

    public String getNamePattern() {
        return namePattern;
    }

    private String getSubtree(File parent, File child) {
        return child.getAbsolutePath().substring(parent.getAbsolutePath().length());
    }

    @Override
    public ConflictDialog getDialog() {
        return dialog;
    }

    public boolean isDir() {
        return src.isDirectory();
    }

    private File rename(File dirCopy) {
        String fileNamePart = dirCopy.getName();
        String fileExtPart = "";
        if (dirCopy.getName().lastIndexOf('.') != -1) {
            fileNamePart = dirCopy.getName().substring(0, dirCopy.getName().lastIndexOf('.'));
            fileExtPart = dirCopy.getName().substring(fileNamePart.length());
        }
        File renamedFile = dirCopy;
        for (int i = 1; renamedFile.exists(); i++) {
            String newName = fileNamePart + String.format(" (%d)", i) + fileExtPart;
            renamedFile = new File(dirCopy.getParent(), newName);
        }
        return renamedFile;
    }
}
