package com.chiefcomma.wfm.tasks.listeners;

import com.chiefcomma.wfm.tasks.FileCounterResult;
import com.chiefcomma.wfm.tasks.interfaces.CompleteListener;
import com.chiefcomma.wfm.tasks.interfaces.ProgressHandler;
import com.chiefcomma.wfm.tasks.interfaces.ProgressUpdateListener;
import com.chiefcomma.wfm.tasks.interfaces.StartListener;

import java.io.File;

/**
 * Created by Hp on 9/30/2017.
 */

public class FileProgressListener implements CompleteListener, ProgressUpdateListener, StartListener {

    ProgressHandler progressHandler;

    public FileProgressListener(ProgressHandler progressHandler) {
        this.progressHandler = progressHandler;
    }

    @Override
    public void startedNewFile(File file) {
        progressHandler.startedNewFile(file);
    }

    @Override
    public void done(FileCounterResult result) {
        progressHandler.done(result);
    }

    @Override
    public void currentFileUpdate(long readyBytes) {
        progressHandler.currentFileUpdate(readyBytes);
    }
}
