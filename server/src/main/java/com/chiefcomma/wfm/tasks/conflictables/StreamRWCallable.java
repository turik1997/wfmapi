package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.StreamRWConflict;
import com.chiefcomma.wfm.tasks.interfaces.ProgressUpdateListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.Callable;

public class StreamRWCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		boolean canSkipAllRWProblems();
		void skipAllRWProblems();
		void cancel();
		boolean isCancelled();
	}

	private InputStream inputStream;
	private OutputStream outputStream;
	private ArrayList<ProgressUpdateListener> listeners;
	private ConflictHandler conflictHandler;
	private Configurator configurator;
	private File inputFile;

	public StreamRWCallable(InputStream inputStream, OutputStream outputStream, File inputFile, ArrayList<ProgressUpdateListener> listeners, ConflictHandler conflictHandler, Configurator configurator) {
		this.inputStream = inputStream;
		this.outputStream = outputStream;
		this.listeners = listeners;
		this.conflictHandler = conflictHandler;
		this.configurator = configurator;
		this.inputFile = inputFile;
	}

	@Override
	public Conflict.Result call() throws Exception {
		int read = -1;
		byte[] buf = new byte[8192];
		try {
			while ( (read = inputStream.read(buf, 0, buf.length)) != -1 && !Thread.currentThread().isInterrupted() && !configurator.isCancelled() )
			{
				outputStream.write(buf, 0, read);
				for ( ProgressUpdateListener listener : listeners )
					listener.currentFileUpdate(read);
			}
			if ( Thread.currentThread().isInterrupted() || configurator.isCancelled() )
				return Conflict.Result.CANCEL;
		} catch (IOException e) {
			if ( configurator.canSkipAllRWProblems() )
				return Conflict.Result.SKIP_ALL;

			Conflict.Result result = conflictHandler.handle(new StreamRWConflict(inputFile));
			if ( result == Conflict.Result.SKIP_ALL )
				configurator.skipAllRWProblems();
			else
			if ( result == Conflict.Result.CANCEL )
				configurator.cancel();

			return result;
		}

		return Conflict.Result.CONTINUE;
	}
}
