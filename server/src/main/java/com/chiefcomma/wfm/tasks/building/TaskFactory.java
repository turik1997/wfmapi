package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.copy.CopyDescription;
import com.chiefcomma.wfm.tasks.copy.CopyFileVisitor;
import com.chiefcomma.wfm.tasks.delete.DeleteFileVisitor;
import com.chiefcomma.wfm.tasks.interfaces.Builder;
import com.chiefcomma.wfm.tasks.zip.ZipFileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;

/**
 * Created by turok on 9/10/17.
 */

public class TaskFactory implements Builder {

    @Override
    public Task buildCopyTask(List<String> fileNames, File destDir, File src) {
        //todo check all code here
        List<File> files = new ArrayList<>();
        for (String fileName : fileNames) {
            files.add(new File(src, fileName));
        }
        CopyFileVisitor copyFileVisitor = new CopyFileVisitor(src, destDir);
        CopyTask copyTask = new CopyTask(files, new FileWalker(), copyFileVisitor);
        copyFileVisitor.setConflictHandler(copyTask);
        copyFileVisitor.addSkipListeners(copyTask);
        copyFileVisitor.addStartListeners(copyTask);
        copyFileVisitor.addUpdateListeners(copyTask);
        copyFileVisitor.addCompletedListeners(copyTask);
        CopyDescription copyDescription = new CopyDescription(src, destDir);
        copyTask.setDescription(copyDescription);
        return copyTask;
    }

    @Override
    public Task buildMoveTask(List<String> fileNames, File destDir, File srcDir) {
        List<File> files = new ArrayList<>();
        for (String fileName : fileNames) {
            files.add(new File(srcDir, fileName));
        }
        CopyTask copyTask = (CopyTask) buildCopyTask(fileNames, destDir, srcDir);
        DeleteTask deleteTask = (DeleteTask) buildDeleteTask(fileNames, srcDir);
        MoveTask moveTask = new MoveTask(files, copyTask, deleteTask);
        ((CopyFileVisitor)copyTask.getFileVisitor()).setConflictHandler(moveTask);
        return moveTask;
    }

    @Override
    public Task buildDeleteTask(List<String> fileNames, File srcDir) {
        ArrayList<File> files = new ArrayList<File>();
        for (String name : fileNames) {
            files.add(new File(srcDir, name));
        }
        DeleteFileVisitor deleteFileVisitor = new DeleteFileVisitor();
        DeleteTask deleteTask = new DeleteTask(files, new FileWalker(), deleteFileVisitor);
        deleteFileVisitor.setProgressHandler(deleteTask);
        return deleteTask;
    }

    @Override
    public Task buildRenameTask(File file, String name, String prefix, String suffix) {
        return new RenameTask(file, name);
    }

    public Task buildZipTask(ArrayList<String> fileNames, File srcDir, OutputStream outputStream) {
        ZipFileVisitor zipFileVisitor = new ZipFileVisitor(
                new ZipOutputStream(outputStream),
                srcDir
        );
        ArrayList<File> files = new ArrayList<File>();
        for (String name : fileNames) {
            files.add(new File(srcDir, name));
        }
        ZipTask zipTask = new ZipTask(files, new FileWalker(), zipFileVisitor);
        zipFileVisitor.setConflictHandler(zipTask);
        zipFileVisitor.addStartListeners(zipTask);
        zipFileVisitor.addCompletedListeners(zipTask);
        zipFileVisitor.addUpdateListeners(zipTask);
        return zipTask;
    }
}
