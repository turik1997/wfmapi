package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.CantDeleteConflict;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.SAFConflict;

import java.io.File;
import java.util.concurrent.Callable;

public class DeleteCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		boolean canSkipDeletingProblems();
		void skipDeletingProblems();
		void cancel();
	}

	private File file;
	private Configurator configurator;
	private ConflictHandler conflictHandler;
	private SAFConflict.Configurator safConfigurator;

	public DeleteCallable(File file, Configurator configurator, SAFConflict.Configurator safConfigurator, ConflictHandler conflictHandler) {
		this.file = file;
		this.configurator = configurator;
		this.conflictHandler = conflictHandler;
		this.safConfigurator = safConfigurator;
	}

	@Override
	public Conflict.Result call() throws Exception {

		if ( configurator.canSkipDeletingProblems() )
			return Conflict.Result.SKIP;

		Conflict.Result result = safConfigurator.isSAF(file)
			? conflictHandler.handle(new SAFConflict(file, safConfigurator))
			: conflictHandler.handle(new CantDeleteConflict(file));
		if ( result == Conflict.Result.SKIP_ALL )
			configurator.skipDeletingProblems();
		return result;
	}
}
