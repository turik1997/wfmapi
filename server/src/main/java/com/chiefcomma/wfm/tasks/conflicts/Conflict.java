package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.tasks.interfaces.Resolvable;

/**
 * Created by turok on 9/10/17.
 */

public abstract class Conflict implements Resolvable {

    protected final static String TEXT_PATH = "com.chiefcomma.wfm.tasks.ConflictMessages";
    private String type;
    protected ConflictDialog.Builder dialogBuilder;

    public Conflict(String type) {
        this.type = type;
    }

    public enum Action {
        CANCEL,

        RETRY,

        SKIP,

        SKIP_ALL,

        RENAME,

        RENAME_ALL,

        OVERWRITE,

        OVERWRITE_ALL,

        WRITE_INTO,

        WRITE_INTO_ALL,

        SHOW_SAF_DIALOG,

        CONTINUE
    }

    public enum Result {
        CANCEL,

        RETRY,

        SKIP,

        SKIP_ALL,

        RENAME,

        RENAME_ALL,

        OVERWRITE,

        OVERWRITE_ALL,

        WRITE_INTO,

        WRITE_INTO_ALL,

        CONTINUE
    }

    public ConflictDialog getDialog() {
        return dialogBuilder.build();
    }

    public final String getType() {
        return type;
    }


    public static class ConflictInfo {

        private String conflictType;
        private ConflictDialog dialog;

        public ConflictInfo(Conflict conflict) {
            if ( conflict == null )
                return;
            conflictType = conflict.getType();
            dialog = conflict.getDialog();
        }

        public String getConflictType() {
            return conflictType;
        }

        public ConflictDialog getDialog() {
            return dialog;
        }
    }
}
