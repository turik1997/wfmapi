package com.chiefcomma.wfm.tasks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by turok on 9/10/17.
 */

public class FileCounterResult {

    private long totalBytes = 0;
    private int totalFiles = 0;
    private List<File> files = new ArrayList<>();

    public FileCounterResult(long totalBytes, int totalFiles) {
        this.totalBytes = totalBytes;
        this.totalFiles = totalFiles;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public int getTotalFiles() {
        return totalFiles;
    }

    public void attachFiles(List<File> files) {
        this.files.addAll(files);
    }
}
