package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.conflicts.CantRenameConflict;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.tasks.conflicts.DestinationExistsConflict;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

public class RenameCallable implements Callable<Conflict.Result> {

	public interface Configurator {
		void skipAllRenamingProblems();
		boolean canSkipRenamingProblems();
		boolean canRenameAll();
		void renameAll();
	}

	private File target;
	private String name;
	private Configurator configurator;
	private ConflictHandler conflictHandler;

	public RenameCallable(File target, String name, Configurator configurator, ConflictHandler conflictHandler) {
		this.target = target;
		this.name = name;
		this.configurator = configurator;
		this.conflictHandler = conflictHandler;
	}

	@Override
	public Conflict.Result call() throws Exception {
		File parentDir = target.getParentFile();
		File destination = new File(parentDir, name);
		if (destination.exists()) {
			return this.conflictHandler.handle(new DestinationExistsConflict(target, parentDir, destination));
		}

		if (!target.renameTo(destination)) {
			throw new IOException("Can't rename " + target.getAbsolutePath() + " to " + name);
		}

		return Conflict.Result.CONTINUE;
	}
}
