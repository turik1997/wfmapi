package com.chiefcomma.wfm.tasks.conflicts;

public interface CantReadConflictConfig {

	boolean canSkip();
}
