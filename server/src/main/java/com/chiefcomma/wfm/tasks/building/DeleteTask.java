package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.*;
import com.chiefcomma.wfm.utils.FileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Hp on 9/27/2017.
 */

public class DeleteTask extends FileWalkingTask {

    private ArrayList<File> files;
    private FileCounter fileCounter;

    DeleteTask(ArrayList<File> files, FileWalker fileWalker, FileVisitor fileVisitor) {
        super(files, fileWalker, fileVisitor);
        this.files = files;
        this.fileCounter = new FileCounter(this.files);
    }

    @Override
    public void onPrepare() {
        FileCounterResult result = fileCounter.countFiles(true);
        this.totalAmountOfFiles = result.getTotalFiles();
    }

    @Override
    protected void onWalkCancelled() {
        if ( fileCounter != null ) {
            fileCounter.cancel();
        }
    }
}
