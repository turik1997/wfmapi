package com.chiefcomma.wfm.tasks.interfaces;

import com.chiefcomma.wfm.tasks.FileCounterResult;

import java.io.File;

public interface SkipListener {

	void skipped(File file, FileCounterResult result);
}
