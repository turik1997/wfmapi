package com.chiefcomma.wfm.tasks;

import java.util.ArrayList;

public class Description {

	protected ArrayList<DescriptionLine> description = new ArrayList<DescriptionLine>();

	public ArrayList<DescriptionLine> getDescription() {
		return description;
	}
}
