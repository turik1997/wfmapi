package com.chiefcomma.wfm.tasks.conflictables;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import com.chiefcomma.wfm.utils.FileVisitResult;

import java.util.concurrent.Callable;

public class ConflictableRunner {

	public FileVisitResult runConflictableCode(Callable<Conflict.Result> code) {
		Conflict.Result result = Conflict.Result.RETRY;
		while ( result == Conflict.Result.RETRY )
		{
			try {
				result = code.call();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		switch ( result )
		{
			case CANCEL:
				return FileVisitResult.TERMINATE;
			case SKIP: case SKIP_ALL:
			{
				return FileVisitResult.SKIP_SUBTREE;
			}
		}


		return FileVisitResult.CONTINUE;
	}
}
