package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.DescriptionLine;
import com.chiefcomma.wfm.tasks.OperationFailedException;
import com.chiefcomma.wfm.utils.FileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class FileWalkingTask extends Task {
    private final List<File> files;
    private final FileWalker fileWalker;
    private final FileVisitor fileVisitor;

    protected abstract void onWalkCancelled();

    public FileWalkingTask(List<File> files, FileWalker fileWalker, FileVisitor fileVisitor) {
        this.files = files;
        this.fileWalker = fileWalker;
        this.fileVisitor = fileVisitor;
    }

    public void runVisitorForEachFile(List<File> files) throws IOException {
        for (File file : files) {
            this.fileWalker.walkTree(file, this.fileVisitor);
        }
    }

    @Override
    public void onStart() throws OperationFailedException {
        try {
            runVisitorForEachFile(this.files);
        } catch (IOException e) {
            e.printStackTrace();
            ArrayList<DescriptionLine> descriptions = getDescription().getDescription();
            String operationTitle = null;
            if (!descriptions.isEmpty()) {
                operationTitle = descriptions.get(0).toString();
            }
            throw new OperationFailedException(
                    "Error during the operation" +
                            (operationTitle == null ? ": " : operationTitle + ": ") +
                            e.getMessage()
            );
        }
    }

    @Override
    public void onCancel() {
        this.fileWalker.cancel();
        this.onWalkCancelled();
    }

    public FileVisitor getFileVisitor() {
        return fileVisitor;
    }
}
