package com.chiefcomma.wfm.tasks.copy;

import com.chiefcomma.wfm.AbstractFile;
import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.FileCounter;
import com.chiefcomma.wfm.tasks.FileCounterResult;
import com.chiefcomma.wfm.tasks.conflictables.*;
import com.chiefcomma.wfm.tasks.interfaces.*;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.ResponseBasedFileVisitor;
import org.nanohttpd.protocols.http.NanoHTTPD;

import java.io.*;
import java.util.*;

/**
 * Created by turok on 9/14/17.
 */

public class CopyFileVisitor extends ResponseBasedFileVisitor implements DestinationExistsCallable.Configurator, MkdirsCallable.Configurator, OpenInputStreamCallable.Configurator, StreamRWCallable.Configurator{

    private ConflictHandler conflictHandler;
    private ArrayList<CompleteListener> completedListeners = new ArrayList<>();
    private ArrayList<ProgressUpdateListener> updateListeners = new ArrayList<>();
    private ArrayList<StartListener> startListeners = new ArrayList<>();
    private ArrayList<SkipListener> skipListeners = new ArrayList<>();
    private File src;
    private File dest;

    private boolean skipAllDirs = false;
    private boolean skipNotFoundFiles = false;
    private boolean skipNameCollidedFiles = false;
    private boolean renameAllDirs = false;
    private boolean renameAllFiles = false;
    private boolean overwriteAll = false;
    private boolean mergeAll = false;
    private boolean isSubtreeSkipped = false;
    private boolean isCanceled = false;
    private boolean isSkipAllLocked = false;
    private boolean skipDirMakingProblem = false;
    private boolean skipRWProblems = false;

    public CopyFileVisitor(File src, File dest) {
        this.src = src;
        this.dest = dest;
    }

    public void setConflictHandler(ConflictHandler conflictHandler) {
        this.conflictHandler = conflictHandler;
    }

    public void addCompletedListeners(CompleteListener completedListener) {
        this.completedListeners.add(completedListener);
    }

    public void addUpdateListeners(ProgressUpdateListener updateListener) {
        this.updateListeners.add(updateListener);
    }

    public void addStartListeners(StartListener startListener) {
        this.startListeners.add(startListener);
    }

    public void addSkipListeners(SkipListener skipListener) {
        this.skipListeners.add(skipListener);
    }

    @Override
    public FileVisitResult preVisitDirectory(File dir) throws IOException {
        FileVisitResult result = null;
        started(dir);

        File dirCopy = getDestFile(dir);
        if (!dirCopy.exists()) {
            mkdir(dirCopy);
            return FileVisitResult.CONTINUE;
        } else if (!dirCopy.isDirectory()) {
            throw new IOException("File with the name " + dirCopy.getName() + " already exists at " + dirCopy.getParentFile().getAbsolutePath());
        } else if (canMergeExistingDirs()) {
            return FileVisitResult.CONTINUE;
        } else if (canSkipExistingDirs()) {
            return handleReturn(dir, FileVisitResult.SKIP_SUBTREE);
        } else if (canRenameAll()) {
            File renamedFile = rename(dirCopy);
            mkdir(renamedFile);
            return FileVisitResult.CONTINUE;
        }

        DestinationExistsCallable callable = new DestinationExistsCallable(this, this, conflictHandler, dir, dest, dirCopy);
        result = runConflictableCode(callable);
        if (shouldReturn(result)) {
            return handleReturn(dir, result);
        }

        return FileVisitResult.CONTINUE;
    }

    private File rename(File dirCopy) {
        String fileNamePart = dirCopy.getName();
        String fileExtPart = "";
        if (dirCopy.getName().lastIndexOf('.') != -1) {
            fileNamePart = dirCopy.getName().substring(0, dirCopy.getName().lastIndexOf('.'));
            fileExtPart = dirCopy.getName().substring(fileNamePart.length());
        }
        File renamedFile = dirCopy;
        for (int i = 1; renamedFile.exists(); i++) {
            String newName = fileNamePart + String.format(" (%d)", i) + fileExtPart;
            renamedFile = new File(dirCopy.getParent(), newName);
        }
        return renamedFile;
    }

    @Override
    public FileVisitResult visitFile(File file) throws IOException {
        FileVisitResult result = null;
        started(file);

        File fileCopy = getDestFile(file);
        if (fileCopy.exists()) {
            if (!canOverwriteExistingFiles()) {
                if (canSkipExistingFiles()) {
                    return handleReturn(file, FileVisitResult.SKIP_SUBTREE);
                } else if (canRenameAll()) {
                    File renamedFile = rename(fileCopy);
                    return copy(file, renamedFile);
                }

            } else if (fileCopy.isFile()) {
                return copy(file, fileCopy);
            }
            DestinationExistsCallable existsCallable = new DestinationExistsCallable(this, this, conflictHandler, file, dest, fileCopy);
            result = runConflictableCode(existsCallable);
            if (shouldReturn(result)) {
                return handleReturn(file, result);
            }
            fileCopy = existsCallable.getRenamedFile();
        }
        return copy(file, fileCopy);
    }

    @Override
    public FileVisitResult postVisitDirectory(File dir, IOException exc) throws IOException {
        if (exc != null) {
            throw exc;
        }
        completed(new FileCounterResult(0, 1));
        return FileVisitResult.CONTINUE;
    }

    protected File getDestFile(File file) {
        String subtree = file.getAbsolutePath().substring(this.src.getAbsolutePath().length()+1);
        return new File(dest, subtree);
    }

    @Override
    public void cancel() {
        isCanceled = true;
    }

    @Override
    public boolean isCancelled() {
        return this.isCanceled;
    }

    @Override
    public void skipExistingDirs() {
        skipAllDirs = true;
    }

    @Override
    public void skipExistingFiles() {
        skipNameCollidedFiles = true;
    }

    @Override
    public boolean canSkipExistingFiles() {
        return skipNameCollidedFiles;
    }

    @Override
    public boolean canSkipExistingDirs() {
        return skipAllDirs;
    }

    @Override
    public void mergeExistingDirs() {
        mergeAll = true;
    }

    @Override
    public void overwriteExistingFiles() {
        overwriteAll = true;
    }

    @Override
    public boolean canOverwriteExistingFiles() {
        return overwriteAll;
    }

    @Override
    public boolean canMergeExistingDirs() {
        return mergeAll;
    }

    @Override
    public boolean canRenameAll() {
        return renameAllDirs;
    }

    @Override
    public void renameAll() {
        renameAllDirs = true;
    }

    @Override
    public void skipDirMakingProblem() {
        skipDirMakingProblem = true;
    }

    @Override
    public boolean canSkipDirMakingProblem() {
        return skipDirMakingProblem;
    }

    @Override
    public boolean canSkipAllNotFound() {
        return skipNotFoundFiles;
    }

    @Override
    public void skipAllNotFound() {
        skipNotFoundFiles = true;
    }

    @Override
    public boolean canSkipAllRWProblems() {
        return skipRWProblems;
    }

    @Override
    public void skipAllRWProblems() {
        skipRWProblems = true;
    }

    private void completed(FileCounterResult result) {
        for ( CompleteListener listener : completedListeners ) {
            listener.done(result);
        }
    }

    private void started(File file) {
        for ( StartListener listener : startListeners )
            listener.startedNewFile(file);
    }

    private FileVisitResult handleReturn(File file, FileVisitResult result) {
        FileCounterResult done = null;
        if (!file.isDirectory()) {
            done = new FileCounterResult(file.length(), 1);
        }
        else
            done = new FileCounter(Arrays.asList(file)).countFiles(true);
        skipped(file, done);

        return result;
    }

    private FileVisitResult copy(File src, File dest) throws IOException {
        InputStream srcFileInputStream = AbstractFile.getInstance().getInputStream(src);
        OutputStream destFileOutputStream = AbstractFile.getInstance().getOutputStream(dest);
        int read = -1;
        byte[] buf = new byte[8192];
        while ((read = srcFileInputStream.read(buf, 0, buf.length)) != -1 && !Thread.currentThread().isInterrupted() && !isCancelled()) {
            destFileOutputStream.write(buf, 0, read);
            for (ProgressUpdateListener listener : updateListeners) {
                listener.currentFileUpdate(read);
            }
        }

        NanoHTTPD.safeClose(srcFileInputStream);
        NanoHTTPD.safeClose(destFileOutputStream);

        if (Thread.currentThread().isInterrupted() || isCancelled()) {
            return FileVisitResult.TERMINATE;
        }
        FileCounterResult fileCounterResult = new FileCounterResult(0, 1);
        fileCounterResult.attachFiles(Arrays.asList(src));
        completed(fileCounterResult);
        return FileVisitResult.CONTINUE;
    }

    private void skipped(File file, FileCounterResult result) {
        completed(result);
        for (SkipListener listener : skipListeners) {
            listener.skipped(file, result);
        }
    }

    private void mkdir(File dir) throws IOException {
        if (!AbstractFile.getInstance().mkdir(dir)) {
            throw new IOException("Can't create the directory " + dir.getAbsolutePath());
        }
    }
}
