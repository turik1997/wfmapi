package com.chiefcomma.wfm.tasks.conflicts;

import com.chiefcomma.wfm.response.ConflictDialog;
import com.chiefcomma.wfm.utils.TripleButtonDialogBuilder;

import java.io.File;

public class StreamRWConflict extends Conflict {

	private File inputFile;
	public final static String TYPE = "RW_CONFLICT";

	public StreamRWConflict(File inputFile) {
		super(TYPE);
		this.inputFile = inputFile;
		dialogBuilder = TripleButtonDialogBuilder.getInstance().setText("Problem during copying " + inputFile.getAbsolutePath());
	}

	@Override
	public Result resolve(Action action, String data) {
		switch ( action )
		{
			case SKIP_ALL: case SKIP: case CANCEL: case RETRY:
				return Result.valueOf(action.toString());
		}

		return Result.RETRY;
	}
}
