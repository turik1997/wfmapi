package com.chiefcomma.wfm.tasks.delete;

import com.chiefcomma.wfm.tasks.Description;
import com.chiefcomma.wfm.tasks.DescriptionLine;
import com.chiefcomma.wfm.utils.SharedSentences;

import java.io.File;

public class DeleteDescription extends Description {

	public DeleteDescription(File fromDir) {
		String fromName = SharedSentences.read(SharedSentences.Keys.FROM);
		description.add(new DescriptionLine(fromName, fromDir.getAbsolutePath()));
	}
}
