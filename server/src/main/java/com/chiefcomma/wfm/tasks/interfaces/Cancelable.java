package com.chiefcomma.wfm.tasks.interfaces;

/**
 * Created by turok on 9/10/17.
 */

public interface Cancelable {

    void cancel();
}
