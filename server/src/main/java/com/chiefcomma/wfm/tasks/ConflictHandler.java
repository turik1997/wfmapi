package com.chiefcomma.wfm.tasks;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;

/**
 * Created by turok on 9/14/17.
 */

public interface ConflictHandler {

    Conflict.Result handle(Conflict conflict);
}
