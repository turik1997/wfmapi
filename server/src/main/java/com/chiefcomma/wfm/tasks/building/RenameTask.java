package com.chiefcomma.wfm.tasks.building;

import com.chiefcomma.wfm.tasks.ConflictHandler;
import com.chiefcomma.wfm.tasks.Description;
import com.chiefcomma.wfm.tasks.TaskProgress;
import com.chiefcomma.wfm.tasks.conflictables.ConflictableRunner;
import com.chiefcomma.wfm.tasks.conflictables.RenameCallable;
import com.chiefcomma.wfm.tasks.conflicts.Conflict;

import java.io.File;

public class RenameTask extends Task implements RenameCallable.Configurator, ConflictHandler {

	private File targetFile;
	private String name;
	private ConflictableRunner conflictableRunner = new ConflictableRunner();

	RenameTask(File file, String name) {
		this.targetFile = file;
		this.name = name;
	}


	@Override
	public TaskProgress getProgress() {
		return null;
	}

	@Override
	public void run() {
		conflictableRunner.runConflictableCode(new RenameCallable(targetFile, name, this, this));
	}

	@Override
	public void skipAllRenamingProblems() {

	}

	@Override
	public void resolve(Conflict.Action action, String data) {
		super.resolve(action, data);
	}

	@Override
	public boolean canSkipRenamingProblems() {
		return false;
	}

	@Override
	public boolean canRenameAll() {
		return false;
	}

	@Override
	public void renameAll() {

	}

	@Override
	public Conflict.Result handle(Conflict conflict) {
		return stickToConflict(conflict);
	}

	@Override
	public Description getDescription() {
		return null;
	}

	@Override
	public void onPrepare() {

	}

	@Override
	public void onStart() {

	}

	@Override
	public void onCancel() {

	}
}
