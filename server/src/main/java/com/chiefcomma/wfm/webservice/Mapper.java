package com.chiefcomma.wfm.webservice;

/**
 * Created by Hp on 10/3/2017.
 */

public interface Mapper<X, Y> {

    Y map(X path);
    void set(X key, Y val);
}
