package com.chiefcomma.wfm.webservice;

import com.chiefcomma.wfm.webservice.api.response.ui_tree_node.UiNode;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.FileVisitor;
import com.chiefcomma.wfm.utils.FileWalker;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Hp on 10/3/2017.
 */

public class NodesGenerator extends FileWalker implements OnNodeCreatedListener {

    private ArrayList<UiNode> nodes = new ArrayList<UiNode>(500);
    private File endDir;
    private Mapper<String, String> mapper;

    @Override
    public void onCreated(UiNode node) {
        nodes.add(node);
    }

    @Override
    public FileVisitResult walkTree(File start, FileVisitor visitor) {
        FileVisitResult result = null;
        try {
            result = super.walkTree(start, new NodeVisitor(endDir, mapper, this));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String id = mapper.map(start.getAbsolutePath());
        //todo consider adding first caller directory into nodes array
        //onCreated(new UiNode());
        return result;
    }
}
