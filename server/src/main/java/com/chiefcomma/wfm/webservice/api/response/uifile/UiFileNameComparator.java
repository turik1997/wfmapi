package com.chiefcomma.wfm.webservice.api.response.uifile;

import java.util.Comparator;

/**
 * Created by turok on 9/11/17.
 */

public class UiFileNameComparator implements Comparator<Integer> {

    private UiFile[] files;
    private boolean isAscending;
    private int property;

    public UiFileNameComparator(UiFile[] files) {
        this(files, true, UiFile.NAME);
    }

    public UiFileNameComparator(UiFile[] files, boolean ascending, int property) {
        this.files = files;
        this.isAscending = ascending;
        this.property = property;
    }

    @Override
    public int compare(Integer lhs, Integer rhs) {

        UiFile left = files[lhs];
        UiFile right = files[rhs];
        int coeff = isAscending ? 1 : -1;

        return left.compareTo(right, property) * coeff;
    }


}
