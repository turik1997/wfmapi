package com.chiefcomma.wfm.webservice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Hp on 10/3/2017.
 */

public class PathToIdMapper implements Mapper<String, Integer> {

    private Map<String, Integer> pathToId = new HashMap<String, Integer>();
    private Object lock = new Object();

    public PathToIdMapper(Map<String, Integer> initialValues) {
        if ( initialValues == null )
            return;
        pathToId.putAll(initialValues);
    }

    @Override
    public void set(String path, Integer id) {
        synchronized ( lock )
        {
            if ( path == null || id == null )
                return;
            pathToId.put(path, id);
        }
    }

    @Override
    public Integer map(String path) {
        synchronized ( lock )
        {
            Integer id = pathToId.get(path);
            if ( id == null )
            {
                id = putPath(path);
            }

            return id;
        }
    }

    private Integer putPath(String path) {
        int id = pathToId.size();
        pathToId.put(path, id);
        return id;
    }
}
