package com.chiefcomma.wfm.webservice.api.response;

import com.chiefcomma.wfm.tasks.conflicts.Conflict;
import org.nanohttpd.protocols.http.request.Method;

public class ErrorAction {
    private Method httpMethod;
    private String url;
    private String dataToSubmit;
    private Conflict.Action actionType;
    private String actionText;
    private String actionDescription;

    public Method getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(Method httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDataToSubmit() {
        return dataToSubmit;
    }

    public void setDataToSubmit(String dataToSubmit) {
        this.dataToSubmit = dataToSubmit;
    }

    public Conflict.Action getActionType() {
        return actionType;
    }

    public void setActionType(Conflict.Action actionType) {
        this.actionType = actionType;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public void setActionDescription(String actionDescription) {
        this.actionDescription = actionDescription;
    }
}
