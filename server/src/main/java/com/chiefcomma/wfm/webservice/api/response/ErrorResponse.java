package com.chiefcomma.wfm.webservice.api.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hp on 10/2/2017.
 */

public class ErrorResponse {

    private String errorType;
    private String error;
    private String reason;
    private List<ErrorAction> actions = new ArrayList<>();

    public ErrorResponse(String error, String reason) {
        this.error = error;
        this.reason = reason;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<ErrorAction> getActions() {
        return actions;
    }

    public void setActions(List<ErrorAction> actions) {
        this.actions = actions;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}

