package com.chiefcomma.wfm.webservice;

import java.util.ArrayList;

/**
 * Created by Hp on 10/5/2017.
 */

public class IdToPathMapper implements Mapper<Integer, String> {

    private ArrayList<String> paths = new ArrayList<String>();
    private Object lock = new Object();

    @Override
    public String map(Integer id) {
        synchronized ( lock )
        {
            if ( id == null || id >= paths.size() || id < 0 )
                return null;
            return paths.get(id);
        }
    }

    @Override
    public void set(Integer id, String path) {
        synchronized ( lock )
        {
            if ( path == null || id == null )
                return;
            if ( id < paths.size() )
            {
                paths.set(id, path);
                return;
            }
            while ( id > paths.size() )
            {
                paths.add(null);
            }

            paths.add(path);
        }
    }
}
