package com.chiefcomma.wfm.webservice.api.response;

import com.chiefcomma.wfm.webservice.api.response.uifile.UiFile;

/**
 * Created by turok on 9/11/17.
 */

public class DirContent {

    private UiFile[] files;
    private Integer[] ascName;
    private Integer[] descName;
    private Integer[] ascSize;
    private Integer[] descSize;
    private Integer[] ascDate;
    private Integer[] descDate;

    public DirContent(UiFile[] files, Integer[] ascName, Integer[] descName, Integer[] ascSize, Integer[] descSize, Integer[] ascDate, Integer[] descDate) {
        this.files = files;
        this.ascName = ascName;
        this.descName = descName;
        this.ascSize = ascSize;
        this.descSize = descSize;
        this.ascDate = ascDate;
        this.descDate = descDate;
    }

    public UiFile[] getFiles() {
        return files;
    }

    public Integer[] getAscName() {
        return ascName;
    }

    public Integer[] getDescName() {
        return descName;
    }

    public Integer[] getAscSize() {
        return ascSize;
    }

    public Integer[] getDescSize() {
        return descSize;
    }

    public Integer[] getAscDate() {
        return ascDate;
    }

    public Integer[] getDescDate() {
        return descDate;
    }
}
