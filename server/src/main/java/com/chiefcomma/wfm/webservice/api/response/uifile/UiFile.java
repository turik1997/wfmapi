package com.chiefcomma.wfm.webservice.api.response.uifile;

import java.io.File;

/**
 * Created by turok on 9/11/17.
 */

public class UiFile {

    public static int FILE = 1;
    public static int DIR = 2;
    public final static int NAME = 1;
    public final static int SIZE = 2;
    public final static int DATE = 4;

    public static class UiFileFactory {

        public UiFile buildDir(File file) {
            return new UiFile(DIR, file.getName(), file.lastModified());
        }

        public UiFile buildFile(File file) {
            String fileName = file.getName();
            int extStart = fileName.lastIndexOf('.');
            String name = getUiFileName(fileName, extStart);
            String ext = getUiFileExtension(fileName, extStart);
            if ( ext.length() == 0 && extStart >= 0 )
                name += '.';

            return new UiFile(FILE, name, file.lastModified(), ext, file.length());
        }

        private String getUiFileName(String fileName, int extStart) {
            return extStart <= 0 ? fileName : fileName.substring(0, extStart);
        }

        private String getUiFileExtension(String fileName, int extStart) {
            return extStart <= 0 || extStart == fileName.length() -1 ?
                    "" : fileName.substring(extStart+1, fileName.length());
        }
    }

    private int type;
    private String name;
    private String extension;
    private long dataSizeInBytes;
    private String dataSizeUnit = "B";
    private double dataSize;
    private long date;


    private UiFile(int type, String name, long date) {
        this(type, name, date, "", 1);
    }

    private UiFile(int type, String name, long date, String extension, long dataSize) {
        this.type = type;
        this.name = name;
        this.extension = extension;
        this.dataSizeInBytes = dataSize;
        this.dataSize = dataSize;
        this.date = date;
    }

    public void roundToHighestUnit() {
        int highest = 1000000000;
        while ( dataSizeInBytes != 0 && highest != 0 && (long) dataSizeInBytes / highest == 0 )
            highest /= 1000;
        switch ( highest )
        {
            case 0:case 1:
                dataSizeUnit = "B";
                break;
            case 1000:
                dataSizeUnit = "KB";
                break;
            case 1000000:
                dataSizeUnit = "MB";
                break;
            case 1000000000:
                dataSizeUnit = "GB";
                break;

        }

        dataSize = 1.0 * dataSizeInBytes / (highest == 0 ? 1 : highest);
    }

    public boolean isDir() {
        return this.type == DIR;
    }


    public int compareNameTo(UiFile file) {
        StringBuilder myNameBuilder = new StringBuilder(name.toLowerCase());
        StringBuilder comparedNameBuilder = new StringBuilder(file.name.toLowerCase());

        if ( !extension.isEmpty() )
            myNameBuilder.append(".").append(extension.toLowerCase());
        if ( !file.extension.isEmpty() )
            comparedNameBuilder.append(".").append(file.extension.toLowerCase());

        String myName = myNameBuilder.toString();
        String comparedName = comparedNameBuilder.toString();

        int compareAt = sharedPartLengthFromStart(myName, comparedName);
        String comparedPartA = myName.substring(compareAt);
        String comparedPartB = comparedName.substring(compareAt);

        return comparedPartA.compareTo(comparedPartB);
    }

    public int compareSizeTo(UiFile file) {
        long diff = dataSizeInBytes - file.dataSizeInBytes;
        return diff < 0 ? -1 : diff == 0 ? 0 : 1;
    }

    public int compareDateTo(UiFile file) {
        long diff = date - file.date;
        return diff < 0 ? -1 : diff == 0 ? 0 : 1;
    }

    public int compareTo(UiFile file, int property) {
        if ( isDir() && !file.isDir() )
            return -1;

        if ( !isDir() && file.isDir() )
            return 1;
        if ( property == NAME )
            return compareNameTo(file);
        if ( property == SIZE )
            return compareSizeTo(file);

        return compareDateTo(file);
    }

    private int sharedPartLengthFromStart(String a, String b) {

        String smallest = b;
        if ( a.length() < b.length() )
        {
            smallest = a;
        }

        int i;
        for ( i = 0; i < smallest.length(); i++ )
        {
            if ( a.charAt(i) != b.charAt(i) )
                return i;
        }

        return i;
    }

    @Override
    public String toString() {
        return "type=" + (type == DIR ? "DIR" : "FILE" ) + "\t" +
                "name=" + name + "\t" +
                "dataSizeInBytes=" + dataSizeInBytes + "\t" +
                "dataSize=" + dataSize + "\t" +
                "dataUnit=" + dataSizeUnit;


    }
}
