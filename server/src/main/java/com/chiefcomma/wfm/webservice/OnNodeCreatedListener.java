package com.chiefcomma.wfm.webservice;

import com.chiefcomma.wfm.webservice.api.response.ui_tree_node.UiNode;

/**
 * Created by Hp on 10/3/2017.
 */

public interface OnNodeCreatedListener {

    void onCreated(UiNode node);
}
