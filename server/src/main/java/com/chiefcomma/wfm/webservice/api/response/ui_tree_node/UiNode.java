package com.chiefcomma.wfm.webservice.api.response.ui_tree_node;

import com.chiefcomma.wfm.webservice.NodeData;

/**
 * Created by Hp on 9/24/2017.
 */

public class UiNode {

    private String id;
    private String text;
    private String parent;
    private String icon;
    private boolean children = true;
    private NodeData data;

    public UiNode(String id, String text, String parent) {
        this(id, text, parent, null, null);
    }

    public UiNode(String id, String text, String parent, String icon, NodeData data) {
        this.id = id;
        this.text = text;
        this.parent = parent;
        this.icon = icon;
        this.data = data;
    }
}
