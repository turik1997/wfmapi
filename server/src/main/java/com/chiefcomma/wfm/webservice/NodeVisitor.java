package com.chiefcomma.wfm.webservice;

import com.chiefcomma.wfm.webservice.api.response.ui_tree_node.UiNode;
import com.chiefcomma.wfm.utils.FileVisitResult;
import com.chiefcomma.wfm.utils.SimpleFileVisitor;

import java.io.File;
import java.io.IOException;

/**
 * Created by Hp on 10/3/2017.
 */

public class NodeVisitor extends SimpleFileVisitor {

    private File endDir;
    private Mapper<String, String> mapper;
    private OnNodeCreatedListener listener;
    private boolean isFound = false;
    private boolean isDirSkipped = false;

    public NodeVisitor(File endDir, Mapper<String, String> mapper, OnNodeCreatedListener listener) {
        this.endDir = endDir;
        this.mapper = mapper;
        this.listener = listener;
    }

    @Override
    public FileVisitResult preVisitDirectory(File dir) throws IOException {

        if ( !endDir.getAbsolutePath().startsWith(dir.getAbsolutePath()) )
        {
            isDirSkipped = true;
            return FileVisitResult.SKIP_SUBTREE;
        }

        if ( dir.getAbsolutePath().equals(endDir.getAbsolutePath()) )
        {
            isFound = true;
            return FileVisitResult.TERMINATE;
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(File dir, IOException exc) throws IOException {
        if ( isDirSkipped )
        {
            isDirSkipped = false;
            return FileVisitResult.SKIP_SUBTREE;
        }

        File[] files = dir.listFiles();
        if ( files != null )
        {
            String dirId = mapper.map(dir.getAbsolutePath());
            for ( File file : files )
            {
                String id = mapper.map(file.getAbsolutePath());
                listener.onCreated(new UiNode(id, file.getName(), dirId));
            }
        }

        if ( isFound )
        {
            return FileVisitResult.TERMINATE;
        }


        return FileVisitResult.CONTINUE;
    }
}
