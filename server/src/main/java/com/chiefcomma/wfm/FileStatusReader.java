package com.chiefcomma.wfm;

import java.io.File;

public interface FileStatusReader {

    enum FileStatus {
        SAF,
        NON_SAF,
    }

    FileStatus read(File file);
}
