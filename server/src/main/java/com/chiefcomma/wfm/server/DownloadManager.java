package com.chiefcomma.wfm.server;

import com.chiefcomma.wfm.utils.MimeTypes;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class DownloadManager {

	private int counter = 0;
	private String idPrefix = "";
	private String urlBase = "";
	private MimeTypes mimeTypes = new MimeTypes();
	private Map<String, Set<String>> urlsContainer = new HashMap<String, Set<String>>();
	private Map<String, Response> urlToResponseReflection = new HashMap<String, Response>();
	private final Object lock = new Object();

	public DownloadManager() {
	}

	public DownloadManager(String urlBase) {
		this.urlBase = urlBase;
	}

	public DownloadManager(String urlBase, String idPrefix) {
		this.idPrefix = idPrefix;
		this.urlBase = urlBase;
	}

	public String makeDownloadUrl(String ip, Response response, String fileName) {
		synchronized ( lock )
		{
			Set<String> urls = urlsContainer.get(ip);
			if ( urls == null )
			{
				urls = new HashSet<String>();
				urlsContainer.put(ip, urls);
			}


			setFileContentDisposition(response, fileName);
			String downloadUrl = createUrl(Integer.toString(++counter));
			urls.add(downloadUrl);
			urlToResponseReflection.put(downloadUrl, response);

			return downloadUrl;
		}
	}

	public String makeDownloadUrl(String ip, File file) throws FileNotFoundException {

		Response downloadResponse = downloadFile(file);
		return makeDownloadUrl(ip, downloadResponse, file.getName());
	}

	public Response getFileResponse(String ip, String url) {
		Set<String> userLinks = urlsContainer.get(ip);
		if ( userLinks == null )
			return null;

		if ( !userLinks.contains(url) )
			return null;

		Response response = urlToResponseReflection.get(url);
		if ( response == null )
			return null;
		return response;
	}

	public void cancelDownload(String ip, String url) {

		synchronized ( lock )
		{
			Set<String> urls = urlsContainer.get(ip);
			if ( urls != null )
			{
				urls.remove(url);
			}

			urlToResponseReflection.remove(url);
		}
	}

	public Response downloadFile(File file) throws FileNotFoundException {
		String mime = mimeTypes.getMime(file);

		FileInputStream fis = new FileInputStream(file);

		Response response = Response.newFixedLengthResponse(Status.OK, mime, fis, file.length());
		return response;
	}

	private String createUrl(String id) {
		StringBuilder urlBuilder = new StringBuilder(urlBase);
		urlBuilder.append("/").append(idPrefix).append(id);

		return urlBuilder.toString();
	}

	//TODO this method exists in secure server. This is a copy of it. Merge them in one place
	private void setFileContentDisposition(Response r, String fileName) {
		String disposition = "attachment";
		r.addHeader("Content-Disposition", disposition + "; filename=\"" + fileName + "\"");
	}
}
