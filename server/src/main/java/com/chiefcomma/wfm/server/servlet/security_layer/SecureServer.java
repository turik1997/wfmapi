package com.chiefcomma.wfm.server.servlet.security_layer;

import com.chiefcomma.wfm.API;
import com.chiefcomma.wfm.interfaces.environment.EnvironmentInformer;
import com.chiefcomma.wfm.interfaces.environment.Notifier;
import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;
import com.chiefcomma.wfm.interfaces.listener.LogListener;
import com.chiefcomma.wfm.interfaces.users.validators.UserAccessValidator;
import com.chiefcomma.wfm.interfaces.users.validators.UserAuthValidator;
import com.chiefcomma.wfm.response.security_layer.UsernameResponse;
import com.chiefcomma.wfm.response.security_layer.errors.AccessError;
import com.chiefcomma.wfm.response.security_layer.AccessStatusResponse;
import com.chiefcomma.wfm.utils.AssetManager;
import com.chiefcomma.wfm.utils.MimeTypes;
import com.google.gson.Gson;
import org.nanohttpd.protocols.http.IHTTPSession;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.io.File;
import java.io.InputStream;
import java.util.*;

import static org.nanohttpd.protocols.http.response.Response.newChunkedResponse;

/**
 * Created by turok on 9/8/17.
 */

public class SecureServer {

    private UserAccessValidator accessValidator;
    private UserAuthValidator authValidator;
    private Notifier notifier;
    private Locale locale;
    private AssetManager assetManager;
    private ConfigProvider configProvider;
    private EnvironmentInformer environmentInformer;
    private ArrayList<LogListener> logListeners = new ArrayList<LogListener>();
    private Gson gson = new Gson();
    private final static String TAG = SecureServer.class.getSimpleName();
    private final static String LOGS_BUNDLE_PATH = "com.chiefcomma.wfm.server.servlet.security_layer" + "." + "AuthLogs";
    private final static String API_BUNDLE_PATH = "com.chiefcomma.wfm.server.servlet.security_layer" + "." + "AuthAPI";

    public SecureServer(
            ConfigProvider configProvider,
            UserAccessValidator accessValidator,
            UserAuthValidator authValidator,
            AssetManager assetManager,
            Notifier notifier,
            EnvironmentInformer environmentInformer
    ) {
        this.assetManager = assetManager;
        this.configProvider = configProvider;
        this.accessValidator = accessValidator;
        this.authValidator = authValidator;
        this.notifier = notifier;
        this.environmentInformer = environmentInformer;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Response validate(IHTTPSession session) {
        Locale requestLocale = locale;
        if ( requestLocale == null ) {
            requestLocale = new Locale("en");
        }

        //ResourceBundle bundle = ResourceBundle.getBundle(LOGS_BUNDLE_PATH, locale);

        String ip = session.getRemoteIpAddress();
        String uri = session.getUri();
        if ( uri.equals("/favicon.ico") )
        {
            //log(DEBUG, null, null, "favicon.ico request ignored");
            return Response.newFixedLengthResponse(Status.NOT_FOUND, "", "");
        }

        ////log(INFO, LOGS_BUNDLE_PATH, LogBundleKey.NEW_REQUEST, ip, session.getMethod().toString(), uri);
        //logListeners(INFO, LOGS_BUNDLE_PATH, LogBundleKey.NEW_REQUEST, ip, session.getMethod().toString(), uri);

        boolean isApiCall = false;
        if (accessValidator.isAccessProhibited(ip)) {
            //log(INFO, LOGS_BUNDLE_PATH, LogBundleKey.PROHIBITED, ip);
            //logListeners(INFO, LOGS_BUNDLE_PATH, LogBundleKey.PROHIBITED, ip);
            if (API.isApiCall(uri)) {
                isApiCall = true;
                String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.NO_ACCESS);
                String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.IP_BLOCKED);
                AccessError error = new AccessError(errorMsg, reason);
                return Response.newFixedLengthResponse(Status.FORBIDDEN, MimeTypes.JSON, gson.toJson(error));
            }

            InputStream forbiddenHtmlStream = assetManager.getAssetFile("web/forbidden.html");
            Response forbiddenHtmlResponse = newChunkedResponse(Status.OK, MimeTypes.HTML, forbiddenHtmlStream);
            return forbiddenHtmlResponse;
        }

        if (API.API_ACCESS_STATUS.equals(uri)) {
            ////log(DEBUG, null, null, "API: ", "Access Status");
            if (!accessValidator.isAccessAllowed(ip)) {
                ////log(DEBUG, null, null, "API: ", "Access Status","\t", ip, " is still not allowed");
                return Response.newFixedLengthResponse(Status.FORBIDDEN, MimeTypes.JSON, "{}");
            }

            return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(new AccessStatusResponse(
                    getDefaultPath()
            )));
        }

        boolean isRes = false;
        if (session.getUri().startsWith(API.RESOURCES)) {
            isRes = true;
            uri = session.getUri().substring(API.RESOURCES.length());
        }

        if ("/css/loading.bundle.css".equals(uri) || "/js/jquery.min.js".equals(uri)
                || "/js/langs.js".equals(uri) || "/js/loading.bundle.js".equals(uri)) {
            return assetManager.getResource(session.getUri());
        }

        if (!accessValidator.isAccessAllowed(ip) && !accessValidator.isAccessProhibited(ip)) {
            if (notifier != null) {
                notifier.askForConnectionApproval(ip);
            }
            Response loadingPageResponse = newChunkedResponse(Status.OK, MimeTypes.HTML, assetManager.getAssetFile("web/loading.html"));
            loadingPageResponse.addCookieHeader("lang=" + requestLocale.getLanguage());
            return loadingPageResponse;
        }

        if ("/css/login.bundle.css".equals(uri) || "/js/login.bundle.js".equals(uri)) {
            return assetManager.getResource(session.getUri());
        }

        Map<String, String> headers = session.getHeaders();

        if (isAuthorizationRequest(uri)) {
            //log(DEBUG, null, null, "API: ", "Authorization");
            //log(DEBUG, LOGS_BUNDLE_PATH, LogBundleKey.AUTH_REQUEST, ip);
            //logListeners(INFO, LOGS_BUNDLE_PATH, LogBundleKey.AUTH_REQUEST, ip);
            String authToken = headers.get("authorization");
            if ( !authValidator.isAuthTokenValid(ip, authToken) )
            {
                String errorMsg = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.AUTH_FAILED);
                String reason = assetManager.readBundle(API_BUNDLE_PATH, ApiBundleKey.WRONG_PASS);
                AccessError error = new AccessError(errorMsg, reason);
                return Response.newFixedLengthResponse(Status.NOT_ACCEPTABLE, MimeTypes.JSON, gson.toJson(error));
            }

            //log(DEBUG, LOGS_BUNDLE_PATH, LogBundleKey.AUTHORIZED, ip);
            //logListeners(INFO, LOGS_BUNDLE_PATH, LogBundleKey.AUTHORIZED, ip);

            AccessStatusResponse successfulAuthResponse = new AccessStatusResponse(getDefaultPath());
            return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(successfulAuthResponse));
        }

        if (API.API_USERNAME.equals(uri)) {
            //log(DEBUG, null, null, "API: ", "Get Username");
            UsernameResponse usernameResponse = new UsernameResponse(ip);
            return Response.newFixedLengthResponse(Status.OK, MimeTypes.JSON, gson.toJson(usernameResponse));
        }

        String authCookie = session.getCookies().read("authorization");
        //log(DEBUG, null, null, "Authorization Cookie: ", authCookie);
        ////System.out.println("Cookie auth: " + authCookie);

        if (configProvider.isSecuredByPass() && !authValidator.isAuthTokenValid(ip, authCookie)) {
            Response loginPageResponse = newChunkedResponse(Status.OK, MimeTypes.HTML, assetManager.getAssetFile("web/login.html"));
            loginPageResponse.addCookieHeader("lang=" + requestLocale.getLanguage());
            return loginPageResponse;
        }

        String referer = headers.get("referer");
        if ( referer != null )
            referer = referer.substring(referer.indexOf("/", "http://".length()));
        //String extPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        /*if ( uri.startsWith(internalStorageDir.getAbsolutePath()) ||
            ( referer != null && referer.startsWith(internalStorageDir.getAbsolutePath()) )
        )
        {
            //Log.d("SecureServer", "internal storage request: " + uri);
            return null;
        }

        /*
        //read permission is necessary for all requests exclude auth and tasks
        if ( !environmentInformer.isExtStorageReadable() )
        {
            //Log.d("Read", " false");
            if ( isApiCall )
            {
                //todo separate api and other requests responses
                return null;
            }
            notifier.askForReadPermission(uri);
            //TODO return api error, storage is not readable
            return Response.newFixedLengthResponse(Status.OK, MimeTypes.PLAIN_TEXT, "Grant read permission");
        }


        //write permission is necessary for all api calls
        if ( API.isApiCall(uri) && !environmentInformer.isExtStorageWritable() )
        {
            //Log.d("Write", " false");
            notifier.askForWritePermission(uri);
            //TODO return api error, storage is not writable
            return Response.newFixedLengthResponse(Status.OK, MimeTypes.PLAIN_TEXT, "Grant write permission");
        }*/

        return null;
    }

    private boolean isAuthorizationRequest(String uri) {
        return API.API_AUTH.equals(uri);
    }

    public String getDefaultPath() {
        File defaultStorage = environmentInformer.getDefaultStorage();
        if (defaultStorage != null) {
            return defaultStorage.getAbsolutePath();
        }
        return "/";
    }
/*
    public void addLogListener(LogListener listener) {
        if ( listener != null )
            logListeners.add(listener);
    }*/

    //todo delete the method
    /*final protected void log(int level, String ...messages) {

        String message = concat(messages);

        if ( level == DEBUG )
        {
            Log.debug(message);
            return;
        }

        if ( level == ERROR )
        {
            Log.error(message);
        }
        else
            Log.info(message);

        //logListeners(level, message);
    }*/
/*
    final protected void logListeners(int level, String bundlePath, String key, String ...params) {

        String message = assetManager.readBundle(bundlePath, key, locale, params);

        if ( level == ERROR )
        {
            for ( LogListener listener : logListeners )
                listener.onError(message);
            return;
        }

        for ( LogListener listener : logListeners )
            listener.onInfo(message);
    }
*/
    private static class ApiBundleKey {
        public final static String NO_ACCESS = "you_dont_have_access";
        public final static String IP_BLOCKED = "ip_blocked_reason";
        public final static String AUTH_FAILED = "auth_failed";
        public final static String WRONG_PASS = "incorrect_password_reason";
    }

    private static class LogBundleKey {
        public final static String PROHIBITED = "access_prohibited";
        public final static String NEW_REQUEST = "new_request";
        public final static String AUTH_REQUEST = "auth_request";
        public final static String AUTHORIZED = "authorized";
    }

}
