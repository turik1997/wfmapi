package com.chiefcomma.wfm.server.servlet;

import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;
import com.chiefcomma.wfm.request.RequestValidator;
import com.chiefcomma.wfm.request.ValidationException;
import com.chiefcomma.wfm.server.servlet.security_layer.SecureServer;
import com.chiefcomma.wfm.utils.ResponseHelper;
import org.nanohttpd.protocols.http.IHTTPSession;
import org.nanohttpd.protocols.http.NanoHTTPD;
import org.nanohttpd.protocols.http.request.Method;
import org.nanohttpd.protocols.http.response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * Created by turok on 9/8/17.
 */

public abstract class Servlet extends NanoHTTPD {

    private SecureServer secureServerService;
    private RequestValidator requestValidator;

    public Servlet(ConfigProvider configProvider, SecureServer secureServer, RequestValidator requestValidator) {
        super(configProvider.getPort());
        this.secureServerService = secureServer;
        this.requestValidator = requestValidator;
    }

    @Override
    protected Response serve(IHTTPSession session) {
        Response authResponse = this.secureServerService.validate(session);
        if (authResponse != null) {
            return authResponse;
        }

        Method method = session.getMethod();

        if (method.compareTo(Method.GET) == 0) {
            return doGet(session);
        }

        if (method.compareTo(Method.POST) == 0) {
            return doPost(session);
        }

        if (method.compareTo(Method.DELETE) == 0) {
            return doDelete(session);
        }

        if (method.compareTo(Method.PUT) == 0) {
            return doPut(session);
        }

        return ResponseHelper.errorWrongApi(requestValidator.getAssetManager(), session.getUri());
    }

    public Response doGet(IHTTPSession session) {
        return null;
    }

    public Response doPost(IHTTPSession session) {
        return null;
    }
    public Response doDelete(IHTTPSession session) {
        return null;
    }
    public Response doPut(IHTTPSession session) {
        return null;
    }
}
