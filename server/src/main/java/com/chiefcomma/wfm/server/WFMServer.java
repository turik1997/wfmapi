package com.chiefcomma.wfm.server;

import com.chiefcomma.wfm.API;
import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;
import com.chiefcomma.wfm.interfaces.environment.EnvironmentInformer;
import com.chiefcomma.wfm.request.*;
import com.chiefcomma.wfm.server.servlet.Servlet;
import com.chiefcomma.wfm.server.servlet.security_layer.SecureServer;
import com.chiefcomma.wfm.utils.AssetManager;
import com.chiefcomma.wfm.utils.MimeTypes;
import com.chiefcomma.wfm.utils.ResponseHelper;
import com.chiefcomma.wfm.webservice.api.response.ErrorResponse;
import com.google.gson.Gson;

import org.nanohttpd.protocols.http.IHTTPSession;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.protocols.http.response.Status;

import java.io.*;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by turok on 9/8/17.
 */

public abstract class WFMServer extends Servlet {

    private final static String FILE_FIELD_NAME = "file";
    private AssetManager assetManager;
    private String ip;
    private Gson gson = new Gson();
    private Locale locale;
    private final static String LOGS_BUNDLE_PATH = "com.chiefcomma.wfm.server." + "ServerLogs";
    public final static String API_BUNDLE_PATH = "com.chiefcomma.wfm.server." + "ServerResponses";
    protected DownloadManager downloadManager = new DownloadManager(API.API_DOWNLOAD);
    private RequestValidator requestValidator;
    private final SecureServer secureServer;
    private final EnvironmentInformer environmentInformer;

    public WFMServer(
            ConfigProvider configProvider,
            SecureServer secureServer,
            Locale locale,
            RequestValidator requestValidator, EnvironmentInformer environmentInformer) {
        super(configProvider, secureServer, requestValidator);
        this.assetManager = requestValidator.getAssetManager();
        this.locale = locale;
        this.secureServer = secureServer;
        this.requestValidator = requestValidator;
        this.environmentInformer = environmentInformer;
    }

    protected abstract Response zip(String zipName, ArrayList<String> fileNames, File srcDir, String clientIp);
    protected abstract Response download(DownloadRequest request, String ip);
    protected abstract Response getDirContent(File dir);
    protected abstract Response getTree(TreeRequest request);
    protected abstract Response fileUpload(FileUploadRequest fileUploadRequest);
    protected abstract Response getTaskInfo(String clientIp, int id);
    protected abstract Response doCopy(CopyRequest request, String clientIp);
    protected abstract Response doMove(CopyRequest request, String clientIp);
    protected abstract Response doDelete(DeleteRequest request, String clientIp);
    protected abstract Response makeDir(MakeDirRequest request, File srcDir);
    protected abstract Response getVacantDirName(String srcDir);
    protected abstract Response getTasks(String ip);
    protected abstract Response updateTask(TaskUpdateRequest request, String clientIp, int id);
    protected abstract Response cancelTask(String clientIp, int id);
    protected abstract Response doRename(RenameRequest renameRequest);
    protected abstract Response doCallback(CallbackRequest callbackRequest);

    @Override
    public Response doGet(IHTTPSession session) {
        String uri = session.getUri();
        String ip = session.getRemoteIpAddress();

        if (uri.startsWith(API.RESOURCES)) {
            return assetManager.getResource(uri);
        }

        if (API.isApiCall(uri)) {
            uri = uri.substring(API.API_URL.length());

            if (uri.startsWith(API.DOWNLOAD + "/")) {
                Response fileResponse = downloadManager.getFileResponse(ip, session.getUri());
                if (fileResponse != null) {
                    downloadManager.cancelDownload(ip, session.getUri());
                    return fileResponse;
                }

                InputStream notFoundPageStream = assetManager.getAssetFile("web/404.html");
                return Response.newChunkedResponse(Status.OK, MimeTypes.HTML, notFoundPageStream);
            }

            if (API.VACANT_DIR_NAME.equals(uri)) {
                String referer = session.getHeaders().get("referer");
                String host = session.getHeaders().get("host");
                String refStart = "http://" + host;
                try {
                    requestValidator.validateHeaders(session.getHeaders());
                } catch (ValidationException e) {
                    return e.getErrorResponse();
                }

                referer = referer.substring(refStart.length());

                return getVacantDirName(URLDecoder.decode(referer));
            }

            if (API.TASKS.equals(uri)) {
                return getTasks(ip);
            }

            if (uri.startsWith(API.TASKS + "/")) {
                String taskId = uri.substring(API.TASKS.length()+1);
                try {
                    requestValidator.validateTaskId(taskId);
                } catch (ValidationException e) {
                    return e.getErrorResponse();
                }

                return getTaskInfo(ip, Integer.parseInt(taskId));
            }
        }

        File file = new File(uri);

        if (!uri.equals("/") && !fileExists(file)) {
            InputStream notFoundPageStream = assetManager.getAssetFile("web/404.html");
            return Response.newChunkedResponse(Status.NOT_FOUND, MimeTypes.HTML, notFoundPageStream);
        }

        Map<String, String> headers = session.getHeaders();
        String preference = headers.get("prefer");

        if (file.isFile()) {
            Response fileResponse = null;
            try {
                fileResponse = downloadManager.downloadFile(file);
            } catch (FileNotFoundException e) {
                //TODO response in the proper form (html or json)
                return Response.newFixedLengthResponse(Status.INTERNAL_ERROR, MimeTypes.PLAIN_TEXT, "Something went wrong. Can't read the file");
            }

            setFileContentDisposition(fileResponse, preference, file.getName());
            return fileResponse;
        }

        if (isJsonPreferred(preference)) {
            return getDirContent(file);
        }

        /*if ( isDownloadPreferred(preference) )
        {
            String host = headers.get("host");
            String referer = headers.get("referer");
            if ( referer == null )
                return  null;

            referer = referer.substring("http://".length()+host.length());

            ArrayList<File> files = new ArrayList<File>();
            files.add(file);
            return downloadAsZip(files, ip, referer);
        }*/

        String defaultPath = secureServer.getDefaultPath();
        if ( uri.equals("/") && !"/".equals(defaultPath) )
        {
            Response r = Response.newFixedLengthResponse(Status.FOUND, "", "");
            if ( !defaultPath.startsWith("/") )
                defaultPath = "/" + defaultPath;
            r.addHeader("Location", defaultPath);
            return r;
        }

        Response r = Response.newChunkedResponse(Status.OK, MimeTypes.HTML, assetManager.getAssetFile("web/index.html"));
        r.addHeader("Vary", "Accept");
        r.addCookieHeader("lang=" + locale.getLanguage());
        return r;
    }

    private boolean fileExists(File file) {
        List<String> listOfStorages = environmentInformer.getListOfStorages();
        for (String storageVolumePath : listOfStorages) {
            if (file.getAbsolutePath().startsWith(storageVolumePath) && file.exists()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Response doPost(IHTTPSession session) {
        try {
            requestValidator.validateHeaders(session.getHeaders());
        } catch (ValidationException e) {
            return e.getErrorResponse();
        }
        String uri = session.getUri();
        if (!API.isApiCall(uri)) {
            return ResponseHelper.errorWrongApi(assetManager, uri);
        }

        uri = uri.substring(API.API_URL.length());
        String host = session.getHeaders().get("host");
        String referer = session.getHeaders().get("referer");
        referer = referer.substring(("http://"+host).length());
        referer = URLDecoder.decode(referer);
        String ip = session.getRemoteIpAddress();

        Map<String, String> body = new HashMap<String, String>();
        try {
            session.parseBody(body);
        } catch (Exception exception) {
            return ResponseHelper.internalError(assetManager);
        }

        if (API.FILE_UPLOAD.equals(uri)) {
            FileUploadRequest fileUploadRequest = buildFileUploadRequest(
                    body.get("file"),
                    session.getParameters().get(FILE_FIELD_NAME),
                    referer
            );
            try {
                requestValidator.validateFileUploadRequest(fileUploadRequest);
            } catch (ValidationException e) {
                return e.getErrorResponse();
            }
            return fileUpload(fileUploadRequest);
        } else if (body.get("postData") == null) {
            return ResponseHelper.errorEmptyPostData(assetManager);
        } else {
            String postData = body.get("postData");
            if (uri.startsWith(API.CALLBACK + "/")) {
                String callbackType = uri.substring((API.CALLBACK + "/").length());
                CallbackRequest callbackRequest = new CallbackRequest();
                callbackRequest.setPostData(postData);
                callbackRequest.setType(callbackType);
                return doCallback(callbackRequest);
            }
            if (uri.startsWith(API.TASKS + "/")) {
                String taskId = uri.substring(API.TASKS.length()+1);
                try {
                    requestValidator.validateTaskId(taskId);
                } catch (ValidationException e) {
                    return e.getErrorResponse();
                }
                TaskUpdateRequest request = gson.fromJson(postData, TaskUpdateRequest.class);
                return updateTask(request, ip, Integer.parseInt(taskId));
            } else if (uri.equals(API.GET_TREE)) {
                TreeRequest request = gson.fromJson(postData, TreeRequest.class);
                return getTree(request);
            } else if (uri.equals(API.DOWNLOAD)) {
                DownloadRequest request = gson.fromJson(postData, DownloadRequest.class);
                return download(request, ip);
            } else if (uri.equals(API.MKDIR)) {
                MakeDirRequest request = gson.fromJson(postData, MakeDirRequest.class);
                return makeDir(request, new File(referer));
            } else if (uri.equals(API.DELETE)) {
                DeleteRequest request = gson.fromJson(postData, DeleteRequest.class);
                return doDelete(request, ip);
            } else if (uri.equals(API.COPY)) {
                CopyRequest request = gson.fromJson(postData, CopyRequest.class);
                return doCopy(request, ip);
            } else if (uri.equals(API.MOVE)) {
                CopyRequest request = gson.fromJson(postData, CopyRequest.class);
                return doMove(request, ip);
            } else if (uri.equals(API.RENAME)) {
                RenameRequest renameRequest = gson.fromJson(postData, RenameRequest.class);
                return doRename(renameRequest);
            }
        }

        return ResponseHelper.errorWrongApi(assetManager, session.getUri());
    }

    @Override
    public Response doPut(IHTTPSession session) {
        return null;
    }

    @Override
    public Response doDelete(IHTTPSession session) {
        String uri = session.getUri();
        if ( uri.startsWith(API.API_TASKS + "/") ) {
            String taskId = uri.substring(API.API_TASKS.length()+1);
            try {
                requestValidator.validateTaskId(taskId);
            } catch (ValidationException e) {
                return e.getErrorResponse();
            }

            return cancelTask(session.getRemoteIpAddress(), Integer.parseInt(taskId));
        }

        return ResponseHelper.errorWrongApi(assetManager, session.getUri());
    }

    private boolean isJsonPreferred(String preference) {
        return preference != null && preference.equals("json");
    }

    private boolean isDownloadPreferred(String preference) {
        return preference != null && preference.equals("download");
    }

    private String getDisposition(String preference) {
        return isDownloadPreferred(preference) ? "attachment" : "inline";
    }

    final protected void setFileContentDisposition(Response r, String preference, String fileName) {
        String disposition = getDisposition(preference);
        r.addHeader("Content-Disposition", disposition + "; filename\"" + fileName + "\"" + "; charset=UTF-8");
    }

    final protected Object fromJson(String json, Class tClass) {
        return gson.fromJson(json, tClass);
    }

    protected final Response getErrorResponse(Status status, String err, String reason) {
        ErrorResponse error = new ErrorResponse(err, reason);
        String json = gson.toJson(error);
        return Response.newFixedLengthResponse(status, MimeTypes.JSON, json);
    }

    private FileUploadRequest buildFileUploadRequest(
            String filePath,
            List<String> fileNameUploadField,
            String referer
    ) {
        String newFileName = null;
        if (fileNameUploadField != null && !fileNameUploadField.isEmpty()) {
            newFileName = fileNameUploadField.get(0);
        }
        FileUploadRequest fileUploadRequest = new FileUploadRequest();
        fileUploadRequest.setRawFilePath(filePath);
        fileUploadRequest.setNewFileName(newFileName);
        fileUploadRequest.setDestPath(referer);
        return fileUploadRequest;
    }

    private static class LogBundleKey {

    }

    public static class ApiBundleKey {
        public final static String TASK_READ_ERR = "task_reading_error";
        public final static String BAD_ID = "bad_id";
        public final static String NEGATIVE_ID = "negative_id";
        public final static String NO_HEADER = "no_header";
        public final static String HOST_REF_MISMATCH = "host_referer_mismatch";
        public final static String NO_TMP_FILE_PATH = "cant_obtain_tmp_file";
        public final static String WRONG_API = "wrong_api";
        public final static String CANT_HANDLE = "cant_handle";
        public final static String DOWNLOAD_ERR = "download_err";
    }
}
