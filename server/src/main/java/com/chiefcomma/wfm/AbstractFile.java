package com.chiefcomma.wfm;

import java.io.*;

public class AbstractFile implements SafFileManager {

    private final FileStatusReader fileStatusReader;
    private final SafFileManager safFileManager;
    private static AbstractFile abstractFile;

    public AbstractFile() {
        this(null, null);
    }

    private AbstractFile(FileStatusReader fileStatusReader, SafFileManager safFileManager) {
        this.fileStatusReader = fileStatusReader;
        this.safFileManager = safFileManager;
    }

    public static void init(FileStatusReader fileStatusReader, SafFileManager safFileManager) {
        abstractFile = new AbstractFile(fileStatusReader, safFileManager);
    }

    public static AbstractFile getInstance() {
        if (abstractFile == null) {
            abstractFile = new AbstractFile();
        }
        return abstractFile;
    }

    public boolean isSAF(File file) {
        if (this.fileStatusReader != null) {
            return this.fileStatusReader.read(file) == FileStatusReader.FileStatus.SAF;
        }
        return false;
    }

    @Override
    public boolean mkdir(File file) {
        if (fileStatusReader != null && safFileManager != null) {
            if (fileStatusReader.read(file.getParentFile()) == FileStatusReader.FileStatus.SAF) {
                return safFileManager.mkdir(file);
            }
        }

        return file.mkdir();
    }

    @Override
    public boolean renameTo(File originalFile, String newName) {
        if (fileStatusReader != null && safFileManager != null) {
            if (fileStatusReader.read(originalFile) == FileStatusReader.FileStatus.SAF) {
                return safFileManager.renameTo(originalFile, newName);
            }
        }

        return originalFile.renameTo(new File(originalFile.getParent(), newName));
    }

    @Override
    public boolean canWrite(File file) {
        if (fileStatusReader != null && safFileManager != null) {
            if (fileStatusReader.read(file) == FileStatusReader.FileStatus.SAF) {
                return safFileManager.canWrite(file);
            }
        }
        return file.canWrite();
    }

    @Override
    public boolean delete(File file) {
        if (fileStatusReader != null && safFileManager != null) {
            if (fileStatusReader.read(file) == FileStatusReader.FileStatus.SAF) {
                return safFileManager.delete(file);
            }
        }
        return file.delete();
    }

    @Override
    public InputStream getInputStream(File file) throws FileNotFoundException {
        try {
            return new FileInputStream(file);
        } catch (Exception exception) {
            if (fileStatusReader != null && safFileManager != null) {
                if (fileStatusReader.read(file) == FileStatusReader.FileStatus.SAF) {
                    InputStream inputStream = safFileManager.getInputStream(file);
                    if (inputStream != null) {
                        return inputStream;
                    }
                }
            }
            throw exception;
        }
    }

    @Override
    public OutputStream getOutputStream(File file) throws FileNotFoundException {
        if (fileStatusReader != null && safFileManager != null) {
            if (fileStatusReader.read(file) == FileStatusReader.FileStatus.SAF) {
                return safFileManager.getOutputStream(file);
            }
        }
        return new FileOutputStream(file);
    }
}
