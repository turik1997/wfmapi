package com.chiefcomma.wfm.server.servlet.security_layer;

import com.chiefcomma.wfm.interfaces.environment.ConfigProvider;
import com.chiefcomma.wfm.utils.AssetManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.util.Locale;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

class SecureServerTest {

    @Test
    public void testTranslation(@TempDir Path tmpPath) {
        //given
        final String bundlePath = "com.chiefcomma.wfm.server.servlet.security_layer.AuthAPI";
        final String key = "simple_test";
        ConfigProvider configProvider = mock(ConfigProvider.class);
        doReturn(2007).when(configProvider).getPort();
        final SecureServer secureServer = new SecureServer(configProvider, null, null, null, null, null);

        //when
        String translation = new AssetManager(Locale.US).readBundle(bundlePath, key, Locale.US);

        //then
        assert(translation).equals("This is simple test");
    }

}